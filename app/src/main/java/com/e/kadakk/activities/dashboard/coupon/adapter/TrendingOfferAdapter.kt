package com.e.kadakk.activities.dashboard.coupon.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.coupon.CouponTabViewModel
import com.e.kadakk.activities.dashboard.coupon.model.CouponTabModel
import com.e.kadakk.base.DeviceUtil
import com.e.kadakk.databinding.ItemCouponOfferTrendingBinding
import com.e.kadakk.utils.GlideApp


class TrendingOfferAdapter(context: Context, viewModel: CouponTabViewModel) :
    RecyclerView.Adapter<TrendingOfferAdapter.ViewHolder>() {
    lateinit var mContext: Context
    lateinit var mViewModel: CouponTabViewModel
    var mList: ArrayList<CouponTabModel.Data.ByCategory.CatOffer>  = ArrayList<CouponTabModel.Data.ByCategory.CatOffer>()

    init {
        mContext = context
        mViewModel = viewModel
    }

    fun setList(list: ArrayList<CouponTabModel.Data.ByCategory.CatOffer>) {
        mList = ArrayList<CouponTabModel.Data.ByCategory.CatOffer>()
        mList = list

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = DataBindingUtil.inflate<ItemCouponOfferTrendingBinding>(
            LayoutInflater.from(mContext),
            R.layout.item_coupon_offer_trending, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
//    return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = mList.get(position)

        holder.binding!!.tvOffer.setText(data.discountValue)
        holder.binding!!.tvName.setText(data.title)

        GlideApp.with(mContext)
            .load(data.offerImage)
            .into( holder.binding!!.ivImage)




        setParam(holder)
        holder.setViewModel(position , mViewModel, data)
    }

    fun setParam(holder: ViewHolder) {
        var width: Int = (DeviceUtil.getScreenWidth(mContext) / 2.3).toInt()

//        var height = RelativeLayout.LayoutParams.WRAP_CONTENT
        var height = (DeviceUtil.getScreenWidth(mContext) / 3).toInt()

        var params = LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT)
        holder.binding!!.llTrending.layoutParams = params


        var coverImageparams = LinearLayout.LayoutParams(width, (height).toInt())
        holder.binding!!.flImage.layoutParams = coverImageparams

//        params.setMargins(0, 0, 10, 0)
//        holder.binding!!.cvItemRoot.layoutParams = params
//        holder.binding!!.ivCoverImage.layoutParams = coverImageparams


    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ItemCouponOfferTrendingBinding>(itemView)

        fun setViewModel(position: Int, viewModel : CouponTabViewModel, data: CouponTabModel.Data.ByCategory.CatOffer)
        {
            binding!!.position = position
            binding!!.viewModel = viewModel
            binding!!.dataItem = data
            binding!!.setVariable(BR.viewModel , viewModel)
            binding!!.executePendingBindings()
        }

    }



}