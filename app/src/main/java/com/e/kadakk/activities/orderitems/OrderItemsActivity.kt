package com.e.kadakk.activities.orderitems

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.orderdetail.OrderDetailActivity
import com.e.kadakk.activities.orderdetail.OrderDetailModel
import com.e.kadakk.activities.orderitems.adapter.OrderItemsAdapter
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityOrderItemsBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson


/**
 * Created by Satish Patel on 17,December,2020
 */
class OrderItemsActivity : BaseActivity<ActivityOrderItemsBinding, OrderItemsViewModel>(),
    OrderItemsNavigator, ToolBarNavigator, serverResponseNavigator {

    var TAG: String = OrderItemsActivity::class.java.simpleName
    var mToolBarViewModel: ToolBarViewModel? = null;
    public lateinit var mBinding: ActivityOrderItemsBinding
    private var mContext: Context? = null
    private var mViewModel: OrderItemsViewModel? = null
    var mShowNetworkDialog: Dialog? = null
    lateinit var mSessionPref: SessionPreferences
    var status = ""
    var totalBill = ""
    var totalItems = ""
    var orderId = ""
    lateinit var mBean : OrderDetailModel
    lateinit var mOrderItemsAdapter: OrderItemsAdapter
    var mList: ArrayList<OrderDetailModel.Data.ListItem?> = ArrayList()


    companion object {

        fun getIntent(context: Context, bundle: Bundle): Intent {
            val intent = Intent(context, OrderItemsActivity::class.java)
            intent.putExtra("bundle-data", bundle)
            return intent
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_order_items
    }

    override fun getViewModel(): OrderItemsViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(OrderItemsViewModel::class.java)

        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)
        getIntentData()
        getControls()
        //mViewModel!!.profileAPI()
    }


    /**
     * Get intent data
     */
    private fun getIntentData() {
        if (intent.getBundleExtra("bundle-data") != null) {
            orderId = intent.getBundleExtra("bundle-data")!!.getString("orderId")!!
            status = intent.getBundleExtra("bundle-data")!!.getString("status")!!
            totalBill = intent.getBundleExtra("bundle-data")!!.getString("total_bill")!!
            totalItems = intent.getBundleExtra("bundle-data")!!.getString("total_item")!!
        }
    }

    fun getControls() {
        mContext = this@OrderItemsActivity
        mSessionPref = SessionPreferences(mContext!!)
        mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)

        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.toolBar.tvTitle.text = "#"+orderId
        mBinding.toolBar.ibNotification.visibility = View.INVISIBLE
        mBinding.toolBar.ibBack.visibility = View.VISIBLE
        mBinding.toolBar.ivHeaderLogo.visibility = View.GONE

        mOrderItemsAdapter = OrderItemsAdapter(mContext!!, mViewModel!!)
        mBinding.rvSearch.layoutManager = LinearLayoutManager(mContext)
        mBinding.rvSearch.adapter = mOrderItemsAdapter

        mViewModel!!.callOrderDetailsAPI(orderId)

    }


    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }

    override fun itemClick(pos: Int, data: OrderDetailModel.Data.ListItem?) {

        val bundle: Bundle = Bundle()
        bundle.putString("orderId", orderId)
        bundle.putString("status", status)
        bundle.putString("total_bill", totalBill)
        bundle.putString("total_item", totalItems)

        bundle.putString("shop_name", data!!.shopName)
        bundle.putString("email", data.email)
        bundle.putString("contact", data.contact)
        bundle.putString("latitude", data.latitude)
        bundle.putString("longitude", data.longitude)
        bundle.putString("offer_image", data.offerImage)

        startActivity(OrderDetailActivity.getIntent(mContext!!,bundle))
    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {

        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })

    }

    override fun onResponse(eventType: String, response: String) {
        try {
            showLoaderOnRequest(false)
            if (eventType == AppConstants.ORDER_DETAIL_API){
               var mBean = Gson().fromJson(response, OrderDetailModel::class.java)

                mList = mBean.data.listItem as  ArrayList<OrderDetailModel.Data.ListItem?>
                mOrderItemsAdapter.setList(mList)
            }
        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: " + e.message)
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }



}