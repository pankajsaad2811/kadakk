package com.e.kadakk.activities.dashboard.coupon.model


import com.google.gson.annotations.SerializedName

data class CouponTabModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("errorcode")
    val errorcode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("version")
    val version: Version
) {
    data class Data(
        @SerializedName("by_category")
        val byCategory: List<ByCategory>,
        @SerializedName("category_list")
        val categoryList: List<Category>
    ) {
        data class ByCategory(
            @SerializedName("cat_id")
            val catId: String,
            @SerializedName("cat_name")
            val catName: String,
            @SerializedName("cat_offers")
            val catOffers: List<CatOffer>
        ) {
            data class CatOffer(
                @SerializedName("calory")
                val calory: String,
                @SerializedName("discount_type")
                val discountType: String,
                @SerializedName("discount_value")
                val discountValue: String,
                @SerializedName("is_favourite")
                var isFavourite: Int,
                @SerializedName("offer_id")
                val offerId: String,
                @SerializedName("offer_image")
                val offerImage: String,
                @SerializedName("short_description")
                val shortDescription: String,
                @SerializedName("time")
                val time: String,
                @SerializedName("title")
                val title: String
            )
        }

        data class Category(
            @SerializedName("cat_id")
            val catId: String,
            @SerializedName("cat_img")
            val catImg: String,
            @SerializedName("cat_name")
            val catName: String
        )
    }

    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )
}