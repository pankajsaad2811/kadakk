package com.e.kadakk.activities.trackmap

import android.app.Application
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.ApiInterface
import com.e.kadakk.utils.server.RxAPICallHelper
import com.e.kadakk.utils.server.serverResponseNavigator


/**
 * Created by Satish Patel on 12,December,2020
 */
class TrackMapViewModel (application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<TrackMapNavigator>(application) {

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    lateinit var mSessionPref: SessionPreferences

    fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    fun onCallClick(){
        getNavigator().onCallClick()
    }

    fun onMessageClick(){
        getNavigator().onMessageClick()
    }

}