package com.e.kadakk.activities.checkout

import android.app.Application
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.*


class CheckOutViewModel (application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<CheckoutNavigator>(application) {

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    lateinit var mSessionPref: SessionPreferences

    var deliveryDayStr = ""
    var noteStr = ""

    fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    fun onConfirmOrderClick(){
        getNavigator().confirmOrderClick()
    }

    fun onDeliveryDateClick(){
        getNavigator().deliveryDateClick()
    }

    fun onChangeAddressClick(){
        getNavigator().changeAddressClick()
    }

    fun callCardDetailsAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val json = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId
        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.CARD_ITEM_LIST_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""),
            AppConstants.CARD_ITEM_LIST_API, serverResponse
        )

    }

    fun callConfirmOrderAPI(orderId : String) {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val json = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "order_id",orderId
        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.CONFIRM_ORDER_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""),
            AppConstants.CONFIRM_ORDER_API, serverResponse
        )

    }

}