package com.e.kadakk.activities.resetpassword

import android.app.Application
import android.text.TextUtils
import com.e.kadakk.R
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.server.*

class ResetPasswordViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<ResetPasswordNavigator>(application) {

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface


    var mUserId: String = ""
    var passwordStr: String = ""
    var confirmPasswordStr: String = ""


    init {
        mApplication = application
        this.serverResponse = serverResponse
    }


    fun onUpdateClick() {

        if (isValidAll() == "") {
            getNavigator().onUpdateClick()
        } else {
            getNavigator().showMessage(isValidAll())
        }

    }


    /*
   * perform validation
   * */
    fun isValidAll(): String {

        if (TextUtils.isEmpty(passwordStr)) {
            return getStringfromVM(R.string.please_enter_new_password)
        } else if (passwordStr.length < 8) {
            return getStringfromVM(R.string.new_password_should_be_min_8_character)
        } /*else if (!Validator.isValidPassword(passwordStr)) {
            return getStringfromVM(R.string.please_enter_valid_new_password)
        } */ else if (TextUtils.isEmpty(confirmPasswordStr)) {
            return getStringfromVM(R.string.please_enter_confirm_password)
        } else if (confirmPasswordStr.length < 8) {
            return getStringfromVM(R.string.confirm_password_should_be_min_8_character)
        } /*else if (!Validator.isValidPassword(confirmPasswordStr)) {
            return getStringfromVM(R.string.Please_enter_valid_confirm_password)
        }*/ else if (!passwordStr.equals(confirmPasswordStr)) {
            return getStringfromVM(R.string.confirm_and_new_password_not_match)
        } else {
            return ""
        }


    }


    /**
     * Call forgot password API
     */
    public fun callSetNewPasswordAPI() {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
/*
     "JSON

{
        ""user_id"":""S7MytFIyV7IGAA=="",
        ""new_password"":""123456789""
}"
      * */
        val jsonObj = JsonElementUtil.getJsonObject(
            "user_id", mUserId,
            "new_password", passwordStr
        );

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.RESETPASSWORD_API,
                jsonObj!!,
                "",
                "",
                "",
                "",
                "",
                ""
            ),
            AppConstants.RESETPASSWORD_API, serverResponse
        )
    }


}