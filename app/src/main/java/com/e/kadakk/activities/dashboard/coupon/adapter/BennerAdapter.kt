package com.e.kadakk.activities.dashboard.coupon.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import androidx.viewpager.widget.ViewPager
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.coupon.CouponTabViewModel
import com.e.kadakk.activities.dashboard.coupon.model.CouponTabModel
import com.e.kadakk.databinding.ItemCouponBannerBinding
import com.e.kadakk.utils.GlideApp


class BennerAdapter(context : Context, viewModel: CouponTabViewModel) : PagerAdapter()
{
    val TAG = BennerAdapter::class.java.simpleName
    lateinit var mContext : Context ;
    lateinit var mViewModel : CouponTabViewModel;
    var mList: ArrayList<CouponTabModel.Data.ByCategory.CatOffer>  = ArrayList<CouponTabModel.Data.ByCategory.CatOffer>()
    var baseURL = ""

    init {
        mContext = context
        mViewModel = viewModel

    }

    fun setList(list : ArrayList<CouponTabModel.Data.ByCategory.CatOffer>)
    {
        mList= ArrayList<CouponTabModel.Data.ByCategory.CatOffer>()
        mList = list

        notifyDataSetChanged()
    }

    fun setBaseUrl(baseUrl: String){
        baseURL = baseUrl
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {

        return mList.size
//        return 10
    }
    lateinit  var itemBrandBinding: ItemCouponBannerBinding;
    override fun instantiateItem(container: ViewGroup, position: Int): Any {

        itemBrandBinding = DataBindingUtil.inflate<ItemCouponBannerBinding>(
            LayoutInflater.from(mContext) , R.layout.item_coupon_banner , container , false)

        GlideApp.with(mContext).load(mList.get(position).offerImage)
            .placeholder(R.mipmap.ic_launcher)
            .error(R.mipmap.ic_launcher).
            into(itemBrandBinding.ivBenner)
//
        itemBrandBinding.tvTitle.setText(mList.get(position).title)
        itemBrandBinding.tvPrice.setText(mList.get(position).discountValue)

//        var width: Int = (DeviceUtil.getScreenWidth(mContext) ).toInt()
//        var height = (width /2.50).toInt() -50
//        var params = LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT)
//        var params1 = LinearLayout.LayoutParams(width, height)
//        itemBrandBinding.llBrand.layoutParams = params
//        itemBrandBinding.ivBanner.layoutParams = params1

        val vp = container as ViewPager
        vp.addView(itemBrandBinding.getRoot(), 0)
        setViewModel(itemBrandBinding, position, mViewModel, mList.get(position))

        return itemBrandBinding.root
    }


    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        val vp = container as ViewPager
        val view = `object` as View
        vp.removeView(view)
    }


    fun setViewModel(itemBannerBinding: ItemCouponBannerBinding, position: Int, viewModel: CouponTabViewModel, data: CouponTabModel.Data.ByCategory.CatOffer) {
//        itemBannerBinding!!.dataItem = data
        itemBannerBinding!!.position = position
        itemBannerBinding!!.viewModel = viewModel
        itemBannerBinding!!.dataItem = data
        itemBannerBinding!!.setVariable(BR.viewModel, viewModel)
        itemBannerBinding!!.executePendingBindings()
    }



}