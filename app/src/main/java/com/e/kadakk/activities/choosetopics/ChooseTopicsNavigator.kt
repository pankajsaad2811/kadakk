package com.e.kadakk.activities.choosetopics

import android.view.View

interface ChooseTopicsNavigator {
    fun showMessage(msg:  String)
    fun onDoneClick()
    fun onChooseCategoryClick(position: Int)



}