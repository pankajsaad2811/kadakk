package com.e.kadakk.activities.otp

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.view.View
import androidx.annotation.RequiresApi
import androidx.core.widget.doAfterTextChanged
import androidx.lifecycle.ViewModelProviders
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.choosetopics.ChooseTopicsActivity
import com.e.kadakk.activities.forgotpassword.ForgotPasswordActivity
import com.e.kadakk.activities.otp.model.OTPModel
import com.e.kadakk.activities.resetpassword.ResetPasswordActivity
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityOTPBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.LoginModel
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import org.json.JSONObject
import java.util.concurrent.TimeUnit

class OTPActivity : BaseActivity<ActivityOTPBinding, OTPViewModel>(),
    OTPNavigator, ToolBarNavigator, serverResponseNavigator {


    lateinit var mContext: Context
    lateinit var mViewModel: OTPViewModel
    lateinit var mSessionPref: SessionPreferences
    lateinit var mToolBarViewModel: ToolBarViewModel
    lateinit var mBinding: ActivityOTPBinding
    lateinit var mModel: LoginModel
    private var mShowNetworkDialog: Dialog? = null;
    var mOkCancelDialog: Dialog? = null;

    var clickEvent = true

    companion object {
        fun getIntent(context: Context, bundle: Bundle): Intent {
            var intent = Intent(context, OTPActivity::class.java);
            intent.putExtra("otp-bundle", bundle)
            return intent;
        }

    }

    /**
     * Get intent data
     */
    private fun getIntentData() {
        if (intent.getBundleExtra("otp-bundle") != null) {

            mViewModel.mUserId = intent.getBundleExtra("otp-bundle")!!.getString("USERID")!!

            if (intent.getBundleExtra("otp-bundle")!!.getBoolean("ISSIGNUP") == true) {
                mViewModel.isSignup = intent.getBundleExtra("otp-bundle")!!.getBoolean("ISSIGNUP")

            }

        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel;
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getLayoutId(): Int {

        return R.layout.activity_o_t_p
    }

    override fun getViewModel(): OTPViewModel {
        mViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(OTPViewModel::class.java)
        return mViewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel
        getIntentData()
        getControls()


    }


    fun getControls() {
        mContext = this
        mSessionPref = SessionPreferences(mContext)
        mViewModel.setSessionPref(mSessionPref)
        mToolBarViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding.layoutToolbar.toolviewModel = mToolBarViewModel
        mToolBarViewModel.setToolBarNavigator(this)
        mBinding.layoutToolbar.tvTitle.text = ""
        mBinding.layoutToolbar.ibNotification.visibility = View.INVISIBLE
        mBinding.layoutToolbar.ibBack.visibility = View.VISIBLE




        mBinding.etOTP.doAfterTextChanged {
           var mOtpStr = mBinding.etOTP.text.toString().trim()


            if (mOtpStr.toString() == "") {
                mViewModel.otpStr = ""
            } else if(mOtpStr.toString().length>4){
                mBinding.etOTP.setText(mViewModel.otpStr)
            }else{
                mViewModel.otpStr = mOtpStr.toString()
            }

//            if (mOtpStr.length == 4) {
//
//                //call API
////                mViewModel.otpAPI(mSignUpData.email, mOtpStr)
//                mViewModel.otpAPI(mEmailIdStr, mOtpStr)
//            }
        }



        calculateTimer()
    }


    /*
* show all common message from one place
* */
    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            });
    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResume() {
        super.onResume()
        clickEvent = true
    }

    override fun onResponse(eventType: String, response: String) {
        clickEvent = true
        showLoaderOnRequest(false)

        if (eventType == AppConstants.VERIFY_OTP_API) {

            var mBean = Gson().fromJson(response, OTPModel::class.java)

            if (mViewModel!!.isSignup) {

                DialogUtil.okCancelDialog(mContext!!,
                    getAppString(R.string.app_name), mBean.message, getAppString(R.string.ok),
                    "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                        override fun okClick() {
                            var bundle: Bundle = Bundle()
                            bundle.putString("USERID", mBean.data.otp.userId)
                            startActivity(ChooseTopicsActivity.getIntent(mContext, bundle))
                        }

                        override fun cancelClick() {

                        }

                    })

            } else {

                var bundle: Bundle = Bundle()
                bundle.putString("USERID", mBean.data.otp.userId)
                startActivity(ResetPasswordActivity.getIntent(mContext!!, bundle))
            }

        } else if (eventType == AppConstants.RESENDOTP_API) {

            var json = JSONObject(response)
            showMessage(json.optString("message"))
            calculateTimer()
        }




    }


    override fun onRequestFailed(eventType: String, response: String) {
        clickEvent = true
        showLoaderOnRequest(false)
        showMessage(response)

    }

    override fun onRequestRetry() {
        clickEvent = true
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        clickEvent = true
    }

    override fun onMinorUpdate() {
        clickEvent = true
        showLoaderOnRequest(false)
        //showMessage(message)
    }

    override fun onAppHardUpdate() {
        clickEvent = true
    }


    override fun noNetwork() {
        clickEvent = true
        showLoaderOnRequest(false)
        showErrorMessage(mContext, getString(R.string.No_internet_connection), false)
    }

    override fun proceedClick() {

        if (clickEvent) {
            clickEvent = false
            showLoaderOnRequest(true)
            mViewModel.callOTPVerifyAPI()
        }

    }

    override fun resendOTPClick() {

        if (clickEvent) {
            clickEvent = false
            showLoaderOnRequest(true)
            mViewModel.resendOtpAPI()
            calculateTimer()
        }
    }

    override fun onBackPressed() {

        if (!mViewModel.isSignup) {
            startActivity(ForgotPasswordActivity.getIntent(mContext))
        }
        finish()

    }



    fun calculateTimer(){


        mBinding.tvResendOtp.visibility = View.GONE
        mBinding.tvResendOtp.isClickable = false
        mBinding.tvResendOtp.isEnabled = false

        var timer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                var diff = millisUntilFinished
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60

                var minute = TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished).toString()
                if (TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished) < 10) {
                    minute ="0".plus(minute)
                }

                var seconds = ((TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)) % 60).toString()
                if ((TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished)) % 60 < 10) {
                    seconds ="0".plus(seconds)
                }

                mBinding.tvTimer.text = getString(R.string.resend_on)+" "+minute + ":" +seconds +" sec"



            }

            override fun onFinish() {
                mBinding.tvResendOtp.isClickable = true
                mBinding.tvResendOtp.isEnabled = true
                mBinding.tvResendOtp.visibility = View.VISIBLE
                mBinding.tvTimer.visibility = View.GONE
            }
        }
        timer!!.start()


    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }


}
