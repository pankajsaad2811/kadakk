package com.e.kadakk.activities.viewallbycategory

import android.app.Application
import android.text.TextUtils
import android.view.View.OnFocusChangeListener
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.e.kadakk.R
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.Validator
import com.e.kadakk.utils.server.*
import java.util.regex.Pattern

class ViewAllOffersByCategoryViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<ViewAllOffersByCategoryNavigator>(application) {
    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface

    var catId: String = ""
    var subCatId: String = "0"
    var categoryName: String = ""

    var mPosition: Int = 0
    var mUserId: String = ""
    lateinit var mSessionPref: SessionPreferences

    private val PASSWORD_PATTERN: Pattern =
        Pattern.compile("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#\$%^&*]).{8,20})")

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    public fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }


    fun onFavoriteClick(position: Int){
        getNavigator().onFavoriteClick(position)
    }

    fun onGrabNowClick(position: Int){
        getNavigator().onGrabClick(position)
    }


    fun addToCardAPI(qty : String,offerId: String) {

        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        //get request

        /*
        "{
        ""user_id"":""S7MytlIyMjRRsgYA"",
        ""offer_id"": ""S7MytFIyVLIGAA==""
}"
          */
        val jsonObj = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "offer_id", offerId,
            "quantity",qty
        )

        mRxApiCallHelper.call(mApiInterface.postRequest(AppConstants.ADD_TO_CARD_API, jsonObj!!,  mSessionPref.getFirebaseRegIdPrefValue() , "", mSessionPref.mLoginData!!.data.login.authorization, "", "0.0", "0.0"),
            AppConstants.ADD_TO_CARD_API, serverResponse)

    }



    /******* API implementation ******/
    fun callSearchAPI(isShow: Boolean, text: String) {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        if (isShow){
            serverResponse.showLoaderOnRequest(true)
        }
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        var json = JsonElementUtil.getJsonObject(
            "search_term", text
        )




        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.SEARCH_OFFER_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                "",
                "",
                "",
                ""

            ),
            AppConstants.SEARCH_OFFER_API, serverResponse
        )

    }

    fun callOfferByCategoryAPI() {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

//    "{
//        ""cat_id"":""S7MyslIyNFCyBgA="",
//        ""sub_cat_id"":""0"",
//        ""user_id"":""S7MytlIyMjRRsgYA""
//}"
        var json = JsonElementUtil.getJsonObject(
            "cat_id", catId,
            "sub_cat_id", subCatId,
            "user_id", mSessionPref!!.mLoginData!!.data.login.userId

        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.OFFER_BY_CATEGORY_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""

            ),
            AppConstants.OFFER_BY_CATEGORY_API, serverResponse
        )

    }


    fun callFavoriteAPI(offerId: String) {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        //get request

        /*
        "{
        ""user_id"":""S7MytlIyMjRRsgYA"",
        ""offer_id"": ""S7MytFIyVLIGAA==""
}"
          */
        val jsonObj = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "offer_id", offerId

        );

        mRxApiCallHelper.call(mApiInterface.postRequest(AppConstants.ADD_WISH_LIST_API, jsonObj!!,  mSessionPref.getFirebaseRegIdPrefValue() , "", mSessionPref.mLoginData!!.data.login.authorization, "", "0.0", "0.0"),
            AppConstants.ADD_WISH_LIST_API, serverResponse)

    }


}