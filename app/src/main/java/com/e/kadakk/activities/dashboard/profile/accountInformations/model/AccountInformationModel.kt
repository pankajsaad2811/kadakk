package com.e.kadakk.activities.dashboard.profile.accountInformations.model


import com.google.gson.annotations.SerializedName

data class AccountInformationModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("errorcode")
    val errorcode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("version")
    val version: Version
) {
    data class Data(
        @SerializedName("profile")
        val profile: Profile
    ) {
        data class Profile(
            @SerializedName("id")
            val id: String,
            @SerializedName("user_email")
            val userEmail: String,
            @SerializedName("fullname")
            val fullName: String,
            @SerializedName("country_code")
            val countryCode: String,
            @SerializedName("user_phone")
            val userPhone: String,
            @SerializedName("dob")
            val dob: String,
            @SerializedName("languages")
            val languages: String,
            @SerializedName("user_role")
            val userRole: String,
            @SerializedName("country")
            val country: String,
            @SerializedName("city")
            val city: String,
            @SerializedName("admin_status")
            val adminStatus: String,
            @SerializedName("user_pic")
            val userPic: String
        )
    }

    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )
}