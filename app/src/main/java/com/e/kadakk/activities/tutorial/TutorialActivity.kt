package com.e.kadakk.activities.tutorial

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager.OnPageChangeListener
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.tutorial.adapter.TutorialPagerAdapter
import com.e.kadakk.activities.tutorial.bean.TutorialListModel
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityTutorialBinding
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import java.util.*
import kotlin.collections.ArrayList


class TutorialActivity : BaseActivity<ActivityTutorialBinding, TutorialViewModel>(),
    serverResponseNavigator, TutorialNavigator, TutorialPagerAdapter.ItemCLick {
    private lateinit var mViewModel: TutorialViewModel
    private lateinit var mBinding: ActivityTutorialBinding
    private lateinit var mContext: Context
    private lateinit var mActivity:Activity
    private val TAG = TutorialActivity::class.java.simpleName
    private var mShowNetworkDialog: Dialog? = null
    private var PAGE_SELECTED = 0
    private var SLIDER_LENGTH = 0


    lateinit var mTutorialList: ArrayList<TutorialListModel.TutorialListBean>

    /**
     * TIMER TASK FOR THE BANNER IMAGES
     */
    private var timerTask: Timer? = null
    public var pageBanner = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        //   AppUtil.setStatusBarColor(this,AppUtil.getColor(R.color.white))
        super.onCreate(savedInstanceState)
        initViews()

    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_tutorial
    }

    override fun getViewModel(): TutorialViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(TutorialViewModel::class.java)
        return mViewModel
    }

    private fun initViews() {
        mContext = this@TutorialActivity
        mActivity = this@TutorialActivity
        mBinding = getViewDataBinding()
        mViewModel.setNavigator(this)
        mTutorialList= ArrayList()
        mTutorialList.addAll(mViewModel.getTutorialList())

        settingSliderData()
        settingSliderView()

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            response,
            false
        )

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
        BaseActivity.showErrorMessage(
            mContext,
            getString(R.string.session_expire_msg),
            true
        )
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
        showMinorUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            false
        )
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
        showHardUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            true
        )
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            getString(R.string.No_internet_connection),
            false
        )
    }


    /**
     * Setting the slider data
     */
    private fun settingSliderView() {
        mBinding.pager.addOnPageChangeListener(object : OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}
            override fun onPageScrolled(
                position: Int,
                positionOffset: Float,
                positionOffsetPixels: Int
            ) {
            }

            override fun onPageSelected(position: Int) {
                PAGE_SELECTED = position
                if (PAGE_SELECTED == SLIDER_LENGTH) {
                    mBinding.tvSkip.visibility = View.INVISIBLE
                    mBinding.pageIndicatorView.visibility = View.VISIBLE
                    //  mBinding.tvGetStarted.visibility = View.VISIBLE
                } else {
                    mBinding.tvSkip.visibility = View.VISIBLE
                    mBinding.pageIndicatorView.visibility = View.VISIBLE
                    //  mBinding.tvGetStarted.visibility = View.GONE
                }
            }
        })
    }
    private fun settingSliderData() {
        SLIDER_LENGTH = mTutorialList.size
        val myPagerAdapter =
            TutorialPagerAdapter(mContext, mTutorialList, this)
        mBinding.pager.adapter = myPagerAdapter
        mBinding.pageIndicatorView.setViewPager(mBinding.pager)
        SLIDER_LENGTH = SLIDER_LENGTH - 1
        if (timerTask == null) {
            timerTask = Timer() // At this line a new Thread will be created
            timerTask!!.scheduleAtFixedRate(
                RemindTask(),
                0,
                3 * 1000.toLong()
            ) // delay
        }


    }

    inner class RemindTask : TimerTask() {
        override fun run() {
            performRemindTask()

        }
    }
    fun performRemindTask()
    {
        runOnUiThread(Runnable {

            if (pageBanner >=mTutorialList.size) {
                if (timerTask != null) {
                    timerTask!!.cancel()
                }
            } else {
                mBinding.pager.setCurrentItem(pageBanner++)
            }
        })
    }
    override fun onPause() {
        super.onPause()
        if (timerTask != null) {
            timerTask!!.cancel()
        }
    }

    override fun getStartedClick() {
        redirectToNextScreen()
    }

    override fun skipClick() {
        redirectToNextScreen()

    }
    fun redirectToNextScreen()
    {
        SessionPreferences(mContext).saveTutorialPrefValue( true)
//        AppUtil.startIntent(null, mActivity, LoginActivity::class.java)
        finish()
    }
}
