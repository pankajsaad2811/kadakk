package com.e.kadakk.activities.viewallbycategory

import android.view.View

interface ViewAllOffersByCategoryNavigator {
    fun showMessage(msg:  String)
    fun onFavoriteClick(position: Int)
    fun onGrabClick(position: Int)

}