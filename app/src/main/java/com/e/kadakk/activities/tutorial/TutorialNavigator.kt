package com.e.kadakk.activities.tutorial

interface TutorialNavigator {
    fun skipClick()
    fun getStartedClick()
}