package com.e.kadakk.activities.forgotpassword

import android.view.View

interface ForgotPasswordNavigator {
    fun showMessage(msg:  String)
    fun sendClick()


}