package com.e.kadakk.activities.dashboard.cart

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.checkout.CheckOutActivity
import com.e.kadakk.activities.checkout.model.CheckoutModel
import com.e.kadakk.activities.dashboard.cart.adapter.CartTabAdapter
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.base.BaseFragment
import com.e.kadakk.databinding.FragmentCartTabBinding
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import org.json.JSONObject


/**
 * Created by Satish Patel on 10,December,2020
 */
class CartTabFragment : BaseFragment<FragmentCartTabBinding, CardTabViewModel>(),
    serverResponseNavigator,CartTabNavigator{

    lateinit var mContext: Context
    //lateinit var mHomeBean: HomeTabResponse.HomeTabBean;

    lateinit var mCartTabAdapter: CartTabAdapter
    var mList: ArrayList<CheckoutModel.Data.ListCard?> = ArrayList()

    var mShowNetworkDialog: android.app.Dialog? = null
    var mOkCancelDialog: Dialog? = null
    lateinit var mSessionPref: SessionPreferences

    var mViewModel: CardTabViewModel? = null
    lateinit var mBinding: FragmentCartTabBinding

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_cart_tab
    }

    override fun getViewModel(): CardTabViewModel {
        mViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(activity!!.application, this))
                .get(CardTabViewModel::class.java)

        return mViewModel!!

    }

    companion object {

        @JvmStatic
        fun newInstance() = CartTabFragment().apply {

        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel!!.setNavigator(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding = getViewDataBinding()
        getControls()
       // mViewModel!!.callCouponOfferAPI()
    }

    override fun onResume() {
        super.onResume()
        mViewModel!!.callCardDetailsAPI()
    }

    @SuppressLint("NewApi")
    fun getControls() {
        mContext = this@CartTabFragment.activity!!
        mSessionPref = SessionPreferences(mContext)
        mViewModel!!.setSessionPref(mSessionPref)

        mCartTabAdapter = CartTabAdapter(mContext, mViewModel!!)
        mBinding.rvSearch.layoutManager = LinearLayoutManager(mContext)
        mBinding.rvSearch.adapter = mCartTabAdapter

       // mViewModel!!.callCardDetailsAPI()

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        try {
            showLoaderOnRequest(false)

            if(eventType == AppConstants.CARD_ITEM_LIST_API){
               val mBean = Gson().fromJson(response, CheckoutModel::class.java)
                mList = mBean.data.listCard as  ArrayList<CheckoutModel.Data.ListCard?>
                mCartTabAdapter.setList(mList)

            }else if(eventType == AppConstants.DELETE_TO_CARD_API){
                val json = JSONObject(response)

                DialogUtil.okCancelDialog(mContext!!,
                    getAppString(R.string.app_name),
                    json.optString("message"),
                    getAppString(R.string.ok),
                    getString(R.string.no),
                    true,
                    false,
                    object : DialogUtil.Companion.selectOkCancelListener {
                        override fun okClick() {
                            mViewModel!!.callCardDetailsAPI()
                        }

                        override fun cancelClick() {

                        }

                    })
            }

        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: "+e.message)
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        if(eventType == AppConstants.CARD_ITEM_LIST_API) {
            mBinding.btnCheckout.visibility = View.GONE
            mList.clear()
            mCartTabAdapter.setList(mList)
        }

        BaseActivity.showErrorMessage(
            mContext,
            response,
            false
        )
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }


    override fun noNetwork() {
        showLoaderOnRequest(false)
        BaseActivity.showErrorMessage(
            mContext,
            getString(R.string.No_internet_connection),
            false
        )
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), "Session expire", getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

//                    startActivity(LoginActivity.getIntent(mContext))
//                    activity!!.finishAffinity()
//                    mSessionPref.clearSession()

                }

                override fun cancelClick() {

                }

            });
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun showMessage(msg: String) {

        mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }
            })
    }

    override fun itemDeleteClick(pos: Int, data: CheckoutModel.Data.ListCard?) {

        val mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            getString(R.string.are_you_sure_do_you_want_to_delete),
            getAppString(R.string.yes),
            getAppString(R.string.no),
            true,
            true,
            object : DialogUtil.Companion.selectOkCancelListener {

                override fun okClick() {
                    mViewModel?.callDeleteCartItemAPI(data!!.offer_id)
                }

                override fun cancelClick() {

                }
            })

    }


    override fun onCheckoutClick() {
        startActivity(CheckOutActivity.getIntent(mContext!!))
    }
}