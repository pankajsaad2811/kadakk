package com.e.kadakk.activities.resetpassword

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProviders
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.login.LoginActivity
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityResetPasswordBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import org.json.JSONObject

class ResetPasswordActivity  : BaseActivity<ActivityResetPasswordBinding, ResetPasswordViewModel>(),
    ResetPasswordNavigator, ToolBarNavigator, serverResponseNavigator {


    lateinit var mContext: Context
    lateinit var mViewModel: ResetPasswordViewModel
    lateinit var mSessionPref: SessionPreferences
    lateinit var mToolBarViewModel: ToolBarViewModel
    lateinit var mBinding: ActivityResetPasswordBinding
    private var mShowNetworkDialog: Dialog? = null;
    var mOkCancelDialog: Dialog? = null;

    companion object {
        fun getIntent(context: Context, bundle:Bundle): Intent {
            var intent = Intent(context, ResetPasswordActivity::class.java);
            intent.putExtra("ResetPassword-bundle", bundle)
            return intent;
        }

    }
    /**
     * Get intent data
     */
    private fun getIntentData() {
        if (intent.getBundleExtra("ResetPassword-bundle")!=null) {
          //  mViewModel.mobileStr = intent.getBundleExtra("ResetPassword-bundle")!!.getString("MOBILE_NUMBER")!!
            mViewModel.mUserId = intent.getBundleExtra("ResetPassword-bundle")!!.getString("USERID")!!
        }
    }
    override fun getBindingVariable(): Int {
        return BR.viewModel;
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getLayoutId(): Int {
        return R.layout.activity_reset_password
    }

    override fun getViewModel(): ResetPasswordViewModel {
        mViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(ResetPasswordViewModel::class.java)
        return mViewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel
        getIntentData()
        getControls()


    }


    fun getControls() {
        mContext = this
        mSessionPref = SessionPreferences(mContext)
        mToolBarViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding.layoutToolbar.toolviewModel = mToolBarViewModel
        mToolBarViewModel.setToolBarNavigator(this)
        mBinding.layoutToolbar.tvTitle.text = ""
        mBinding.layoutToolbar.ibNotification.visibility = View.INVISIBLE
        mBinding.layoutToolbar.ibBack.visibility = View.VISIBLE




    }


    /*
* show all common message from one place
* */
    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            });
    }

    override fun onUpdateClick() {
        showLoaderOnRequest(true)
        mViewModel.callSetNewPasswordAPI()
    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)


        if (eventType == AppConstants.RESETPASSWORD_API) {
            var msg = JSONObject(response).optString("message", "")

            DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name), msg, getAppString(R.string.ok),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {

                        startActivity(LoginActivity.getIntent(mContext))
                        finishAffinity()

                    }

                    override fun cancelClick() {

                    }

                });
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
        //showMessage(message)
    }

    override fun onAppHardUpdate() {

    }





    override fun noNetwork() {
        showLoaderOnRequest(false)
        showErrorMessage(mContext, getString(R.string.No_internet_connection), false)
    }


    override fun onBackPressed() {
        super.onBackPressed()
    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }


}
