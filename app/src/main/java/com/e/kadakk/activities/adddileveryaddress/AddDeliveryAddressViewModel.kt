package com.e.kadakk.activities.adddileveryaddress

import android.app.Application
import com.e.kadakk.activities.signup.SignUpNavigator
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.*


class AddDeliveryAddressViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<AddDeliveryAddressNavigator>(application) {

    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface

    var codeStr = ""
    var mobilenumberStr = ""
    var addressStr = ""

    lateinit var mSessionPref: SessionPreferences

    fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    fun onCountryCodeClicked() {
        getNavigator().countryCodeClick()
    }

    fun onSaveClick(){
        getNavigator().saveClick()
    }

    /**
     * Call SignUp API
     */
     fun callAddDeliveryAddressAPI(defaultAddress : String, addressType : String) {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val jsonObj = JsonElementUtil.getJsonObject(
            "user_id",mSessionPref.mLoginData!!.data.login.userId,
             "username",mSessionPref.mLoginData!!.data.login.firstname,
              "address_type",addressType,
               "address",addressStr,
               "latitude","12.0458",
                "longitude","45.23665",
                "country_code",codeStr,
                "mobile",mobilenumberStr,
                "default_address",defaultAddress)

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.ADD_ADDRESS_API,
                jsonObj!!,
                "",
                "",
                "",
                "",
                "",
                ""
            ), AppConstants.ADD_ADDRESS_API, serverResponse
        )
    }

}