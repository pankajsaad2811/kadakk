package com.e.kadakk.activities.changedeliveryaddress.adapter

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.changedeliveryaddress.ChangeDeliveryAddressViewModel
import com.e.kadakk.activities.changedeliveryaddress.model.ChangeDeliveryAddressModel
import com.e.kadakk.databinding.ChangeDeliveryAddressItemBinding



class ChangeDeliveryAddressAdapter (context: Context, viewModel: ChangeDeliveryAddressViewModel,fromWherestr:String) :
    RecyclerView.Adapter<ChangeDeliveryAddressAdapter.ViewHolder>() {

    lateinit var mContext: Context
    lateinit var fromWhere:String
    var mList: ArrayList<ChangeDeliveryAddressModel.Data?> = ArrayList()
    val TAG = ChangeDeliveryAddressAdapter::class.java.simpleName
    var mViewModel: ChangeDeliveryAddressViewModel

    init {
        mContext = context
        mViewModel = viewModel
        fromWhere = fromWherestr
    }

    fun setList(list: ArrayList<ChangeDeliveryAddressModel.Data?>) {
        mList = ArrayList()
        mList = list
        Log.e("zzffz", "JJJJJ: "+mList.size)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = DataBindingUtil.inflate<ChangeDeliveryAddressItemBinding>(
            LayoutInflater.from(mContext), R.layout.change_delivery_address_item, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val data = mList.get(position)

         holder.binding!!.tvAddressLocation.setText(data?.address)
         holder.binding!!.tvAddressName.text = data?.userName
         holder.binding!!.tvNameOfAddress.text = data?.addressType
         holder.binding!!.tvAddressPhone.text = data?.countryCode+" "+data?.mobile

        /* GlideApp.with(mContext)
             .load(data.busiImg)
             .into(holder.binding!!.ivImage)*/

        if(data?.defaultAddress.equals("yes",true)){
            holder.binding!!.tvDefaultAddress.visibility = View.VISIBLE
        }else{
            holder.binding!!.tvDefaultAddress.visibility = View.GONE
        }

        if(fromWhere.equals("")){
            holder.binding!!.btnDelete.visibility = View.VISIBLE
        }else{
            holder.binding!!.btnDelete.visibility = View.GONE
        }


        holder.setViewModel(position, mViewModel, data)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ChangeDeliveryAddressItemBinding>(itemView)

        fun setViewModel(position: Int, viewModel: ChangeDeliveryAddressViewModel, data: ChangeDeliveryAddressModel.Data?) {
            binding!!.position = position
            binding!!.itemRow = data
            binding!!.viewModel = viewModel
            binding!!.setVariable(BR.viewModel, viewModel)
            binding!!.executePendingBindings()
        }
    }

}