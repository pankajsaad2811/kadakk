package com.e.kadakk.activities.dashboard.coupon.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.coupon.CouponTabViewModel
import com.e.kadakk.activities.dashboard.coupon.model.CouponTabModel
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ItemCouponCategoryBinding
import com.e.kadakk.utils.GlideApp


class CategoryAdapter(context: Context, viewModel: CouponTabViewModel) :
    RecyclerView.Adapter<CategoryAdapter.ViewHolder>() {
    lateinit var mContext: Context
    lateinit var mViewModel: CouponTabViewModel
    var mList: ArrayList<CouponTabModel.Data.Category>  = ArrayList<CouponTabModel.Data.Category>()

    init {
        mContext = context
        mViewModel = viewModel
    }

    fun setList(list: ArrayList<CouponTabModel.Data.Category>) {
        mList = ArrayList<CouponTabModel.Data.Category>()
        mList = list

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = DataBindingUtil.inflate<ItemCouponCategoryBinding>(
            LayoutInflater.from(mContext),
            R.layout.item_coupon_category, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
//    return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = mList.get(position)

        if (position%4 == 0){

            holder.binding!!.cvCategory.setCardBackgroundColor( BaseActivity.getColor(mContext, R.color.light_violet_color))

        }else if ((position%4) == 1){

            holder.binding!!.cvCategory.setCardBackgroundColor( BaseActivity.getColor(mContext, R.color.blue))

        }else if ((position%4) == 2){

            holder.binding!!.cvCategory.setCardBackgroundColor( BaseActivity.getColor(mContext, R.color.green_color))

        }else if ((position%4) == 3){

            holder.binding!!.cvCategory.setCardBackgroundColor( BaseActivity.getColor(mContext, R.color.vivid_orange_color))

        }


        holder.binding!!.tvName.setText(data.catName)

        GlideApp.with(mContext)
            .load(data.catImg)
            .into( holder.binding!!.ivIcon)


        holder.setViewModel(position , mViewModel)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ItemCouponCategoryBinding>(itemView)

        fun setViewModel(position: Int, viewModel : CouponTabViewModel)
        {
            binding!!.position = position
            binding!!.viewModel = viewModel
//            binding!!.itemData = data
            binding!!.setVariable(BR.viewModel , viewModel)
            binding!!.executePendingBindings()
        }

    }



}