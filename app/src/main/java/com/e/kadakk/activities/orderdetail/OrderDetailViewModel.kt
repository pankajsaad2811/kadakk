package com.e.kadakk.activities.orderdetail

import android.app.Application
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.*


class OrderDetailViewModel (application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<OrderDetailNavigator>(application){

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    lateinit var mSessionPref: SessionPreferences
    var orderId = ""

    fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    fun onTrackMapClick(){
        getNavigator().trackMapClick()
    }

    fun callOrderDetailsAPI() {

        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val json = JsonElementUtil.getJsonObject(
            "order_id", orderId
        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.ORDER_DETAIL_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""),
            AppConstants.ORDER_DETAIL_API, serverResponse
        )

    }

}