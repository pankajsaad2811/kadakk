package com.e.kadakk.activities.categorydetail

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.categorydetail.adapter.SimilarOfferAdapter
import com.e.kadakk.activities.categorydetail.model.CategoryDetailModel
import com.e.kadakk.activities.checkout.CheckOutActivity
import com.e.kadakk.activities.dashboard.search.SearchTabFragment
import com.e.kadakk.activities.dashboard.search.adapter.SearchTabAdapter
import com.e.kadakk.activities.dashboard.search.model.SearchModel

import com.e.kadakk.base.BaseActivity
import com.e.kadakk.base.GridSpacesItemDecoration
import com.e.kadakk.databinding.ActivityCategoryDetailBinding
import com.e.kadakk.databinding.AddCategoryItemDialogBinding
import com.e.kadakk.databinding.DialogOkCancelBinding

import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.*
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import org.json.JSONObject


class CategoryDetailActivity : BaseActivity<ActivityCategoryDetailBinding, CategoryDetailViewModel>(),
    CategoryDetailNavigator, ToolBarNavigator, serverResponseNavigator {

    var mShowNetworkDialog: Dialog? = null
    var TAG: String = CategoryDetailActivity::class.java.simpleName
    var mToolBarViewModel: ToolBarViewModel? = null;
    private lateinit var mBinding: ActivityCategoryDetailBinding
    private var mContext: Context? = null
    private lateinit var mSessionPref: SessionPreferences
    private var mViewModel: CategoryDetailViewModel? = null
    var mBean: CategoryDetailModel? = null

    lateinit var mAdapter: SimilarOfferAdapter
    var mList: ArrayList<CategoryDetailModel.Data.SimilarOffer> = ArrayList()


    companion object {

        fun getIntent(context: Context, bundle: Bundle): Intent {
            val intent = Intent(context, CategoryDetailActivity::class.java);
            intent.putExtra("bundle-data", bundle)
            return intent
        }
    }

    /**
     * Get intent data
     */
    private fun getIntentData() {
        if (intent.getBundleExtra("bundle-data") != null) {
            mViewModel!!.offerId = intent.getBundleExtra("bundle-data")!!.getString("offerId")!!

        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_category_detail
    }

    override fun getViewModel(): CategoryDetailViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(CategoryDetailViewModel::class.java)
        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)
        getIntentData()
        getControls()
    }

    fun getControls() {
        mContext = this@CategoryDetailActivity
        mSessionPref = SessionPreferences(mContext!!)
        mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)

        mBinding.layoutToolbar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.layoutToolbar.tvTitle.text = ""
        mBinding.layoutToolbar.ibNotification.visibility = View.INVISIBLE

        mBinding.layoutToolbar.ibBack.visibility = View.VISIBLE


        mAdapter = SimilarOfferAdapter(mContext!!, mViewModel!!)
        mBinding.rvList.adapter = mAdapter
        mBinding.rvList.layoutManager = GridLayoutManager(mContext, 2)

        val spanCount =
            2 // 3 columns
        val spacing = 10 // 50px
        val includeEdge = false
        mBinding.rvList.addItemDecoration(
            GridSpacesItemDecoration(
                spanCount,
                spacing,
                includeEdge
            )
        )

        mViewModel!!.callOfferDetailsAPI()
    }

    override fun onResume() {
        super.onResume()
        setProfileData()
    }

    fun setProfileData() {
        mSessionPref = SessionPreferences(mContext!!)
    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    fun setData() {

        mList = mBean!!.data.similarOffer as ArrayList<CategoryDetailModel.Data.SimilarOffer>
        mAdapter.setList(mList)

        mBinding.itemName.text = mBean!!.data.offerDetails.title
        mBinding.offerDiscount.text = mBean!!.data.offerDetails.discountValue+"%off"
        mBinding.tvDescription.text = mBean!!.data.offerDetails.shortDescription

        GlideApp.with(mContext!!)
            .load(mBean!!.data.offerDetails.offerImage.image)
            .into( mBinding!!.productImg)

    }

    override fun onResponse(eventType: String, response: String) {

        try {

            showLoaderOnRequest(false)

            Log.e("cvxcv", "Response: " + response)

            if (eventType == AppConstants.OFFER_DETAILS_API) {
                mBean = Gson().fromJson(response, CategoryDetailModel::class.java)
                setData()
            } else if (eventType == AppConstants.ADD_WISH_LIST_API) {
                if (mList.get(mViewModel!!.mPosition).isFavourite == 0) {
                    mList.get(mViewModel!!.mPosition).isFavourite = 1
                } else {
                    mList.get(mViewModel!!.mPosition).isFavourite = 0
                }
                mAdapter.notifyItemChanged(mViewModel!!.mPosition)

            } else  if (eventType == AppConstants.ADD_TO_CARD_API){
                startActivity(CheckOutActivity.getIntent(mContext!!))
                finish()
            }

        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: " + e.message)
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            response,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onGrabNowClick(position: Int, data: CategoryDetailModel.Data.SimilarOffer) {

        val dialog = Dialog(mContext!!)//, R.style.AppListDialogTheme)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.getWindow()!!.getAttributes().windowAnimations = R.style.Dialog_WindowAnimation;

        val binding = DataBindingUtil.inflate<AddCategoryItemDialogBinding>(
            LayoutInflater.from(mContext!!),
            R.layout.add_category_item_dialog, null, false
        )
        dialog.window!!.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.setContentView(binding.getRoot())

        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

//      binding.tvAddressLocation.text = mBean!!.data.offerDetails.
        //     binding.tvTitle.text = mBean!!.data.offerDetails.discountType
        binding.tvOff.text = data.discountValue+"% off"
       // binding.tvAmount.text = data.onMinPurchase
        binding.tvItemName.text = data.title

        GlideApp.with(mContext!!)
            .load(mBean!!.data.offerDetails.offerImage.image)
            .into( binding!!.itemImage)

        binding.btnCancel.setOnClickListener{
            dialog.dismiss()
        }

        binding.btnPlus.setOnClickListener {

            val qty :String = binding.tvQuantity.text.toString()

            if(Integer.parseInt(qty)<5){
                val newQty = Integer.parseInt(qty)+1
                binding.tvQuantity.text = ""+newQty
            }
        }

        binding.btnMinus.setOnClickListener {

            val qty :String = binding.tvQuantity.text.toString()

            if(Integer.parseInt(qty)>1){
                val newQty = Integer.parseInt(qty)-1
                binding.tvQuantity.text = ""+newQty
            }
        }

        binding.btnGrabNow.setOnClickListener {
            mViewModel!!.addToCardAPI(binding.tvQuantity.text.toString(),data.offerId)
            dialog.dismiss()
        }

        dialog.show()

    }

    override fun grabClick() {
        showGrabDialog()
    }

    fun showGrabDialog() {

        val dialog = Dialog(mContext!!)//, R.style.AppListDialogTheme)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.getWindow()!!.getAttributes().windowAnimations = R.style.Dialog_WindowAnimation;

        val binding = DataBindingUtil.inflate<AddCategoryItemDialogBinding>(
            LayoutInflater.from(mContext!!),
            R.layout.add_category_item_dialog, null, false
        )
        dialog.window!!.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.setContentView(binding.getRoot())

        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)

//      binding.tvAddressLocation.text = mBean!!.data.offerDetails.
 //     binding.tvTitle.text = mBean!!.data.offerDetails.discountType
        binding.tvOff.text = mBean!!.data.offerDetails.discountValue+"% off"
        binding.tvAmount.text = mBean!!.data.offerDetails.onMinPurchase
        binding.tvItemName.text = mBean!!.data.offerDetails.title

        GlideApp.with(mContext!!)
            .load(mBean!!.data.offerDetails.offerImage.image)
            .into( binding!!.itemImage)

        binding.btnCancel.setOnClickListener{
            dialog.dismiss()
        }

        binding.btnPlus.setOnClickListener {

            val qty :String = binding.tvQuantity.text.toString()

            if(Integer.parseInt(qty)<5){
                val newQty = Integer.parseInt(qty)+1
                binding.tvQuantity.text = ""+newQty
            }
        }

        binding.btnMinus.setOnClickListener {

            val qty :String = binding.tvQuantity.text.toString()

            if(Integer.parseInt(qty)>1){
                val newQty = Integer.parseInt(qty)-1
                binding.tvQuantity.text = ""+newQty
            }
        }

        binding.btnGrabNow.setOnClickListener {
            mViewModel!!.addToCardAPI(binding.tvQuantity.text.toString(),mViewModel!!.offerId)
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun notificationIconClick() {

    }

    override
    fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            });
    }

    override fun onFavoriteClick(position: Int, data: CategoryDetailModel.Data.SimilarOffer) {
        mViewModel!!.mPosition = position
        mViewModel!!.callFavoriteAPI(data.offerId)

    }

    override fun onItemClick(position: Int, data: CategoryDetailModel.Data.SimilarOffer) {

        var bundle: Bundle = Bundle()
        bundle.putString("offerId", data.offerId)

        startActivity(CategoryDetailActivity.getIntent(mContext!!, bundle))

    }

}