package com.e.kadakk.activities.splash

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.DashboardActivity
import com.e.kadakk.activities.login.LoginActivity
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivitySplashBinding
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory

class SplashActivity : BaseActivity<ActivitySplashBinding, SplashViewModel>(),
    serverResponseNavigator, SplashNavigator {
    lateinit var mViewModel: SplashViewModel
    private lateinit var mBinding: ActivitySplashBinding
    private lateinit var mContext: Context
    private lateinit var mActivity: Activity
    private val TAG = SplashActivity::class.java.simpleName
    private lateinit var mSessionPref: SessionPreferences
    var mShowNetworkDialog: Dialog? = null;
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViews()

    }
    companion object {
        fun getIntent(context: Context): Intent {
            var intent = Intent(context, SplashActivity::class.java)
            return intent;
        }
    }
    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_splash
    }

    override fun getViewModel(): SplashViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(SplashViewModel::class.java)
        return mViewModel
    }


    private fun initViews() {
        mContext = this@SplashActivity
        mActivity = this@SplashActivity
        mSessionPref = SessionPreferences(mContext)
        mBinding = getViewDataBinding()
        mViewModel.setNavigator(this)

        Handler().postDelayed(
            {



                    if (mSessionPref.getSessionPref() != null && mSessionPref.getSessionPref()!!.data.login.authorization!="") {

                        startActivity(DashboardActivity.getIntent(mContext!!, true))

                    }else {
                        AppUtil.startIntent(null, mActivity, LoginActivity::class.java)
                        finish()
                    }



              //  startActivity(LoginActivity.getIntent(mContext))

            },3000)

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            response,
            false
        )

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
        BaseActivity.showErrorMessage(
            mContext,
            getString(R.string.session_expire_msg),
            true
        )
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
        showMinorUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            false
        )
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
        showHardUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            true
        )
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            getString(R.string.No_internet_connection),
            false
        )
    }
}
