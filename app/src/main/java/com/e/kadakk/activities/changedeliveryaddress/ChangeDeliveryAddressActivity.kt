package com.e.kadakk.activities.changedeliveryaddress

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.adddileveryaddress.AddDeliveryAddressActivity
import com.e.kadakk.activities.changedeliveryaddress.adapter.ChangeDeliveryAddressAdapter
import com.e.kadakk.activities.changedeliveryaddress.model.ChangeDeliveryAddressModel
import com.e.kadakk.activities.checkout.CheckOutActivity
import com.e.kadakk.activities.login.LoginActivity
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.base.BaseFragment
import com.e.kadakk.databinding.ActivityChangeDeliveryAddressBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import org.json.JSONObject


class ChangeDeliveryAddressActivity : BaseActivity<ActivityChangeDeliveryAddressBinding, ChangeDeliveryAddressViewModel>(),
    serverResponseNavigator, ChangeDeliveryAddressNavigator, ToolBarNavigator {

    var mViewModel: ChangeDeliveryAddressViewModel? = null
    lateinit var mBinding: ActivityChangeDeliveryAddressBinding
    lateinit var mContext: Context
    var mShowNetworkDialog: Dialog? = null
    var mToolBarViewModel: ToolBarViewModel? = null
    var changeDeliveryAddressModel: ChangeDeliveryAddressModel? = null
    private lateinit var mSessionPref: SessionPreferences
    lateinit var mAdapter: ChangeDeliveryAddressAdapter
    var mList: ArrayList<ChangeDeliveryAddressModel.Data?> = ArrayList()
    var fromWhere : String = ""

    companion object {

        fun getIntent(context: Context): Intent {
            val intent = Intent(context, ChangeDeliveryAddressActivity::class.java);
            return intent
        }

        fun getIntent(context: Context,bundle: Bundle): Intent {
            val intent = Intent(context, ChangeDeliveryAddressActivity::class.java)
            intent.putExtra("bundle-data", bundle)
            return intent
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel;
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_change_delivery_address
    }

    override fun getViewModel(): ChangeDeliveryAddressViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ChangeDeliveryAddressViewModel::class.java)
        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)
        getIntentData()
        getControls()
    }

    /**
     * Get intent data
     */
    private fun getIntentData() {
        if (intent.getBundleExtra("bundle-data") != null) {
            fromWhere = intent.getBundleExtra("bundle-data")!!.getString("from_were")!!
        }
    }

    fun getControls() {

        mContext = this@ChangeDeliveryAddressActivity
        mSessionPref = SessionPreferences(mContext!!)
        mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.toolBar.tvTitle.text = getAppString(R.string.change_delivery_address)
        mBinding.toolBar.ibNotification.visibility = View.INVISIBLE
        mBinding.toolBar.ibBack.visibility = View.VISIBLE
        mBinding.toolBar.ivHeaderLogo.visibility = View.GONE


        // Set Top Cook Adapter
        mAdapter = ChangeDeliveryAddressAdapter(mContext!!, mViewModel!!,fromWhere)
        mBinding.rvList.adapter = mAdapter

    }

    override fun itemDeleteClick(pos: Int, data: ChangeDeliveryAddressModel.Data) {

        var mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            getString(R.string.are_you_sure_do_you_want_to_delete),
            getAppString(R.string.yes),
            getAppString(R.string.no),
            true,
            true,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {
                    mViewModel!!.callDeleteAddressAPI(data.addressID)
                }

                override fun cancelClick() {

                }
            })
    }

    override fun itemClick(pos: Int, data: ChangeDeliveryAddressModel.Data) {
        val intent = Intent()
        intent.putExtra("address_id",data.addressID)
        intent.putExtra("username",data.userName)
        intent.putExtra("address_type",data.addressType)
        intent.putExtra("address",data.address)
        intent.putExtra("latitude",data.latitude)
        intent.putExtra("longitude",data.longitude)
        intent.putExtra("country_code",data.countryCode)
        intent.putExtra("mobile",data.mobile)
        intent.putExtra("default_address",data.defaultAddress)
        intent.putExtra("created_at",data.created_at)

        if (!fromWhere.equals("",true)) {
            setResult(Activity.RESULT_OK, intent)
            finish()
        }
    }

    override fun onResume() {
        super.onResume()
        mViewModel!!.callAddressListAPI()
        setProfileData()
    }

    fun setProfileData() {
        mSessionPref = SessionPreferences(mContext!!)
    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)

        if (eventType == AppConstants.GET_ADDRESS_LIST_API) {

            try {
                mList.clear()
                val mBean = Gson().fromJson(response, ChangeDeliveryAddressModel::class.java)
                mList.addAll(mBean.data)
                mAdapter.setList(mList)

            } catch (e: Exception) {
                Log.e("Exception", "OnResponce: "+e.message)
            }

        }else if(eventType == AppConstants.DELETE_ADDRESS_API){

            val json = JSONObject(response)

            DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name),
                json.optString("message"),
                getAppString(R.string.ok),
                getString(R.string.no),
                true,
                false,
                object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                        mViewModel!!.callAddressListAPI()
                    }

                    override fun cancelClick() {

                    }

                })
        }
    }


    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })
    }


    override fun addNewAddressClick() {
        startActivity(AddDeliveryAddressActivity.getIntent(mContext!!))
        //startActivity(CheckOutActivity.getIntent(mContext!!))
    }
}