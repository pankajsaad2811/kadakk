package com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing

import android.app.Application
import com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing.model.OngoingModel
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.*


class OngoingOrderViewModel (application: Application, serverResponse: serverResponseNavigator) :

    BaseViewModel<OngoingOrderNavigator>(application) {

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    lateinit var mSessionPref: SessionPreferences

    fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

   fun onGotoShoppingClick(){
       getNavigator().gotoShoppingClick()
   }

   fun onItemClick(pos:Int,data: OngoingModel.Data.ListOrder?){
       getNavigator().onItemClick(pos,data)
   }

    public fun callOngoingListAPI() {

        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val json = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "tab_order","1"
        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.ORDER_LIST_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""),
            AppConstants.ORDER_LIST_API, serverResponse
        )

    }

}