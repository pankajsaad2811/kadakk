package com.e.kadakk.activities.trackmap


/**
 * Created by Satish Patel on 12,December,2020
 */
interface TrackMapNavigator {
    fun showMessage(msg:  String)
    fun onCallClick()
    fun onMessageClick()
}