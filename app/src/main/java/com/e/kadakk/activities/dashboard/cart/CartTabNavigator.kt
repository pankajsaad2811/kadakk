package com.e.kadakk.activities.dashboard.cart

import com.e.kadakk.activities.checkout.model.CheckoutModel


/**
 * Created by Satish Patel on 10,December,2020
 */
interface CartTabNavigator {
    fun showMessage(msg:  String)
    fun itemDeleteClick(pos:Int , data: CheckoutModel.Data.ListCard?)
    fun onCheckoutClick()
}