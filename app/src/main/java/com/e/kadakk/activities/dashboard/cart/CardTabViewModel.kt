package com.e.kadakk.activities.dashboard.cart

import android.app.Application
import com.e.kadakk.activities.checkout.model.CheckoutModel
import com.e.kadakk.activities.dashboard.coupon.CouponTabNavigator
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.*


/**
 * Created by Satish Patel on 10,December,2020
 */
class CardTabViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<CartTabNavigator>(application){

    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    var emailStr: String = ""
    var passwordStr: String = ""
    lateinit var mSessionPref: SessionPreferences
    var mPosition: Int = 0
    var mOuterPosition: Int = 0

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    public fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    fun onItemDeleteClick(pos:Int , data: CheckoutModel.Data.ListCard?){
        getNavigator().itemDeleteClick(pos,data)
    }

    fun onCheckoutClick(){
        getNavigator().onCheckoutClick()
    }

    /******* API implementation ******/
    fun callCardDetailsAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val json = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId
        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.CARD_ITEM_LIST_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""),
            AppConstants.CARD_ITEM_LIST_API, serverResponse
        )
    }


    /******* API implementation ******/
    fun callDeleteCartItemAPI(offer_id:String) {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val json = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "offer_id",offer_id
        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.DELETE_TO_CARD_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""),
            AppConstants.DELETE_TO_CARD_API, serverResponse
        )
    }


    /******* API implementation ******/
    fun callUpdateCartItemAPI(offer_id:String,qty: Int) {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val json = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "offer_id",offer_id,
             "quantity",""+qty
        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.UPDATE_TO_CARD_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""),
            AppConstants.UPDATE_TO_CARD_API, serverResponse
        )
    }
}