package com.e.kadakk.activities.choosetopics.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.choosetopics.ChooseTopicsViewModel
import com.e.kadakk.activities.choosetopics.model.ChooseCategoryModel
import com.e.kadakk.base.DeviceUtil
import com.e.kadakk.databinding.ItemChooseCategoryBinding
import com.e.kadakk.utils.GlideApp


class ChooseCategoryAdapter(context: Context, viewModel: ChooseTopicsViewModel) :
    RecyclerView.Adapter<ChooseCategoryAdapter.ViewHolder>() {
    lateinit var mContext: Context
    lateinit var mViewModel: ChooseTopicsViewModel
    var mList: ArrayList<ChooseCategoryModel.Data>  = ArrayList<ChooseCategoryModel.Data>()

    init {
        mContext = context
        mViewModel = viewModel
    }

    fun setList(list: ArrayList<ChooseCategoryModel.Data>) {
        mList = ArrayList<ChooseCategoryModel.Data>()
        mList = list

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = DataBindingUtil.inflate<ItemChooseCategoryBinding>(
            LayoutInflater.from(mContext),
            R.layout.item_choose_category, parent, false
        )
        return ViewHolder(
            view.root
        )
    }

    override fun getItemCount(): Int {
        return mList.size
//    return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = mList.get(position)

        holder.binding!!.tvName.setText(data.catName)

        GlideApp.with(mContext)
            .load(data.catImg)
            .into( holder.binding!!.ivImage)

        if (data.isSelected){
            holder.binding!!.ivChecked.setImageResource(R.drawable.ic_checked_choose_category)
        }else{
            holder.binding!!.ivChecked.setImageResource(R.drawable.ic_uncheked_choose_category)
        }


        setParam(holder)
        holder.setViewModel(position , mViewModel)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ItemChooseCategoryBinding>(itemView)

        fun setViewModel(position: Int, viewModel : ChooseTopicsViewModel)
        {
            binding!!.position = position
            binding!!.viewModel = viewModel
//            binding!!.itemData = data
            binding!!.setVariable(BR.viewModel , viewModel)
            binding!!.executePendingBindings()
        }

    }


    fun setParam(holder: ViewHolder) {
        var width: Int = (DeviceUtil.getScreenWidth(mContext) / 4).toInt()

        var params = LinearLayout.LayoutParams(width, width)
        params.setMargins(0,0,0,22)
        holder.binding!!.cvCategory.layoutParams = params

    }


}