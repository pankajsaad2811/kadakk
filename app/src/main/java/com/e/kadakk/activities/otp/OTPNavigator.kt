package com.e.kadakk.activities.otp

import android.os.Bundle

interface OTPNavigator {

    fun showMessage(msg:  String)
    fun proceedClick()
    fun resendOTPClick()

}