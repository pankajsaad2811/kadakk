package com.e.kadakk.activities.checkout



interface CheckoutNavigator {
    fun showMessage(msg:  String)
    fun confirmOrderClick()
    fun deliveryDateClick()
    fun changeAddressClick()
}