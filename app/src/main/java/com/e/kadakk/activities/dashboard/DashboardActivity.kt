package com.e.kadakk.activities.dashboard

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.core.view.get
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.cart.CartTabFragment
import com.e.kadakk.activities.dashboard.coupon.CouponTabFragment
import com.e.kadakk.activities.dashboard.favorite.FavoriteTabFragment
import com.e.kadakk.activities.dashboard.profile.ProfileTabFragment
import com.e.kadakk.activities.dashboard.search.SearchTabFragment
import com.e.kadakk.activities.login.LoginActivity
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityDashboardBinding
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory



class DashboardActivity : BaseActivity<ActivityDashboardBinding,DashboardViewModel>(),
    serverResponseNavigator,DashboardNavigator {

    private lateinit var mViewModel:DashboardViewModel
    private lateinit var mBinding:ActivityDashboardBinding
    private lateinit var mContext: Context
    private lateinit var mActivity: Activity
    private val TAG = DashboardActivity::class.java.simpleName
    private lateinit var mSessionPref: SessionPreferences
    private var mShowNetworkDialog: Dialog? = null
    var tabPosition: Int = 0

   companion object {

        fun getIntent(context: Context, isClear: Boolean): Intent {
            val intent = Intent(context, DashboardActivity::class.java)
            if (isClear) {
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or
                            Intent.FLAG_ACTIVITY_NEW_TASK or
                            Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            return intent
        }

        fun getIntent(context: Context, isClear: Boolean, bundle: Bundle): Intent {
            val intent = Intent(context, DashboardActivity::class.java)
            intent.putExtra("bundle-data", bundle)

            if (isClear) {
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or
                            Intent.FLAG_ACTIVITY_NEW_TASK or
                            Intent.FLAG_ACTIVITY_CLEAR_TASK
            }
            return intent
        }

       fun getIntent(context: Context): Intent {
           val intent = Intent(context, DashboardActivity::class.java)
           return intent
       }
    }

    /**
     * Get intent data
     */
    private fun getIntentData() {
        if (intent.getBundleExtra("bundle-data") != null) {
            tabPosition = intent.getBundleExtra("bundle-data")!!.getInt("TAB-POSITION")
            Log.e("dsfd", "JJJJ: " + tabPosition)
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = getViewDataBinding()

        mViewModel.setNavigator(this)

        mBinding.viewModel = mViewModel
        getIntentData()
        initViews()

        mBinding.navigationView.setOnNavigationItemSelectedListener {

            when(it.itemId){

                R.id.navigation_coupon ->{

                    loadFragment(CouponTabFragment.newInstance())
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.navigation_search ->{

                    loadFragment(SearchTabFragment.newInstance())
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.navigation_cart ->{
                    loadFragment(CartTabFragment.newInstance())
                    return@setOnNavigationItemSelectedListener true

                }

                R.id.navigation_favorites ->{
                    loadFragment(FavoriteTabFragment.newInstance())
                    return@setOnNavigationItemSelectedListener true
                }

                R.id.navigation_profile ->{
                    loadFragment(ProfileTabFragment.newInstance())
                    return@setOnNavigationItemSelectedListener true
                }
            }
            false
        }

        if (tabPosition == 0){
            mBinding.navigationView.menu.getItem(0).setChecked(true)
            loadFragment(CouponTabFragment.newInstance())
        }else{
            loadFragment(SearchTabFragment.newInstance())
            mBinding.navigationView.menu.getItem(1).setChecked(true)

        }
    }

    private fun loadFragment(fragment: Fragment) {
        // load fragment
        val transaction = supportFragmentManager.beginTransaction()
        transaction.replace(R.id.container, fragment)
        transaction.addToBackStack(null)
        transaction.commit()
    }

    private fun initViews() {
        mContext = this@DashboardActivity
        mActivity = this@DashboardActivity
        mViewModel.setNavigator(this)

        //loadFragment(CouponTabFragment.newInstance())
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_dashboard
    }

    override fun getViewModel(): DashboardViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(DashboardViewModel::class.java)
        return mViewModel
    }

    override fun getBindingVariable(): Int {
       return BR.viewModel
    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {

    }

    override fun onResponse(eventType: String, response: String) {

    }

    override fun onRequestFailed(eventType: String, response: String) {

    }

    override fun onRequestRetry() {

    }

    override fun onSessionExpire() {

    }

    override fun onMinorUpdate() {

    }

    override fun onAppHardUpdate() {

    }

    override fun noNetwork() {

    }
}