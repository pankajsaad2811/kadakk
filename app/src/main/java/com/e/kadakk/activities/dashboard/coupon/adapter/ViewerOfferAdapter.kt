package com.e.kadakk.activities.dashboard.coupon.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.coupon.CouponTabViewModel
import com.e.kadakk.activities.dashboard.coupon.model.CouponTabModel
import com.e.kadakk.base.DeviceUtil
import com.e.kadakk.databinding.ItemCouponOfferViewerBinding
import com.e.kadakk.utils.GlideApp


class ViewerOfferAdapter(context: Context, viewModel: CouponTabViewModel, outerPosition: Int) :
    RecyclerView.Adapter<ViewerOfferAdapter.ViewHolder>() {
    lateinit var mContext: Context
    lateinit var mViewModel: CouponTabViewModel
    var mOuterPosition: Int
    var mList: ArrayList<CouponTabModel.Data.ByCategory.CatOffer>  = ArrayList<CouponTabModel.Data.ByCategory.CatOffer>()

    init {
        mContext = context
        mViewModel = viewModel
        mOuterPosition = outerPosition
    }

    fun setList(list: ArrayList<CouponTabModel.Data.ByCategory.CatOffer>) {
        mList = ArrayList<CouponTabModel.Data.ByCategory.CatOffer>()
        mList = list

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = DataBindingUtil.inflate<ItemCouponOfferViewerBinding>(
            LayoutInflater.from(mContext),
            R.layout.item_coupon_offer_viewer, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
//    return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = mList.get(position)

        holder.binding!!.tvTime.setText(data.time+" - "+data.calory)
        holder.binding!!.tvName.setText(data.title)

        GlideApp.with(mContext)
            .load(data.offerImage)
            .into( holder.binding!!.ivImage)


        if (data.isFavourite == 1){

            holder.binding!!.ivLike.setBackgroundResource(R.drawable.rounded_favourite_bg_red)

        }else{
            holder.binding!!.ivLike.setBackgroundResource(R.drawable.rounded_favourite_bg_gray)
        }

        setParam(holder)

        holder.setViewModel(position , mViewModel, mOuterPosition,data)
    }

    fun setParam(holder: ViewHolder) {
        var width: Int = (DeviceUtil.getScreenWidth(mContext)).toInt()

//        var height = RelativeLayout.LayoutParams.WRAP_CONTENT
        var height = (width/2).toInt()

        var params = LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT)
        holder.binding!!.cvRow.layoutParams = params


        var coverImageparams = FrameLayout.LayoutParams(width, (height).toInt())
        holder.binding!!.ivImage.layoutParams = coverImageparams

//        params.setMargins(0, 0, 10, 0)
//        holder.binding!!.cvItemRoot.layoutParams = params
//        holder.binding!!.ivCoverImage.layoutParams = coverImageparams


    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ItemCouponOfferViewerBinding>(itemView)

        fun setViewModel(position: Int, viewModel : CouponTabViewModel, outerPosition: Int, data:  CouponTabModel.Data.ByCategory.CatOffer)
        {
            binding!!.position = position
            binding!!.viewModel = viewModel
            binding!!.outerPosition = outerPosition
            binding!!.dataItem = data
            binding!!.setVariable(BR.viewModel , viewModel)
            binding!!.executePendingBindings()
        }

    }



}