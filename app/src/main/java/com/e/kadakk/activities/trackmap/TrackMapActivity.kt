package com.e.kadakk.activities.trackmap

import android.Manifest
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.app.ActivityCompat
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityTrackMapBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.GlideApp
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions


/**
 * Created by Satish Patel on 12,December,2020
 */
class TrackMapActivity : BaseActivity<ActivityTrackMapBinding, TrackMapViewModel>(),
    TrackMapNavigator, ToolBarNavigator, serverResponseNavigator, OnMapReadyCallback
{

    var TAG: String = TrackMapActivity::class.java.simpleName
    var mToolBarViewModel: ToolBarViewModel? = null;
    public lateinit var mBinding: ActivityTrackMapBinding
    private var mContext: Context? = null
    private var mViewModel: TrackMapViewModel? = null
    var mShowNetworkDialog: Dialog? = null
    lateinit var mSessionPref: SessionPreferences
    var mMap: GoogleMap? =null
    var offerImage = ""
    var shopName = ""
    var contact = ""
    var email = ""
    var latitude = ""
    var longitude = ""

    /**
     * The current location.
     */
    private var mLocation: Location? = null

    // Used in checking for runtime permissions.
    private val REQUEST_PERMISSIONS_REQUEST_CODE = 34

    /**
     * Provides access to the Fused Location Provider API.
     */
    private var mFusedLocationClient: FusedLocationProviderClient? = null


    companion object {
        fun getIntent(context: Context, bundle: Bundle): Intent {
            val intent = Intent(context, TrackMapActivity::class.java)
            intent.putExtra("bundle-data", bundle)
            return intent
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_track_map
    }

    override fun getViewModel(): TrackMapViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(TrackMapViewModel::class.java)

        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)
         getIntentData()
        getControls()
        //mViewModel!!.profileAPI()

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        // Check that the user hasn't revoked permissions by going to Settings.

        // Check that the user hasn't revoked permissions by going to Settings.
        if (checkPermissions()) {
            //checkGPSEnable()
            getLastLocation()
        } else {
            requestPermissions()
        }


        val mapFragment = supportFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    /**
     * Returns the current state of the permissions needed.
     */
    private fun checkPermissions(): Boolean {
        return PackageManager.PERMISSION_GRANTED == ActivityCompat.checkSelfPermission(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )
    }

    private fun requestPermissions() {

        val shouldProvideRationale = ActivityCompat.shouldShowRequestPermissionRationale(
            this,
            Manifest.permission.ACCESS_FINE_LOCATION
        )

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            Log.i(TAG, "Displaying permission rationale to provide additional context.")

        } else {
            Log.i(TAG, "Requesting permission")
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(
                this@TrackMapActivity,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_PERMISSIONS_REQUEST_CODE
            )
        }
    }

    private fun getLastLocation() {
        try {
            mFusedLocationClient!!.lastLocation
                .addOnCompleteListener { task ->
                    if (task.isSuccessful && task.result != null) {
                        mLocation = task.result
                        addMarker()
                    } else {
                        Log.w(TAG, "Failed to get location.")
                    }
                }
        } catch (unlikely: SecurityException) {
            Log.e(TAG, "Lost location permission.$unlikely")
        }
    }

    private fun addMarker() {
        try {
            Log.e(
                TAG,
                "location data. speed=" + mLocation!!.speed + " altitude=" + mLocation!!.altitude
            )
            val sydney = LatLng(mLocation!!.latitude, mLocation!!.longitude)
            mMap!!.addMarker(MarkerOptions().position(sydney).title("My location"))
            mMap!!.animateCamera(CameraUpdateFactory.newLatLngZoom(sydney, 12.0f))


            val shop = LatLng(latitude.toDouble(), longitude.toDouble())
            mMap!!.addMarker(MarkerOptions().position(shop).title(shopName))


            // Add polylines to the map.
            // Polylines are useful to show a route or some other connection between points.
            val polyline1 = mMap!!.addPolyline(
                PolylineOptions()
                    .clickable(true)
                    .add(
                        LatLng(mLocation!!.latitude, mLocation!!.longitude),
                        LatLng(latitude.toDouble(), longitude.toDouble())
                    )
            )

        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if(requestCode == REQUEST_PERMISSIONS_REQUEST_CODE){

            if (grantResults.size <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                Log.i(TAG, "User interaction was cancelled.")
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getLastLocation()
            }
        }
    }

    /**
     * Get intent data
     */
    private fun getIntentData() {

        if (intent.getBundleExtra("bundle-data") != null) {
            offerImage = intent.getBundleExtra("bundle-data")!!.getString("offer_image")!!
            shopName = intent.getBundleExtra("bundle-data")!!.getString("shop_name")!!
            email = intent.getBundleExtra("bundle-data")!!.getString("email")!!
            contact = intent.getBundleExtra("bundle-data")!!.getString("contact")!!
            latitude = intent.getBundleExtra("bundle-data")!!.getString("latitude")!!
            longitude = intent.getBundleExtra("bundle-data")!!.getString("longitude")!!
        }
    }

    override fun onCallClick() {
        val intent = Intent(Intent.ACTION_CALL, Uri.parse("tel:$contact"))
        startActivity(intent)
    }

    override fun onMessageClick() {
        val intent = Intent(Intent.ACTION_SENDTO)
        intent.data = Uri.parse("mailto:") // only email apps should handle this

        intent.putExtra(Intent.EXTRA_EMAIL, email)
        intent.putExtra(Intent.EXTRA_SUBJECT, "")
        if (intent.resolveActivity(packageManager) != null) {
            startActivity(intent)
        }
    }

    override fun onMapReady(googleMap: GoogleMap?) {
        mMap = googleMap
    }


    fun getControls() {
        mContext = this@TrackMapActivity
        mSessionPref = SessionPreferences(mContext!!)
        mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)

        mBinding.layoutToolbar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.layoutToolbar.tvTitle.text = ""//getAppString(R.string.account_info)
        mBinding.layoutToolbar.ibNotification.visibility = View.INVISIBLE

        mBinding.layoutToolbar.ibBack.visibility = View.VISIBLE
        //mBinding.layoutToolbar.ivHeaderLogo.visibility = View.GONE

        mBinding.tvUsername.text = shopName
        mBinding.tvPhoneNumber.text = contact

        GlideApp.with(mContext!!)
            .load(offerImage)
            .into(mBinding!!.ivProfile)

    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {

        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })

    }

    override fun onResponse(eventType: String, response: String) {
        try {
            showLoaderOnRequest(false)
            /*if (eventType == AppConstants.GET_PROFILE_API){
                val mBean = Gson().fromJson(response, AccountInformationModel::class.java)
                mBinding.tvName.text = mBean.data.profile.fullName
                mBinding.tvEmail.text = mBean.data.profile.userEmail
                mBinding.tvPhoneNumber.text = mBean.data.profile.userPhone
                mBinding.tvDateOfBirth.text = mBean.data.profile.dob
            }*/
        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: " + e.message)
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }



}