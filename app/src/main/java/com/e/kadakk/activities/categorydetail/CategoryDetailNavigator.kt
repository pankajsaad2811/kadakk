package com.e.kadakk.activities.categorydetail

import com.e.kadakk.activities.categorydetail.model.CategoryDetailModel


interface CategoryDetailNavigator {
    fun grabClick()
    fun showMessage(msg:  String)
    fun onFavoriteClick(position: Int,  data: CategoryDetailModel.Data.SimilarOffer)
    fun onItemClick(position: Int,  data: CategoryDetailModel.Data.SimilarOffer)
    fun onGrabNowClick(position: Int,  data: CategoryDetailModel.Data.SimilarOffer)

}