package com.e.kadakk.activities.tutorial

import android.app.Application
import com.e.kadakk.R
import com.e.kadakk.activities.tutorial.bean.TutorialListModel
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.server.serverResponseNavigator
import java.util.*


class TutorialViewModel (application: Application, serverResponse: serverResponseNavigator) : BaseViewModel<TutorialNavigator>(application){
    lateinit var serverResponse: serverResponseNavigator;
    var mApplication: Application;
    init {
        mApplication = application;
        this.serverResponse = serverResponse;
    }

    //these method is for adding data to the tutorial list
    fun getTutorialList(): ArrayList<TutorialListModel.TutorialListBean> {
        val list: ArrayList<TutorialListModel.TutorialListBean> = ArrayList()
        try {
            list.add(
                TutorialListModel.TutorialListBean(
                    "1",
                    R.drawable.walkthrough_one,
                    getStringfromVM(R.string.walkthrough_title_1),
                    getStringfromVM(R.string.walkthrough_msg_1)
                )
            )
            list.add(
                TutorialListModel.TutorialListBean(
                    "2",
                    R.drawable.walkthrough_two,
                    getStringfromVM(R.string.walkthrough_title_2),
                    getStringfromVM(R.string.walkthrough_msg_2)
                )
            )
            list.add(
                TutorialListModel.TutorialListBean(
                    "3",
                    R.drawable.walkthrough_three,
                    getStringfromVM(R.string.walkthrough_title_3),
                    getStringfromVM(R.string.walkthrough_msg_3)
                )
            )
        } catch (ex: Exception) {
            ex.printStackTrace()
        }
        return list
    }

    fun skipClick()
    {
        getNavigator().skipClick()
    }
    fun getStartedClick()
    {
        getNavigator().getStartedClick()
    }
}