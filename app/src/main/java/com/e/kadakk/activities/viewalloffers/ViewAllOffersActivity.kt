package com.e.kadakk.activities.viewalloffers

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.GridLayoutManager
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.checkout.CheckOutActivity
import com.e.kadakk.activities.dashboard.coupon.model.CouponTabModel
import com.e.kadakk.activities.dashboard.search.SearchTabFragment
import com.e.kadakk.activities.otp.OTPActivity

import com.e.kadakk.activities.signup.model.RegistrationModel
import com.e.kadakk.activities.viewalloffers.adapter.ViewAllOfferAdapter
import com.e.kadakk.activities.viewalloffers.model.ViewAllOfferModel
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.base.GridSpacesItemDecoration
import com.e.kadakk.country.CountryListResponse
import com.e.kadakk.databinding.ActivityViewAllOffersBinding
import com.e.kadakk.databinding.AddCategoryItemDialogBinding

import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.*
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import kotlin.collections.ArrayList

class ViewAllOffersActivity : BaseActivity<ActivityViewAllOffersBinding, ViewAllOffersViewModel>(),
    serverResponseNavigator, ViewAllOffersNavigator, ToolBarNavigator {
    private lateinit var mViewModel: ViewAllOffersViewModel
    private lateinit var mBinding: ActivityViewAllOffersBinding
    private lateinit var mContext: Context
    private lateinit var mActivity: Activity
    private val TAG = ViewAllOffersActivity::class.java.simpleName
    private lateinit var mSessionPref: SessionPreferences
    private lateinit var mCountryList: ArrayList<CountryListResponse.Data>
    private var mShowNetworkDialog: Dialog? = null
    private lateinit var mToolBarViewModel: ToolBarViewModel
    var mFavoriteItemList: ArrayList<ViewAllOfferModel.Data> = ArrayList()
    lateinit var mFavoriteItemAdapter: ViewAllOfferAdapter


    /*
* true means show dialog else do not show dialog  after get  the response from API
* */
    private var isOpenCountryDialog: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel
        getIntentData()
        initViews()

    }

    companion object {
        fun getIntent(context: Context, bundle: Bundle): Intent {
            var intent = Intent(context, ViewAllOffersActivity::class.java)
            intent.putExtra("ViewAllOffers-bundle", bundle)
            return intent;
        }
    }


    //    "{
//    ""cat_id"":""0"",
//    ""category_name"":""raa""
//}"
    fun getIntentData() {

        if (intent.getBundleExtra("ViewAllOffers-bundle") != null) {

            mViewModel.catId = intent.getBundleExtra("ViewAllOffers-bundle")!!.getString("catId")!!
            mViewModel.categoryName =
                intent.getBundleExtra("ViewAllOffers-bundle")!!.getString("categoryName")!!
        }

    }


    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_view_all_offers
    }

    override fun getViewModel(): ViewAllOffersViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ViewAllOffersViewModel::class.java)
        return mViewModel
    }


    private fun initViews() {
        mContext = this@ViewAllOffersActivity
        mSessionPref = SessionPreferences(mContext)
        mActivity = this@ViewAllOffersActivity
        mViewModel.setNavigator(this)
        mViewModel.setSessionPref(mSessionPref)
        mCountryList = ArrayList();


        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding = getViewDataBinding()
        mBinding.layoutToolbar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)

        mBinding.layoutToolbar.ibBack.visibility = View.VISIBLE
        mBinding.layoutToolbar.tvTitle.text = mViewModel.categoryName



        mFavoriteItemAdapter = ViewAllOfferAdapter(mContext, mViewModel)
        mBinding.rvList.layoutManager = GridLayoutManager(mContext, 2)
        mBinding.rvList.adapter = mFavoriteItemAdapter
        val spanCount =
            SearchTabFragment.SPECIAL_ITEM_ROW // 3 columns
        val spacing = 10 // 50px
        val includeEdge = false
        mBinding.rvList.addItemDecoration(
            GridSpacesItemDecoration(
                spanCount,
                spacing,
                includeEdge
            )
        )



        mViewModel.callViewAllOfferCategoryAPI()


    }

    /*
        * show all common message
        * */
    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }
            });
    }

    override fun onFavoriteClick(position: Int) {

        mViewModel!!.mPosition = position

        mViewModel!!.callFavoriteAPI(mFavoriteItemList.get(position).offerId)

    }

    override fun onGrabClick(position: Int) {

        showGrabDialog(mFavoriteItemList.get(position))
    }

    fun showGrabDialog(data: ViewAllOfferModel.Data) {

        val dialog = Dialog(mContext!!)//, R.style.AppListDialogTheme)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.getWindow()!!.getAttributes().windowAnimations = R.style.Dialog_WindowAnimation;

        val binding = DataBindingUtil.inflate<AddCategoryItemDialogBinding>(
            LayoutInflater.from(mContext!!),
            R.layout.add_category_item_dialog, null, false
        )
        dialog.window!!.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.setContentView(binding.getRoot())

        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)


//      binding.tvAddressLocation.text = mBean!!.data.offerDetails.
        //     binding.tvTitle.text = mBean!!.data.offerDetails.discountType
        binding.tvOff.text =data.discountValue+"% off"
        //  binding.tvAmount.text = data.offerDetails.onMinPurchase
        binding.tvItemName.text =data.title

        GlideApp.with(mContext!!)
            .load(data.offerImage)
            .into( binding!!.itemImage)

        binding.btnCancel.setOnClickListener{
            dialog.dismiss()
        }

        binding.btnPlus.setOnClickListener {

            val qty :String = binding.tvQuantity.text.toString()

            if(Integer.parseInt(qty)<5){
                val newQty = Integer.parseInt(qty)+1
                binding.tvQuantity.text = ""+newQty
            }
        }

        binding.btnMinus.setOnClickListener {

            val qty :String = binding.tvQuantity.text.toString()

            if(Integer.parseInt(qty)>1){
                val newQty = Integer.parseInt(qty)-1
                binding.tvQuantity.text = ""+newQty
            }
        }

        binding.btnGrabNow.setOnClickListener {
            mViewModel!!.addToCardAPI(binding.tvQuantity.text.toString(),data.offerId)
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)
        if (eventType == AppConstants.VIEW_ALL_OFFER_API) {

            val bean = Gson().fromJson(response, ViewAllOfferModel::class.java)
            mFavoriteItemList = bean.data as ArrayList<ViewAllOfferModel.Data>;
            mFavoriteItemAdapter.setList(mFavoriteItemList)

        } else if(eventType == AppConstants.ADD_WISH_LIST_API){


            if (mFavoriteItemList.get(mViewModel!!.mPosition).isFavourite == 0) {
                mFavoriteItemList.get(mViewModel!!.mPosition).isFavourite =
                    1
            }else{
                mFavoriteItemList.get(mViewModel!!.mPosition).isFavourite =
                    0
            }
            mFavoriteItemAdapter.notifyItemChanged(mViewModel!!.mPosition)

        }else  if (eventType == AppConstants.ADD_TO_CARD_API){
            startActivity(CheckOutActivity.getIntent(mContext!!))
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            response,
            false
        )

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
        BaseActivity.showErrorMessage(
            mContext,
            getString(R.string.session_expire_msg),
            true
        )
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
        showMinorUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            false
        )
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
        showHardUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            true
        )
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            getString(R.string.No_internet_connection),
            false
        )
    }


    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }

}