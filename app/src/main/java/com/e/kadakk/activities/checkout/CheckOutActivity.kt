package com.e.kadakk.activities.checkout

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.changedeliveryaddress.ChangeDeliveryAddressActivity
import com.e.kadakk.activities.checkout.model.CheckoutModel
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityCheckoutBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import org.json.JSONObject
import java.util.*


class CheckOutActivity : BaseActivity<ActivityCheckoutBinding, CheckOutViewModel>(),
    serverResponseNavigator, CheckoutNavigator, ToolBarNavigator, AdapterView.OnItemSelectedListener{

    var mViewModel: CheckOutViewModel? = null
    lateinit var mBinding: ActivityCheckoutBinding
    lateinit var mContext: Context
    var mShowNetworkDialog: Dialog? = null
    var mToolBarViewModel: ToolBarViewModel? = null
    var mBean :CheckoutModel? = null
    private lateinit var mSessionPref: SessionPreferences
    private var isOpenCountryDialog: Boolean = false
    private var isDefaultAddress: String = "no"
    private var addressType: String = ""

    companion object {
        fun getIntent(context: Context): Intent {
            val intent = Intent(context, CheckOutActivity::class.java);
            return intent
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_checkout
    }

    override fun getViewModel(): CheckOutViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(CheckOutViewModel::class.java)
        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)
        getControls()
    }

    fun getControls() {
        mContext = this@CheckOutActivity
        mSessionPref = SessionPreferences(mContext!!)
        mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.toolBar.tvTitle.text = getAppString(R.string.checkout)
        mBinding.toolBar.ibNotification.visibility = View.INVISIBLE
        mBinding.toolBar.ibBack.visibility = View.VISIBLE
        mBinding.toolBar.ivHeaderLogo.visibility = View.GONE

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.time_slot,
            android.R.layout.simple_spinner_item)

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mBinding.spTimeSlot.adapter = adapter
        mBinding.spTimeSlot.onItemSelectedListener = this

        mViewModel!!.callCardDetailsAPI()
    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
    }

    override fun confirmOrderClick() {
            mViewModel!!.callConfirmOrderAPI(mBean!!.data.order_info.orderId)
    }

    override fun changeAddressClick() {
        val bundle: Bundle = Bundle()
        bundle.putString("from_were", "checkout")
        startActivityForResult(ChangeDeliveryAddressActivity.getIntent(mContext!!,bundle),101)
    }


    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) { //after a place is searched
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {
            var address_id = data!!.extras!!.get("address_id")
            var username = data!!.extras!!.get("username")
            var address_type = data!!.extras!!.get("address_type")
            var address = data!!.extras!!.get("address")
            var latitude = data!!.extras!!.get("latitude")
            var longitude = data!!.extras!!.get("longitude")
            var country_code = data!!.extras!!.get("country_code")
            var mobile = data!!.extras!!.get("mobile")
            var default_address = data!!.extras!!.get("default_address")
            var created_at = data!!.extras!!.get("created_at")

            mBinding.tvAddressLocation.text = ""+address
            mBinding.tvAddressName.text = ""+username
            mBinding.tvAddressPhone.text = ""+mobile

        }

    }

    override fun deliveryDateClick() {
        AppUtil.hideSoftKeyBoard((mContext as Activity?)!!, mBinding.etDateOfDelivery)
        onPickDate()
    }

    fun onPickDate(){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            // Display Selected date in textbox
//            mBinding.tvDate.setText(DateUtil.getDateDDMMMYYYY(""+dayOfMonth+"-"+(monthOfYear+1)+"-"+year))
            mBinding.etDateOfDelivery.setText((""+dayOfMonth+"-"+(monthOfYear+1)+"-"+year))
            mViewModel!!.deliveryDayStr = (""+dayOfMonth+"-"+(monthOfYear+1)+"-"+year)

        }, year, month, day)

        val cc = Calendar.getInstance()
        //cc.add(Calendar.YEAR,  -18)
        dpd.datePicker.minDate =cc.timeInMillis

        dpd.show()
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        addressType = parent?.getItemAtPosition(position).toString()
    }

    override fun onResponse(eventType: String, response: String) {
        try {
            showLoaderOnRequest(false)

            if(eventType == AppConstants.CARD_ITEM_LIST_API){
                mBean = Gson().fromJson(response, CheckoutModel::class.java)
                setData()
            }else if(eventType == AppConstants.CONFIRM_ORDER_API){
                val json = JSONObject(response)

                DialogUtil.okCancelDialog(mContext!!,
                    getAppString(R.string.app_name),
                    json.optString("message"),
                    getAppString(R.string.ok),
                    getString(R.string.no),
                    true,
                    false,
                    object : DialogUtil.Companion.selectOkCancelListener {
                        override fun okClick() {
                            finish()
                        }

                        override fun cancelClick() {

                        }

                    })
            }

        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: "+e.message)
        }
    }

    private fun setData(){
        mBinding.tvOderId.text = "#"+mBean!!.data.order_info.orderId
        mBinding.tvNameOfAddress.text = mBean!!.data.defaultAddress.addressType
        mBinding.tvAddressLocation.text = mBean!!.data.defaultAddress.address
        mBinding.tvAddressName.text = mBean!!.data.defaultAddress.username
        mBinding.tvAddressPhone.text = mBean!!.data.defaultAddress.countryCode+" "+mBean!!.data.defaultAddress.mobile
        mBinding.tvTotalItem.text = ""+mBean!!.data.order_info.totalItems +" items"
        mBinding.tvTotalPrice.text = ""+mBean!!.data.order_info.total_price
        mBinding.tvDeliveryFee.text = ""+mBean!!.data.order_info.deliveryFees
        mBinding.tvTotalBill.text = ""+mBean!!.data.order_info.totalBill
    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {
                    finish()
                }

                override fun cancelClick() {

                }

            })
      }

}