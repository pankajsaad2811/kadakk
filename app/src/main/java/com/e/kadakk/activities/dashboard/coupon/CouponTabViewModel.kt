package com.e.kadakk.activities.dashboard.coupon

import android.app.Application
import android.text.TextUtils
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.coupon.model.CouponTabModel
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.Validator
import com.e.kadakk.utils.server.*
import java.util.regex.Pattern

class CouponTabViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<CouponTabNavigator>(application) {
    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface

    var emailStr: String = ""
    var passwordStr: String = ""
    lateinit var mSessionPref: SessionPreferences

    var mPosition: Int = 0
    var mOuterPosition: Int = 0

    private val PASSWORD_PATTERN: Pattern =
        Pattern.compile("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#\$%^&*]).{8,20})")

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    public fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }


    fun onSearchClick(){
        getNavigator().onSearchClick()
    }

    fun onFavoriteClick(position: Int, innerPosition:Int, data: CouponTabModel.Data.ByCategory.CatOffer){

        getNavigator().onFavoriteClick(position, innerPosition, data)
    }


    fun onCategoryItemClick(position: Int){

        getNavigator().onCategoryItemClick(position)
    }

    fun onBennerItemClick(position: Int, data: CouponTabModel.Data.ByCategory.CatOffer){

        getNavigator().onBennerItemClick(position, data)
    }

    fun onOfferItemClick(position: Int, data: CouponTabModel.Data.ByCategory.CatOffer){

        getNavigator().onOfferItemClick(position, data)
    }

    fun onBennerGrabNowClick(position: Int,data: CouponTabModel.Data.ByCategory.CatOffer){

        getNavigator().onBennerGrabNowClick(position,data)
    }

   fun onViewAllClick(position: Int){

        getNavigator().onViewAllClick(position)
    }

    fun addToCardAPI(qty : String,offerId: String) {

        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        //get request

        /*
        "{
        ""user_id"":""S7MytlIyMjRRsgYA"",
        ""offer_id"": ""S7MytFIyVLIGAA==""
}"
          */
        val jsonObj = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "offer_id", offerId,
            "quantity",qty
        )

        mRxApiCallHelper.call(mApiInterface.postRequest(AppConstants.ADD_TO_CARD_API, jsonObj!!,  mSessionPref.getFirebaseRegIdPrefValue() , "", mSessionPref.mLoginData!!.data.login.authorization, "", "0.0", "0.0"),
            AppConstants.ADD_TO_CARD_API, serverResponse)

    }

    /*
    * perform validation
    * */
    fun isValidAll() :String{
        if (TextUtils.isEmpty(emailStr)) {
            return getStringfromVM(R.string.Please_enter_email_id_or_mobile)
        }else if(!Validator.isValidMobile(emailStr) && !Validator.isEmailValid(emailStr)){
            return getStringfromVM(R.string.Please_enter_valid_email_Id)
        }else if(Validator.isValidMobile(emailStr) &&  !Validator.isMobileNumberValid(emailStr)){
            return getStringfromVM(R.string.Please_enter_valid_phone_number)
        }else if (TextUtils.isEmpty(passwordStr)) {
            return  getStringfromVM(R.string.Please_enter_password)
        } else if (passwordStr.length < 8) {
            return   getStringfromVM(R.string.Password_should_be_min_8_character)
        } /*else if (!Validator.isValidPassword(passwordStr)) {
            return   getStringfromVM(R.string.Please_enter_valid_password)
        } */else {
            return   "";
        }


    }


    /******* API implementation ******/
    fun callCouponOfferAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        //get request

        /*
        "{
        ""user_id"":""S7MytlIyMjRRsgYA""
}"
          */
        val jsonObj = JsonElementUtil.getJsonObject("user_id", mSessionPref.mLoginData!!.data.login.userId);

        mRxApiCallHelper.call(mApiInterface.postRequest(AppConstants.OFFER_HOME_OFFERS_API, jsonObj!!,  mSessionPref.getFirebaseRegIdPrefValue() , "", mSessionPref.mLoginData!!.data.login.authorization, "", "0.0", "0.0"),
            AppConstants.OFFER_HOME_OFFERS_API, serverResponse)

    }

 fun callFavoriteAPI(offerId: String) {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        //get request

        /*
        "{
        ""user_id"":""S7MytlIyMjRRsgYA"",
        ""offer_id"": ""S7MytFIyVLIGAA==""
}"
          */
        val jsonObj = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "offer_id", offerId

        );

        mRxApiCallHelper.call(mApiInterface.postRequest(AppConstants.ADD_WISH_LIST_API, jsonObj!!,  mSessionPref.getFirebaseRegIdPrefValue() , "", mSessionPref.mLoginData!!.data.login.authorization, "", "0.0", "0.0"),
            AppConstants.ADD_WISH_LIST_API, serverResponse)

    }



}