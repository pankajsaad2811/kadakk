package com.e.kadakk.activities.categorydetail.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.categorydetail.CategoryDetailViewModel
import com.e.kadakk.activities.categorydetail.model.CategoryDetailModel
import com.e.kadakk.activities.dashboard.search.SearchTabViewModel
import com.e.kadakk.activities.dashboard.search.model.SearchModel
import com.e.kadakk.base.DeviceUtil
import com.e.kadakk.databinding.ItemSearchTabBinding
import com.e.kadakk.databinding.ItemSimilarOfferBinding
import com.e.kadakk.utils.GlideApp


class SimilarOfferAdapter(context: Context, viewModel: CategoryDetailViewModel) :
    RecyclerView.Adapter<SimilarOfferAdapter.ViewHolder>() {
    lateinit var mContext: Context
    lateinit var mViewModel: CategoryDetailViewModel
    var mList: ArrayList<CategoryDetailModel.Data.SimilarOffer>  = ArrayList<CategoryDetailModel.Data.SimilarOffer>()

    init {
        mContext = context
        mViewModel = viewModel
    }

    fun setList(list: ArrayList<CategoryDetailModel.Data.SimilarOffer>) {
        mList = ArrayList<CategoryDetailModel.Data.SimilarOffer>()
        mList = list

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = DataBindingUtil.inflate<ItemSimilarOfferBinding>(
            LayoutInflater.from(mContext),
            R.layout.item_similar_offer, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
//    return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = mList.get(position)

        holder.binding!!.tvName.setText(data.title)
        holder.binding!!.tvOffer.setText(data.discountValue)

        GlideApp.with(mContext)
            .load(data.offerImage)
            .into( holder.binding!!.ivImage)


        if (data.isFavourite == 1){

            holder.binding!!.ivLike.setBackgroundResource(R.drawable.rounded_favourite_bg_red)

        }else{
            holder.binding!!.ivLike.setBackgroundResource(R.drawable.rounded_favourite_bg_gray)
        }


        setParam(holder)
        holder.setViewModel(position , mViewModel, data)
    }

    fun setParam(holder: ViewHolder) {
        var width: Int = (DeviceUtil.getScreenWidth(mContext) / 2.4).toInt()


        var height = (DeviceUtil.getScreenWidth(mContext) / 3.5).toInt()

        var params = LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT)
        holder.binding!!.llTrending.layoutParams = params


        var coverImageparams = LinearLayout.LayoutParams(width, (height).toInt())
        holder.binding!!.flImage.layoutParams = coverImageparams



    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ItemSimilarOfferBinding>(itemView)

        fun setViewModel(position: Int, viewModel : CategoryDetailViewModel, data: CategoryDetailModel.Data.SimilarOffer)
        {
            binding!!.position = position
            binding!!.viewModel = viewModel
            binding!!.dataItem = data
            binding!!.setVariable(BR.viewModel , viewModel)
            binding!!.executePendingBindings()
        }

    }



}