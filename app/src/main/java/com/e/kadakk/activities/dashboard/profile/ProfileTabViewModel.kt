package com.e.kadakk.activities.dashboard.profile

import android.app.Application
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator



class ProfileTabViewModel (application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<ProfileTabNavigator>(application){

    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application

    lateinit var mSessionPref: SessionPreferences


    public fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    fun onLogoutClick(){
        getNavigator().logoutClick()
    }

    fun onEditProfileClick(){
        getNavigator().editProfileClick()
    }

    fun onAccountInformationClick(){
        getNavigator().accountInformationClick()
    }

    fun onMyOrderClick(){
        getNavigator().myOrderClick()
    }

    fun onPaymentMethodClick(){
        getNavigator().paymentMethodClick()
    }

    fun onDeliveryAddressClick(){
        getNavigator().deliveryAddressClick()
    }

    fun onSettingClick(){
        getNavigator().settingClick()
    }

    fun onHelpCenterClick(){
        getNavigator().helpCenterClick()
    }

    fun onAboutUsClick(){
        getNavigator().aboutUsClick()
    }

    fun onShareAppClick(){
        getNavigator().shareAppClick()
    }

}