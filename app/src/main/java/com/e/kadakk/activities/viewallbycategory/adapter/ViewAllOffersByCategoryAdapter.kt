package com.e.kadakk.activities.viewallbycategory.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.search.adapter.SearchTabAdapter
import com.e.kadakk.activities.viewallbycategory.ViewAllOffersByCategoryViewModel
import com.e.kadakk.activities.viewallbycategory.model.ViewAllOffersByCategoryModel
import com.e.kadakk.base.DeviceUtil
import com.e.kadakk.databinding.ItemViewAllOfferByCategoryBinding
import com.e.kadakk.utils.GlideApp


class ViewAllOffersByCategoryAdapter(context: Context, viewModel: ViewAllOffersByCategoryViewModel) :
    RecyclerView.Adapter<ViewAllOffersByCategoryAdapter.ViewHolder>() {
    lateinit var mContext: Context
    lateinit var mViewModel: ViewAllOffersByCategoryViewModel
    var mList: ArrayList<ViewAllOffersByCategoryModel.Data.ByCategory>  = ArrayList<ViewAllOffersByCategoryModel.Data.ByCategory>()

    init {
        mContext = context
        mViewModel = viewModel
    }

    fun setList(list: ArrayList<ViewAllOffersByCategoryModel.Data.ByCategory>) {
        mList = ArrayList<ViewAllOffersByCategoryModel.Data.ByCategory>()
        mList = list

        Log.e("cxvxcv", "SSSSS: "+mList.size)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = DataBindingUtil.inflate<ItemViewAllOfferByCategoryBinding>(
            LayoutInflater.from(mContext),
            R.layout.item_view_all_offer_by_category, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
//    return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = mList.get(position)

        holder.binding!!.tvName.setText(data.title)
        holder.binding!!.tvOffer.setText(data.discountValue)

        GlideApp.with(mContext)
            .load(data.offerImage)
            .into( holder.binding!!.ivImage)



        if (data.isFavourite == 1){

            holder.binding!!.ivLike.setBackgroundResource(R.drawable.rounded_favourite_bg_red)

        }else{
            holder.binding!!.ivLike.setBackgroundResource(R.drawable.rounded_favourite_bg_gray)
        }

        setParam(holder)
        holder.setViewModel(position , mViewModel)
    }

    fun setParam(holder: ViewHolder) {
        var width: Int = (DeviceUtil.getScreenWidth(mContext) / 2.4).toInt()


        var height = (DeviceUtil.getScreenWidth(mContext) / 3.5).toInt()

        var params = LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT)
        holder.binding!!.llTrending.layoutParams = params


        var framparams = LinearLayout.LayoutParams(width, (height).toInt())
        holder.binding!!.flImage.layoutParams = framparams

        var coverImageparams = FrameLayout.LayoutParams(width, (height).toInt())
        holder.binding!!.ivImage.layoutParams = coverImageparams


    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ItemViewAllOfferByCategoryBinding>(itemView)

        fun setViewModel(position: Int, viewModel : ViewAllOffersByCategoryViewModel)
        {
            binding!!.position = position
            binding!!.viewModel = viewModel
//            binding!!.itemData = data
            binding!!.setVariable(BR.viewModel , viewModel)
            binding!!.executePendingBindings()
        }

    }



}