package com.e.kadakk.activities.adddileveryaddress


interface AddDeliveryAddressNavigator {
    fun showMessage(msg:  String)
    fun saveClick()
    fun countryCodeClick()
}