package com.e.kadakk.activities.dashboard.profile.accountInformations

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.profile.accountInformations.model.AccountInformationModel
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityAccountInformationBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson



class AccountInformationActivity : BaseActivity<ActivityAccountInformationBinding, AccountInformationViewModel>(),
    AccountInformationNavigator, ToolBarNavigator, serverResponseNavigator {

    var TAG: String = AccountInformationActivity::class.java.simpleName
    var mToolBarViewModel: ToolBarViewModel? = null;
    public lateinit var mBinding: ActivityAccountInformationBinding
    private var mContext: Context? = null
    private var mViewModel: AccountInformationViewModel? = null
    var mShowNetworkDialog: Dialog? = null
    lateinit var mSessionPref: SessionPreferences

    companion object {
        fun getIntent(context: Context): Intent {
            val intent = Intent(context, AccountInformationActivity::class.java)
            return intent
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_account_information
    }

    override fun getViewModel(): AccountInformationViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(AccountInformationViewModel::class.java)

        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)
       // getIntentData()
        getControls()
        mViewModel!!.profileAPI()
    }

    fun getControls() {
        mContext = this@AccountInformationActivity
        mSessionPref = SessionPreferences(mContext!!)
        mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)

        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.toolBar.tvTitle.text = getAppString(R.string.account_info)
        mBinding.toolBar.ibNotification.visibility = View.INVISIBLE

        mBinding.toolBar.ibBack.visibility = View.VISIBLE
        mBinding.toolBar.ivHeaderLogo.visibility = View.GONE

    }

    override fun onResume() {
        super.onResume()
    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {

        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })

    }

    override fun onResponse(eventType: String, response: String) {
        try {
            showLoaderOnRequest(false)
            if (eventType == AppConstants.GET_PROFILE_API){
                val mBean = Gson().fromJson(response, AccountInformationModel::class.java)
                mBinding.tvName.text = mBean.data.profile.fullName
                mBinding.tvEmail.text = mBean.data.profile.userEmail
                mBinding.tvPhoneNumber.text = mBean.data.profile.userPhone
                mBinding.tvDateOfBirth.text = mBean.data.profile.dob
            }
        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: "+e.message)
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }
}