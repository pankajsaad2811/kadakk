package com.e.kadakk.activities.changedeliveryaddress.model

import com.google.gson.annotations.SerializedName



class ChangeDeliveryAddressModel (
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("errorcode")
    val errorcode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("version")
    val version: Version
) {
    data class Data(
        @SerializedName("address_id")
        val addressID: String,
        @SerializedName("username")
        val userName: String,
        @SerializedName("address_type")
        val addressType: String,
        @SerializedName("address")
        val address: String,
        @SerializedName("latitude")
        val latitude: String,
        @SerializedName("longitude")
        val longitude: String,
        @SerializedName("country_code")
        val countryCode: String,
        @SerializedName("mobile")
        val mobile: String,
        @SerializedName("default_address")
        val defaultAddress: String,
        @SerializedName("created_at")
        val created_at: String
    )

    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )
}