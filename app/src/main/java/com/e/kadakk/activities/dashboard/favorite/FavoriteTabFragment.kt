package com.e.kadakk.activities.dashboard.favorite

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.categorydetail.CategoryDetailActivity
import com.e.kadakk.activities.dashboard.favorite.adapter.FavoriteTabAdapter
import com.e.kadakk.activities.dashboard.favorite.model.FavoriteTabModel


import com.e.kadakk.base.BaseActivity
import com.e.kadakk.base.BaseFragment
import com.e.kadakk.base.GridSpacesItemDecoration
import com.e.kadakk.databinding.FragmentFavoriteTabBinding
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson


class FavoriteTabFragment : BaseFragment<FragmentFavoriteTabBinding, FavoriteTabViewModel>(),
    serverResponseNavigator,
    FavoriteTabNavigator {


    lateinit var mContext: Context
    //lateinit var mHomeBean: HomeTabResponse.HomeTabBean;

    lateinit var mFavoriteTabAdapter: FavoriteTabAdapter
    var mList: ArrayList<FavoriteTabModel.Data> = ArrayList()


    var mShowNetworkDialog: android.app.Dialog? = null;
    var mOkCancelDialog: Dialog? = null;
    lateinit var mSessionPref: SessionPreferences


    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_favorite_tab
    }

    override fun getViewModel(): FavoriteTabViewModel {
        mViewModel =
            ViewModelProviders.of(this, ViewModelProviderFactory(activity!!.application, this))
                .get(FavoriteTabViewModel::class.java)

        return mViewModel!!

    }

    var mViewModel: FavoriteTabViewModel? = null
    lateinit var mBinding: FragmentFavoriteTabBinding;

    companion object {

        val SPECIAL_ITEM_ROW = 2;

        @JvmStatic
        fun newInstance() =
            FavoriteTabFragment().apply {

            }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel!!.setNavigator(this)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding = getViewDataBinding()
        getControls()
//        mViewModel!!.callCouponOfferAPI()
    }

    @SuppressLint("NewApi")
    fun getControls() {
        mContext = this@FavoriteTabFragment.activity!!
        mSessionPref = SessionPreferences(mContext)
        mViewModel!!.setSessionPref(mSessionPref)


        mFavoriteTabAdapter = FavoriteTabAdapter(mContext, mViewModel!!)
        mBinding.rvSearch.layoutManager = GridLayoutManager(mContext, 2)
        mBinding.rvSearch.adapter = mFavoriteTabAdapter
        val spanCount =
            SPECIAL_ITEM_ROW // 3 columns
        val spacing = 10 // 50px
        val includeEdge = false
        mBinding.rvSearch.addItemDecoration(
            GridSpacesItemDecoration(
                spanCount,
                spacing,
                includeEdge
            )
        )




        mViewModel!!.callFavoriteAPI()




    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    fun setData() {


    }


    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)

        try {

            if (eventType == AppConstants.MY_WISH_LIST_API) {

                var mBean = Gson().fromJson(response, FavoriteTabModel::class.java)
                mList = mBean.data as ArrayList<FavoriteTabModel.Data>
                mFavoriteTabAdapter.setList(mList)


            }else if(eventType == AppConstants.ADD_WISH_LIST_API){

                mList.removeAt(mViewModel!!.mPosition)
                mFavoriteTabAdapter.notifyItemRemoved(mViewModel!!.mPosition)

            }


        } catch (e: Exception) {
            AppUtil.showLogs("Exception", "onResponse: " + e.message)

        }

    }


    override fun onPause() {
        super.onPause()

    }


    override fun onResume() {
        super.onResume()

//        pageNo = 1
//        mViewModel!!.callChatUsersListAPI(pageNo)
    }


    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }


    override fun noNetwork() {
        showLoaderOnRequest(false)
        BaseActivity.showErrorMessage(
            mContext,
            getString(R.string.No_internet_connection),
            false
        )
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), "Session expire", getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

//                    startActivity(LoginActivity.getIntent(mContext))
//                    activity!!.finishAffinity()
//                    mSessionPref.clearSession()

                }

                override fun cancelClick() {

                }

            });
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun showMessage(msg: String) {

        mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }
            })
    }

    override fun onFavoriteClick(position: Int) {
        mViewModel!!.mPosition = position
        mViewModel!!.callFavoriteAPI(mList.get(position).offerId)
    }

    override fun onGrabNow(position: Int) {
        var bundle: Bundle = Bundle()
        bundle.putString("offerId", mList.get(position).offerId)
        startActivity(CategoryDetailActivity.getIntent(mContext, bundle))
    }

    override fun onDestroy() {
        super.onDestroy()

    }

}