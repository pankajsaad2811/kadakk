package com.e.kadakk.activities.dashboard.favorite


interface FavoriteTabNavigator {
    fun showMessage(msg:  String)
    fun onFavoriteClick(position: Int)
    fun onGrabNow(position: Int)
}