package com.e.kadakk.activities.dashboard

import android.app.Application
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.server.serverResponseNavigator



class DashboardViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<DashboardNavigator>(application) {

    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }



}