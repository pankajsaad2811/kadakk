package com.e.kadakk.activities.dashboard.profile

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.core.app.ActivityCompat.finishAffinity
import androidx.databinding.library.baseAdapters.BR
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.e.kadakk.R
import com.e.kadakk.activities.categorydetail.CategoryDetailActivity
import com.e.kadakk.activities.changedeliveryaddress.ChangeDeliveryAddressActivity
import com.e.kadakk.activities.checkout.CheckOutActivity
import com.e.kadakk.activities.dashboard.DashboardActivity
import com.e.kadakk.activities.dashboard.profile.accountInformations.AccountInformationActivity
import com.e.kadakk.activities.dashboard.profile.myorder.MyOrderActivity
import com.e.kadakk.activities.editprofile.EditProfileActivity
import com.e.kadakk.activities.login.LoginActivity
import com.e.kadakk.activities.notification.NotificationActivity


import com.e.kadakk.base.BaseFragment
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.databinding.FragmentProfileTabBinding

import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.GlideApp
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory



class ProfileTabFragment : BaseFragment<FragmentProfileTabBinding, ProfileTabViewModel>(),
    ProfileTabNavigator, serverResponseNavigator {

    var mViewModel: ProfileTabViewModel? = null
    lateinit var mBinding: FragmentProfileTabBinding
    lateinit var mContext: Context
    lateinit var mSessionPref: SessionPreferences

    companion object {

        @JvmStatic
        fun newInstance() = ProfileTabFragment().apply {

        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {

        return R.layout.fragment_profile_tab
    }

    override fun getViewModel(): ProfileTabViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(activity!!.application, this))
            .get(ProfileTabViewModel::class.java)

        return mViewModel!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel!!.setNavigator(this)
        mBinding = getViewDataBinding()
        getControls()
    }


    @SuppressLint("NewApi")
    fun getControls() {
        mContext = this@ProfileTabFragment.activity!!
        mSessionPref = SessionPreferences(mContext)
        mViewModel!!.setSessionPref(mSessionPref)

        mBinding.tvUsername.text =
            mSessionPref.mLoginData!!.data.login.firstname + "" + mSessionPref.mLoginData!!.data.login.lastname
        mBinding.tvEmail.text = mSessionPref.mLoginData!!.data.login.userEmail

        GlideApp.with(mContext!!)
            .load(mSessionPref.mLoginData!!.data.login.userPic)
            .error(R.drawable.ic_default)
            .into(mBinding.ivProfile)

    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }


    override fun accountInformationClick() {
        startActivity(AccountInformationActivity.getIntent(mContext!!))
    }

    override fun myOrderClick() {
        startActivity(MyOrderActivity.getIntent(mContext!!))
    }

    override fun paymentMethodClick() {

    }

    override fun deliveryAddressClick() {
        startActivity(ChangeDeliveryAddressActivity.getIntent(mContext!!))
      //  startActivity(CheckOutActivity.getIntent(mContext!!))
    }

    override fun settingClick() {
        startActivity(NotificationActivity.getIntent(mContext!!))
    }

    override fun helpCenterClick() {

    }

    override fun aboutUsClick() {

    }

    override fun shareAppClick() {

    }

    override fun editProfileClick() {
        startActivity(EditProfileActivity.getIntent(mContext!!))
    }

    override fun logoutClick() {
        var mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            getString(R.string.are_you_sure_do_you_want_to_logout),
            getAppString(R.string.yes),
            getAppString(R.string.no),
            true,
            true,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                    mSessionPref.clearSession()
                    startActivity(LoginActivity.getIntent(mContext))
                    activity!!.finishAffinity()

                }

                override fun cancelClick() {

                }
            })

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {

    }

    override fun onResponse(eventType: String, response: String) {

    }

    override fun onRequestFailed(eventType: String, response: String) {

    }

    override fun onRequestRetry() {

    }

    override fun onSessionExpire() {

    }

    override fun onMinorUpdate() {

    }

    override fun onAppHardUpdate() {

    }

    override fun noNetwork() {

    }
}