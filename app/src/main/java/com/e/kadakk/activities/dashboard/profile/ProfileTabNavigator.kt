package com.e.kadakk.activities.dashboard.profile



interface ProfileTabNavigator {

    fun accountInformationClick()
    fun myOrderClick()
    fun paymentMethodClick()
    fun deliveryAddressClick()
    fun settingClick()
    fun helpCenterClick()
    fun aboutUsClick()
    fun shareAppClick()
    fun editProfileClick()
    fun logoutClick()
}