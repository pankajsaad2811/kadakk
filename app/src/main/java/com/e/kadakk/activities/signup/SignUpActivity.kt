package com.e.kadakk.activities.signup

import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.otp.OTPActivity
import com.e.kadakk.activities.signup.model.RegistrationModel
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.country.CountryDialogListFragment
import com.e.kadakk.country.CountryListResponse
import com.e.kadakk.databinding.ActivitySignupBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.*
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import java.util.*
import kotlin.collections.ArrayList


class SignUpActivity : BaseActivity<ActivitySignupBinding, SignUpViewModel>(),
    serverResponseNavigator, SignUpNavigator, ToolBarNavigator {
    private lateinit var mViewModel: SignUpViewModel
    private lateinit var mBinding: ActivitySignupBinding
    private lateinit var mContext: Context
    private lateinit var mActivity: Activity
    private val TAG = SignUpActivity::class.java.simpleName
    private lateinit var mSessionPref: SessionPreferences
    private lateinit var mCountryList: ArrayList<CountryListResponse.Data>
    private var mShowNetworkDialog: Dialog? = null
    private lateinit var mToolBarViewModel: ToolBarViewModel
    /*
* true means show dialog else do not show dialog  after get  the response from API
* */
    private var isOpenCountryDialog: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel

        initViews()

    }
    companion object {
        fun getIntent(context: Context): Intent {
            var intent = Intent(context, SignUpActivity::class.java)
            return intent;
        }
    }





    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_signup
    }

    override fun getViewModel(): SignUpViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(SignUpViewModel::class.java)
        return mViewModel
    }


    private fun initViews() {
        mContext = this@SignUpActivity
        mActivity = this@SignUpActivity
        mViewModel.setNavigator(this)
        mCountryList = ArrayList();


        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding = getViewDataBinding()
        mBinding.layoutToolbar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)

        mBinding.layoutToolbar.ibBack.visibility = View.VISIBLE
        mBinding.layoutToolbar.tvTitle.text = ""



        mViewModel.getSignUpResult().observe(this, Observer { result ->

            if (result.equals("")) {

//                var bundle: Bundle = Bundle()
//                bundle.putString("parent_id", "41")
//                bundle.putString("mobile", mViewModel.mobilenumberStr)
//                bundle.putString("email", mViewModel.mEmailStr)
//                bundle.putString("contryCode", mViewModel!!.codeStr)
//                bundle.putBoolean("Type", true)
//
//                startActivity(VerifyOTPActivity.getIntent(mContext!!, bundle))

                mViewModel.callSignUpAPI()

            } else {
                showMessage(result)
            }
        })
    }
    /*
        * show all common message
        * */
    fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }
            });
    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)
        if (eventType == "countryListResponse") {

            val bean = Gson().fromJson(response, CountryListResponse.CountryBean::class.java)
            mCountryList = bean.data as ArrayList<CountryListResponse.Data>;
            Log.e(TAG, "mCountryList>>>>????   " + mCountryList.size)
            isOpenCountryDialog=true

            if (isOpenCountryDialog) {
                isOpenCountryDialog = false;
                showCountryListDialog(mCountryList, getAppString(R.string.country))
            }
        }else if (eventType == AppConstants.REGISTER_API){

            var mBean = Gson().fromJson(response, RegistrationModel::class.java)

            var mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name), mBean.message, getAppString(R.string.ok),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                        var bundle : Bundle = Bundle()
                        bundle.putString("USERID", mBean.data.register.userId)
                        bundle.putBoolean("ISSIGNUP", true)
                        startActivity(OTPActivity.getIntent(mContext!!, bundle))
                    }

                    override fun cancelClick() {

                    }
                })

        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            response,
            false
        )

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
        BaseActivity.showErrorMessage(
            mContext,
            getString(R.string.session_expire_msg),
            true
        )
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
        showMinorUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            false
        )
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
        showHardUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            true
        )
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            getString(R.string.No_internet_connection),
            false
        )
    }

    override fun LoginClick() {
//        AppUtil.startIntent(null,mActivity,LoginActivity::class.java)
        finish()
    }

    override fun onDateOfBirthClick() {
        AppUtil.hideSoftKeyBoard((mContext as Activity?)!!, mBinding.etDateOfBirth)
        onPickDate()
    }


    fun onPickDate(){

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)


        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            // Display Selected date in textbox
//            mBinding.tvDate.setText(DateUtil.getDateDDMMMYYYY(""+dayOfMonth+"-"+(monthOfYear+1)+"-"+year))
            mBinding.etDateOfBirth.setText((""+dayOfMonth+"-"+(monthOfYear+1)+"-"+year))
            mViewModel!!.birthDayStr = (""+dayOfMonth+"-"+(monthOfYear+1)+"-"+year)

        }, year, month, day)

        val cc = Calendar.getInstance()
        cc.add(Calendar.YEAR,  -18)
        dpd.datePicker.maxDate =cc.timeInMillis

        dpd.show()
    }


    override fun onAddressClick() {

    }


    override fun countryCodeClick() {
//        if (mCountryList == null || mCountryList.size == 0) {
//            isOpenCountryDialog = true;
//            mViewModel.countryListAPI()
//
//        } else {
//            //open country code picker dialog
//            isOpenCountryDialog = false;
//            showCountryListDialog(mCountryList, getAppString(R.string.country))
//        }

//        val jsonFileString = getJsonDataFromAsset(mContext!!, "CountryCode.json")
        try {

            isOpenCountryDialog = false;
            val jsonFileString = Methods.getJsonDataFromAsset(mContext!!, "CountryCode.json")
            val bean = Gson().fromJson(jsonFileString, CountryListResponse.CountryBean::class.java)
            mCountryList = bean.data as ArrayList<CountryListResponse.Data>;
            showCountryListDialog(mCountryList, getAppString(R.string.country))
        }catch (e: Exception){
            Log.e("Exception", "countryCodeClick: "+e.message)
        }

    }


    /**********Show country code dialog  */
    var mSelectedCountry: CountryListResponse.Data? = null;

    private fun showCountryListDialog(
        list: java.util.ArrayList<CountryListResponse.Data>,
        placeHolder: String
    ) {
        lateinit var dialogFrag: CountryDialogListFragment
        try {
            dialogFrag = CountryDialogListFragment.newInstance(
                getAppString(R.string.app_name),
                list,
                placeHolder
            )
            AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)



            dialogFrag.show(supportFragmentManager, "countrystate-dialog")
            dialogFrag.setCancelable(false)

            // val ft: FragmentTransaction = fragmentManager.beginTransaction()

        }
        catch(e:Exception)
        {
            Log.e("dialog_exception","--->"+e.toString())
        }
        dialogFrag.setOnDialogClickedListener(object :
            CountryDialogListFragment.OnDialogClickedListener {
            override fun onDialogYes() {
                dialogFrag.dismiss()

                AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)
            }

            override fun onDialogNo() {
                dialogFrag.dismiss()

                AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)
            }

            override fun selectedDialogItem(
                dialogPos: Int,
                selectedData: CountryListResponse.Data
            ) {
                AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)

                dialogFrag.dismiss()

                if (!selectedData.phonecode.contains("+")) {
                    selectedData.phonecode = "+" + selectedData.phonecode
                } else {
                    selectedData.phonecode = "" + selectedData.phonecode
                }
                mSelectedCountry = selectedData
                mViewModel.codeStr=mSelectedCountry!!.phonecode
                mBinding.etcontryCode.setText((mSelectedCountry!!.phonecode))
            }
        })
    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }


}
