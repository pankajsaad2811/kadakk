package com.e.kadakk.activities.orderitems.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.orderdetail.OrderDetailModel
import com.e.kadakk.activities.orderitems.OrderItemsViewModel
import com.e.kadakk.databinding.ItemOrderItemsBinding
import com.e.kadakk.utils.GlideApp


/**
 * Created by Satish Patel on 17,December,2020
 */
class OrderItemsAdapter(context: Context, viewModel: OrderItemsViewModel) : RecyclerView.Adapter<OrderItemsAdapter.ViewHolder>() {

    lateinit var mContext: Context
    lateinit var fromWhere:String
    var mList: ArrayList<OrderDetailModel.Data.ListItem?> = ArrayList()
    val TAG = OrderItemsAdapter::class.java.simpleName
    var mViewModel: OrderItemsViewModel

    init {
        mContext = context
        mViewModel = viewModel
    }

    fun setList(list: ArrayList<OrderDetailModel.Data.ListItem?>) {
        mList = ArrayList()
        mList = list
        Log.e("zzffz", "JJJJJ: "+mList.size)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = DataBindingUtil.inflate<ItemOrderItemsBinding>(
            LayoutInflater.from(mContext), R.layout.item_order_items, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val data = mList.get(position)

        holder.binding!!.tvItemName.setText(data?.title)
        holder.binding!!.tvAmount.text = data?.offerPrice
        holder.binding!!.tvQuantity.text = "Qty "+data?.quantity

        GlideApp.with(mContext)
            .load(data?.offerImage)
            .into(holder.binding!!.itemImage)

        holder.setViewModel(position, mViewModel, data)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ItemOrderItemsBinding>(itemView)

        fun setViewModel(position: Int, viewModel: OrderItemsViewModel, data: OrderDetailModel.Data.ListItem?) {
            binding!!.position = position
            binding!!.itemRow = data
            binding!!.viewModel = viewModel
            binding!!.setVariable(BR.viewModel, viewModel)
            binding!!.executePendingBindings()
        }
    }

}