package com.e.kadakk.activities.choosetopics

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.annotation.RequiresApi
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.choosetopics.adapter.ChooseCategoryAdapter
import com.e.kadakk.activities.choosetopics.model.ChooseCategoryModel
import com.e.kadakk.activities.dashboard.DashboardActivity
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.base.GridSpacesItemDecoration
import com.e.kadakk.databinding.ActivityChooseTopicsBinding
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.LoginModel
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import org.json.JSONObject

class ChooseTopicsActivity : BaseActivity<ActivityChooseTopicsBinding, ChooseTopicsViewModel>(),
    ChooseTopicsNavigator, serverResponseNavigator {


    lateinit var mContext: Context
    lateinit var mViewModel: ChooseTopicsViewModel
    lateinit var mSessionPref: SessionPreferences
    lateinit var mBinding: ActivityChooseTopicsBinding
    private var mShowNetworkDialog: Dialog? = null;
    var mOkCancelDialog: Dialog? = null;

    var clickEvent = true
    lateinit var mGridLayoutManager: GridLayoutManager
    lateinit var mAdapter: ChooseCategoryAdapter
    var mList: ArrayList<ChooseCategoryModel.Data> = ArrayList()

    companion object {
        fun getIntent(context: Context, bundle: Bundle?): Intent {
            var intent = Intent(context, ChooseTopicsActivity::class.java);
            intent.putExtra("ChooseTopics-bundle", bundle)
            return intent;
        }

    }

    /**
     * Get intent data
     */
    private fun getIntentData() {
        if (intent.getBundleExtra("ChooseTopics-bundle") != null) {

            mViewModel.mUserId =
                intent.getBundleExtra("ChooseTopics-bundle")!!.getString("USERID")!!


        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel;
    }

    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun getLayoutId(): Int {

        return R.layout.activity_choose_topics
    }

    override fun getViewModel(): ChooseTopicsViewModel {
        mViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
            .get(ChooseTopicsViewModel::class.java)
        return mViewModel
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel
        getIntentData()
        getControls()


    }


    fun getControls() {
        mContext = this
        mSessionPref = SessionPreferences(mContext)
        mViewModel.setSessionPref(mSessionPref)
//        mToolBarViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application, this))
//            .get(ToolBarViewModel::class.java)
//        mBinding.layoutToolbar.toolviewModel = mToolBarViewModel
//        mToolBarViewModel.setToolBarNavigator(this)
//        mBinding.layoutToolbar.tvTitle.text = ""
//        mBinding.layoutToolbar.ibNotification.visibility = View.INVISIBLE
//        mBinding.layoutToolbar.ibBack.visibility = View.VISIBLE

        mAdapter =
            ChooseCategoryAdapter(
                mContext,
                mViewModel
            )
        mGridLayoutManager = GridLayoutManager(mContext, 3)
        mBinding.rvChooseTopics.layoutManager = mGridLayoutManager
        mBinding.rvChooseTopics.adapter = mAdapter

        val spanCount = 3 // 3 columns
        val spacing = 22 // 50px
        val includeEdge = false
        mBinding.rvChooseTopics.addItemDecoration(
            GridSpacesItemDecoration(
                spanCount,
                spacing,
                includeEdge
            )
        );



        mViewModel!!.callChooseCategoryAPI()

    }


    /*
* show all common message from one place
* */
    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            });
    }

    override fun onDoneClick() {
var ids = ""

        for (i in 0.. mList.size-1){
           if (mList.get(i).isSelected){
               if (ids.trim() == ""){
                   ids = mList.get(i).catId
               }else{
                   ids = ids+","+mList.get(i).catId
               }
           }

        }

        mViewModel!!.callUpdateChooseCategoryAPI(ids)


    }

    override fun onChooseCategoryClick(position: Int) {

        if (mList.get(position).isSelected){
            mList.get(position).isSelected = false
        }else{
            mList.get(position).isSelected = true
        }
        mAdapter.notifyItemChanged(position)
    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResume() {
        super.onResume()
        clickEvent = true
    }

    override fun onResponse(eventType: String, response: String) {
        clickEvent = true
        showLoaderOnRequest(false)

        if (eventType == AppConstants.OFFER_CHOOSE_CATEGORY_API) {

            var mBean = Gson().fromJson(response, ChooseCategoryModel::class.java)
            mList = mBean.data as ArrayList<ChooseCategoryModel.Data>
            mAdapter.setList(mList)



        }else if (eventType == AppConstants.OFFER_UPDATE_CHOOSE_CATEGORY_API){
            var mBean = Gson().fromJson(response, LoginModel::class.java)



            DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name), mBean.message, getAppString(R.string.ok),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                        mSessionPref.saveSessionPref(mBean)
                        startActivity(DashboardActivity.getIntent(mContext!!, true))
                        finish()
                    }

                    override fun cancelClick() {

                    }

                })
        }


    }


    override fun onRequestFailed(eventType: String, response: String) {
        clickEvent = true
        showLoaderOnRequest(false)
        showMessage(response)

    }

    override fun onRequestRetry() {
        clickEvent = true
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        clickEvent = true
    }

    override fun onMinorUpdate() {
        clickEvent = true
        showLoaderOnRequest(false)
        //showMessage(message)
    }

    override fun onAppHardUpdate() {
        clickEvent = true
    }


    override fun noNetwork() {
        clickEvent = true
        showLoaderOnRequest(false)
        showErrorMessage(mContext, getString(R.string.No_internet_connection), false)
    }


    override fun onBackPressed() {
//        super.onBackPressed()
    }


}
