package com.e.kadakk.activities.dashboard.coupon.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.coupon.CouponTabViewModel
import com.e.kadakk.activities.dashboard.coupon.model.CouponTabModel
import com.e.kadakk.base.DeviceUtil
import com.e.kadakk.databinding.ItemCouponOfferBinding


class OfferAdapter(context: Context, viewModel: CouponTabViewModel) :
    RecyclerView.Adapter<OfferAdapter.ViewHolder>() {
    lateinit var mContext: Context
    lateinit var mViewModel: CouponTabViewModel
    var mList: ArrayList<CouponTabModel.Data.ByCategory> =
        ArrayList<CouponTabModel.Data.ByCategory>()

    init {
        mContext = context
        mViewModel = viewModel
    }

    fun setList(list: ArrayList<CouponTabModel.Data.ByCategory>) {
        mList = ArrayList<CouponTabModel.Data.ByCategory>()
        mList = list

        notifyDataSetChanged()
    }


    fun setFavoriteItem(){


    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = DataBindingUtil.inflate<ItemCouponOfferBinding>(
            LayoutInflater.from(mContext),
            R.layout.item_coupon_offer, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
//    return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = mList.get(position)

        holder.binding!!.tvTitle.text = data.catName
        holder.binding!!.flBanner.visibility = View.GONE
        holder.binding!!.llOffers.visibility = View.GONE

        if (data.catName.trim().toLowerCase() == mContext.getString(R.string.top_offers)) {

            holder.binding!!.flBanner.visibility = View.VISIBLE
            setTopOffers(holder, data, position)

        } else if (data.catName.trim()
                .toLowerCase() == mContext.getString(R.string.tranding_offers)
        ) {

            holder.binding!!.llOffers.visibility = View.VISIBLE
            setTrandingOffers(holder, data, position)

        } else {
            holder.binding!!.llOffers.visibility = View.VISIBLE
            setAllOffers(holder, data, position)
        }

        holder.setViewModel(position, mViewModel)
    }

    fun setTopOffers(holder: ViewHolder, data: CouponTabModel.Data.ByCategory, position: Int) {

        var mBennerAdapter = BennerAdapter(mContext, mViewModel!!)
        holder.binding!!.vpBenner.clipToPadding = false
        holder.binding!!.vpBenner.setPadding(30, 0, 80, 0)
        // mBrandAdapter.setBrandList(mBrandList)
        holder.binding!!.vpBenner.adapter = mBennerAdapter
        mBennerAdapter.setList(data.catOffers as ArrayList<CouponTabModel.Data.ByCategory.CatOffer>)
        holder.binding!!.indicator.setViewPager(holder.binding!!.vpBenner)

        var width: Int = (DeviceUtil.getScreenWidth(mContext)).toInt()
        var height = (width / 2).toInt()
        var params = FrameLayout.LayoutParams(width, height)
        params.setMargins(0, 0, 0, 0)
        holder.binding!!.vpBenner.layoutParams = params
    }

    fun setTrandingOffers(holder: ViewHolder, data: CouponTabModel.Data.ByCategory, position: Int) {

        var mTrendingOfferAdapter = TrendingOfferAdapter(mContext, mViewModel!!)
        holder.binding!!.rvItem.adapter = mTrendingOfferAdapter
        holder.binding!!.rvItem.layoutManager =
            LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        mTrendingOfferAdapter.setList(data.catOffers as ArrayList<CouponTabModel.Data.ByCategory.CatOffer>)

        //holder.binding!!.rvItem.setPadding(30, 0, 0, 0)

//        var width: Int = (DeviceUtil.getScreenWidth(mContext)).toInt()
//        var height = (width / 2).toInt()
//        var params = LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT)
//        params.setMargins(0, 0, 0, 0)
//        holder.binding!!.vpBenner.layoutParams = params


    }

    fun setAllOffers(holder: ViewHolder, data: CouponTabModel.Data.ByCategory, position: Int) {

        var mViewerOfferAdapter = ViewerOfferAdapter(mContext, mViewModel!!, position)
        holder.binding!!.rvItem.adapter = mViewerOfferAdapter
        holder.binding!!.rvItem.layoutManager =
            LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        mViewerOfferAdapter.setList(data.catOffers as ArrayList<CouponTabModel.Data.ByCategory.CatOffer>)



    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ItemCouponOfferBinding>(itemView)

        fun setViewModel(position: Int, viewModel: CouponTabViewModel) {
            binding!!.position = position
            binding!!.viewModel = viewModel
//            binding!!.itemData = data
            binding!!.setVariable(BR.viewModel, viewModel)
            binding!!.executePendingBindings()
        }

    }


}