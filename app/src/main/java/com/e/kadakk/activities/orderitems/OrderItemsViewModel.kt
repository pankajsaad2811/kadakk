package com.e.kadakk.activities.orderitems

import android.app.Application
import com.e.kadakk.activities.orderdetail.OrderDetailModel
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.*


/**
 * Created by Satish Patel on 17,December,2020
 */
class OrderItemsViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<OrderItemsNavigator>(application) {

    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    var emailStr: String = ""
    var passwordStr: String = ""
    lateinit var mSessionPref: SessionPreferences
    var mPosition: Int = 0
    var mOuterPosition: Int = 0

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    public fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    fun onItemClick(pos:Int,data : OrderDetailModel.Data.ListItem?){
        getNavigator().itemClick(pos,data)
    }

    fun callOrderDetailsAPI(orderId:String) {

        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val json = JsonElementUtil.getJsonObject(
            "order_id", orderId
        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.ORDER_DETAIL_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""),
            AppConstants.ORDER_DETAIL_API, serverResponse
        )

    }


}