package com.e.kadakk.activities.changedeliveryaddress

import com.e.kadakk.activities.changedeliveryaddress.model.ChangeDeliveryAddressModel


interface ChangeDeliveryAddressNavigator {

    fun showMessage(msg:  String)
    fun addNewAddressClick()
    fun itemClick(pos:Int , data: ChangeDeliveryAddressModel.Data)
    fun itemDeleteClick(pos:Int , data: ChangeDeliveryAddressModel.Data)
}