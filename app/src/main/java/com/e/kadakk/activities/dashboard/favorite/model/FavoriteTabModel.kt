package com.e.kadakk.activities.dashboard.favorite.model


import com.google.gson.annotations.SerializedName

data class FavoriteTabModel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("errorcode")
    val errorcode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("version")
    val version: Version
) {
    data class Data(
        @SerializedName("discount_type")
        val discountType: String,
        @SerializedName("discount_value")
        val discountValue: String,
        @SerializedName("offer_id")
        val offerId: String,
        @SerializedName("offer_image")
        val offerImage: String,
        @SerializedName("title")
        val title: String
    )

    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )
}