package com.e.kadakk.activities.dashboard.coupon

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.e.kadakk.BR

import com.e.kadakk.R
import com.e.kadakk.activities.categorydetail.CategoryDetailActivity
import com.e.kadakk.activities.checkout.CheckOutActivity
import com.e.kadakk.activities.dashboard.DashboardActivity
import com.e.kadakk.activities.dashboard.coupon.adapter.CategoryAdapter
import com.e.kadakk.activities.dashboard.coupon.adapter.OfferAdapter
import com.e.kadakk.activities.dashboard.coupon.model.CouponTabModel
import com.e.kadakk.activities.viewallbycategory.ViewAllOffersByCategoryActivity
import com.e.kadakk.activities.viewalloffers.ViewAllOffersActivity
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.base.BaseFragment
import com.e.kadakk.databinding.AddCategoryItemDialogBinding
import com.e.kadakk.databinding.FragmentCouponTabBinding
import com.e.kadakk.utils.*
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import org.json.JSONObject

class CouponTabFragment : BaseFragment<FragmentCouponTabBinding, CouponTabViewModel>(),
    serverResponseNavigator,
    CouponTabNavigator {


    lateinit var mContext: Context
    //lateinit var mHomeBean: HomeTabResponse.HomeTabBean;


    var mShowNetworkDialog: android.app.Dialog? = null;
    var mOkCancelDialog: Dialog? = null;
    lateinit var mSessionPref: SessionPreferences


    var pageNo: Int = 1


    //Coupon Category
    lateinit var mCategoryAdapter: CategoryAdapter
    var mCategoryList: ArrayList<CouponTabModel.Data.Category> = ArrayList()

    //Coupon Benner
    lateinit var mOfferAdapter: OfferAdapter
    var mList: ArrayList<CouponTabModel.Data.ByCategory> = ArrayList()


    var mBean: CouponTabModel? = null


    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_coupon_tab
    }

    override fun getViewModel(): CouponTabViewModel {
        mViewModel =
            ViewModelProviders.of(this, ViewModelProviderFactory(activity!!.application, this))
                .get(CouponTabViewModel::class.java)

        return mViewModel!!

    }

    var mViewModel: CouponTabViewModel? = null
    lateinit var mBinding: FragmentCouponTabBinding;

    companion object {

        val SPECIAL_ITEM_ROW = 2;

        @JvmStatic
        fun newInstance() =
            CouponTabFragment().apply {

            }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel!!.setNavigator(this)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding = getViewDataBinding()
        getControls()
         mViewModel!!.callCouponOfferAPI()
    }

    @SuppressLint("NewApi")
    fun getControls() {
        mContext = this@CouponTabFragment.activity!!
        mSessionPref = SessionPreferences(mContext)
        mViewModel!!.setSessionPref(mSessionPref)


        //Category Adapter
        mCategoryAdapter = CategoryAdapter(mContext, mViewModel!!)
        mBinding.rvCategory.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        mBinding.rvCategory.adapter = mCategoryAdapter


        // Offer Adapter
        mOfferAdapter = OfferAdapter(mContext, mViewModel!!)
        mBinding.rvProduct.adapter = mOfferAdapter

//        // Benner Pager Adapter
//        mBennerAdapter = BennerAdapter(mContext, mViewModel!!)
//        mBinding.vpBenner.clipToPadding = false
//        mBinding.vpBenner.setPadding(30, 0, 80, 0)
//        // mBrandAdapter.setBrandList(mBrandList)
//        mBinding.vpBenner.adapter = mBennerAdapter
//
//        var width: Int = (DeviceUtil.getScreenWidth(mContext)).toInt()
//        var height = (width / 1.75).toInt()
//        var params = FrameLayout.LayoutParams(width, height)
//        params.setMargins(0, 0, 0, 0)
//
//        mBinding.vpBenner.layoutParams = params


    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    fun setData() {

        mCategoryList = mBean!!.data.categoryList as ArrayList<CouponTabModel.Data.Category>
        mCategoryAdapter.setList(mCategoryList)


        mList = mBean!!.data.byCategory as ArrayList<CouponTabModel.Data.ByCategory>
        mOfferAdapter.setList(mList)


    }


    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)

        try {

            if (eventType == AppConstants.OFFER_HOME_OFFERS_API) {

                mBean = Gson().fromJson(response, CouponTabModel::class.java)

                setData()


            }else if(eventType == AppConstants.ADD_WISH_LIST_API){


                if (mList.get(mViewModel!!.mOuterPosition).catOffers.get(mViewModel!!.mPosition).isFavourite == 0) {
                    mList.get(mViewModel!!.mOuterPosition).catOffers.get(mViewModel!!.mPosition).isFavourite =
                        1
                }else{
                    mList.get(mViewModel!!.mOuterPosition).catOffers.get(mViewModel!!.mPosition).isFavourite =
                        0
                }
                mOfferAdapter.notifyItemChanged(mViewModel!!.mOuterPosition)

            }else  if (eventType == AppConstants.ADD_TO_CARD_API){

                startActivity(CheckOutActivity.getIntent(mContext!!))
            }


        } catch (e: Exception) {
            AppUtil.showLogs("Exception", "onResponse: " + e.message)

        }

    }


    override fun onPause() {
        super.onPause()

    }


    override fun onResume() {
        super.onResume()

//        pageNo = 1
//        mViewModel!!.callChatUsersListAPI(pageNo)
    }


    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            response,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
        BaseActivity.showErrorMessage(
            mContext,
            getString(R.string.No_internet_connection),
            false
        )
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), "Session expire", getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

//                    startActivity(LoginActivity.getIntent(mContext))
//                    activity!!.finishAffinity()
//                    mSessionPref.clearSession()

                }

                override fun cancelClick() {

                }

            });
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun showMessage(msg: String) {

        mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }
            })
    }

    override fun onSearchClick() {

        var bundle: Bundle = Bundle()
        bundle.putInt("TAB-POSITION", 1)
        startActivity(DashboardActivity.getIntent(mContext, true, bundle))
    }

    override fun onCategoryItemClick(position: Int) {

        Log.e("xcvxcv", "IIIIII: "+mCategoryList.get(position).catId)

        var bundle: Bundle = Bundle()
        bundle.putString("catId", ""+mCategoryList.get(position).catId)
        bundle.putString("categoryName", ""+mCategoryList.get(position).catName)
        startActivity(ViewAllOffersByCategoryActivity.getIntent(mContext, bundle))


    }

    override fun onBennerItemClick(position: Int, data: CouponTabModel.Data.ByCategory.CatOffer) {

        var bundle: Bundle = Bundle()
        bundle.putString("offerId", data.offerId)

        startActivity(CategoryDetailActivity.getIntent(mContext, bundle))

    }

    override fun onOfferItemClick(position: Int, data: CouponTabModel.Data.ByCategory.CatOffer) {


        var bundle: Bundle = Bundle()
        bundle.putString("offerId", data.offerId)

        startActivity(CategoryDetailActivity.getIntent(mContext, bundle))

    }

    override fun onBennerGrabNowClick(
        position: Int,
        data: CouponTabModel.Data.ByCategory.CatOffer
    ) {
         showGrabDialog(data)
    }

    fun showGrabDialog(data: CouponTabModel.Data.ByCategory.CatOffer) {

        val dialog = Dialog(mContext!!)//, R.style.AppListDialogTheme)

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)

        dialog.getWindow()!!.getAttributes().windowAnimations = R.style.Dialog_WindowAnimation;

        val binding = DataBindingUtil.inflate<AddCategoryItemDialogBinding>(
            LayoutInflater.from(mContext!!),
            R.layout.add_category_item_dialog, null, false
        )
        dialog.window!!.decorView.setBackgroundResource(android.R.color.transparent)
        dialog.setContentView(binding.getRoot())

        dialog.setCancelable(false)
        dialog.setCanceledOnTouchOutside(false)


//      binding.tvAddressLocation.text = mBean!!.data.offerDetails.
        //     binding.tvTitle.text = mBean!!.data.offerDetails.discountType
        binding.tvOff.text =data.discountValue+"% off"
      //  binding.tvAmount.text = data.offerDetails.onMinPurchase
        binding.tvItemName.text =data.title

        GlideApp.with(mContext!!)
            .load(data.offerImage)
            .into( binding!!.itemImage)

        binding.btnCancel.setOnClickListener{
            dialog.dismiss()
        }

        binding.btnPlus.setOnClickListener {

            val qty :String = binding.tvQuantity.text.toString()

            if(Integer.parseInt(qty)<5){
                val newQty = Integer.parseInt(qty)+1
                binding.tvQuantity.text = ""+newQty
            }
        }

        binding.btnMinus.setOnClickListener {

            val qty :String = binding.tvQuantity.text.toString()

            if(Integer.parseInt(qty)>1){
                val newQty = Integer.parseInt(qty)-1
                binding.tvQuantity.text = ""+newQty
            }
        }

        binding.btnGrabNow.setOnClickListener {
            mViewModel!!.addToCardAPI(binding.tvQuantity.text.toString(),data.offerId)
            dialog.dismiss()
        }

        dialog.show()
    }

    override fun onViewAllClick(position: Int) {

        Log.e("xzvzxv", "OOOO: "+mList.get(position).catId)
        Log.e("xzvzxv", "OOOO: "+mList.get(position).catName)

        var bundle: Bundle = Bundle()
        bundle.putString("catId", ""+mList.get(position).catId)
        bundle.putString("categoryName", ""+mList.get(position).catName)
        startActivity(ViewAllOffersActivity.getIntent(mContext, bundle))


    }

    override fun onFavoriteClick(
        position: Int,
        outerPosition: Int,
        data: CouponTabModel.Data.ByCategory.CatOffer
    ) {
        mViewModel!!.mPosition = position
        mViewModel!!.mOuterPosition = outerPosition
        mViewModel!!.callFavoriteAPI(data.offerId)
    }


    override fun onDestroy() {
        super.onDestroy()

    }

}
