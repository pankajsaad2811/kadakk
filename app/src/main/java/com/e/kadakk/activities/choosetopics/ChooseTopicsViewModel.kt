package com.e.kadakk.activities.choosetopics

import android.app.Application
import android.text.TextUtils
import android.view.View.OnFocusChangeListener
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.e.kadakk.R
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.Validator
import com.e.kadakk.utils.server.*
import java.util.regex.Pattern

class ChooseTopicsViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<ChooseTopicsNavigator>(application) {
    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface


    var mUserId: String = ""
    lateinit var mSessionPref: SessionPreferences

    private val PASSWORD_PATTERN: Pattern =
        Pattern.compile("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#\$%^&*]).{8,20})")

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    public fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    fun onDoneClick() {
        getNavigator().onDoneClick()
    }

    fun onChooseCategoryClick(position: Int) {
        getNavigator().onChooseCategoryClick(position)
    }

    /******* API implementation ******/
    fun callChooseCategoryAPI() {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!



        mRxApiCallHelper.call(
            mApiInterface.getRequest(
                AppConstants.OFFER_CHOOSE_CATEGORY_API,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                "",
                "",
                "",
                ""

            ),
            AppConstants.OFFER_CHOOSE_CATEGORY_API, serverResponse
        )

    }

    fun callUpdateChooseCategoryAPI(selectedCategoryId: String) {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

//    "{
//    ""user_id"": ""K7MyMrUGAA=="",
//    ""selected_category"": ""1,5,6""
//}"
        var json = JsonElementUtil.getJsonObject(
            "user_id", mUserId,
            "selected_category", selectedCategoryId

        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.OFFER_UPDATE_CHOOSE_CATEGORY_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                "",
                "",
                "",
                ""

            ),
            AppConstants.OFFER_UPDATE_CHOOSE_CATEGORY_API, serverResponse
        )

    }

}