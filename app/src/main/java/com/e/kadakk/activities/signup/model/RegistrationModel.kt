package com.e.kadakk.activities.signup.model


import com.google.gson.annotations.SerializedName

data class RegistrationModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("errorcode")
    val errorcode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("version")
    val version: Version
) {
    data class Data(
        @SerializedName("register")
        val register: Register
    ) {
        data class Register(
            @SerializedName("otp")
            val otp: String,
            @SerializedName("user_id")
            val userId: String
        )
    }

    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )
}