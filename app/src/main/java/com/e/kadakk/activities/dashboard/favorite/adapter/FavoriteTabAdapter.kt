package com.e.kadakk.activities.dashboard.favorite.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.favorite.FavoriteTabViewModel
import com.e.kadakk.activities.dashboard.favorite.model.FavoriteTabModel
import com.e.kadakk.activities.dashboard.search.SearchTabViewModel
import com.e.kadakk.activities.dashboard.search.model.SearchModel
import com.e.kadakk.base.DeviceUtil
import com.e.kadakk.databinding.ItemFavoriteBinding
import com.e.kadakk.databinding.ItemSearchTabBinding
import com.e.kadakk.utils.GlideApp


class FavoriteTabAdapter(context: Context, viewModel: FavoriteTabViewModel) :
    RecyclerView.Adapter<FavoriteTabAdapter.ViewHolder>() {
    lateinit var mContext: Context
    lateinit var mViewModel: FavoriteTabViewModel
    var mList: ArrayList<FavoriteTabModel.Data>  = ArrayList<FavoriteTabModel.Data>()

    init {
        mContext = context
        mViewModel = viewModel
    }

    fun setList(list: ArrayList<FavoriteTabModel.Data>) {
        mList = ArrayList<FavoriteTabModel.Data>()
        mList = list

        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        var view = DataBindingUtil.inflate<ItemFavoriteBinding>(
            LayoutInflater.from(mContext),
            R.layout.item_favorite, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
//    return 10
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        var data = mList.get(position)

        holder.binding!!.tvName.setText(data.title)
        holder.binding!!.tvOffer.setText(data.discountValue)

        GlideApp.with(mContext)
            .load(data.offerImage)
            .into( holder.binding!!.ivImage)

        holder.binding!!.ivLike.setBackgroundResource(R.drawable.rounded_favourite_bg_red)

        setParam(holder)
        holder.setViewModel(position , mViewModel)
    }

    fun setParam(holder: ViewHolder) {
        var width: Int = (DeviceUtil.getScreenWidth(mContext) / 2.4).toInt()


        var height = (DeviceUtil.getScreenWidth(mContext) / 3.5).toInt()

        var params = LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT)
        holder.binding!!.llTrending.layoutParams = params


        var coverImageparams = LinearLayout.LayoutParams(width, (height).toInt())
        holder.binding!!.flImage.layoutParams = coverImageparams



    }



    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ItemFavoriteBinding>(itemView)

        fun setViewModel(position: Int, viewModel : FavoriteTabViewModel)
        {
            binding!!.position = position
            binding!!.viewModel = viewModel
//            binding!!.dataItem = data
            binding!!.setVariable(BR.viewModel , viewModel)
            binding!!.executePendingBindings()
        }

    }



}