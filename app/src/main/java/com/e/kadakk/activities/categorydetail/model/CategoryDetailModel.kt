package com.e.kadakk.activities.categorydetail.model


import com.google.gson.annotations.SerializedName

data class CategoryDetailModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("errorcode")
    val errorcode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("version")
    val version: Version
) {
    data class Data(
        @SerializedName("offer_details")
        val offerDetails: OfferDetails,
        @SerializedName("similar_offer")
        val similarOffer: List<SimilarOffer>
    ) {
        data class OfferDetails(
            @SerializedName("calory")
            val calory: String,
            @SerializedName("category_id")
            val categoryId: String,
            @SerializedName("discount_type")
            val discountType: String,
            @SerializedName("discount_value")
            val discountValue: String,
            @SerializedName("is_favourite")
            val isFavourite: Int,
            @SerializedName("offer_id")
            val offerId: String,
            @SerializedName("offer_image")
            val offerImage: OfferImage,
            @SerializedName("on_max_purchase")
            val onMaxPurchase: String,
            @SerializedName("on_min_purchase")
            val onMinPurchase: String,
            @SerializedName("short_description")
            val shortDescription: String,
            @SerializedName("time")
            val time: String,
            @SerializedName("title")
            val title: String
        ) {
            data class OfferImage(
                @SerializedName("image")
                val image: String
            )
        }

        data class SimilarOffer(
            @SerializedName("calory")
            val calory: String,
            @SerializedName("discount_type")
            val discountType: String,
            @SerializedName("discount_value")
            val discountValue: String,
            @SerializedName("is_favourite")
            var isFavourite: Int,
            @SerializedName("offer_id")
            val offerId: String,
            @SerializedName("offer_image")
            val offerImage: String,
            @SerializedName("short_description")
            val shortDescription: String,
            @SerializedName("time")
            val time: String,
            @SerializedName("title")
            val title: String
        )
    }

    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )
}