package com.e.kadakk.activities.dashboard.search

import android.app.Application
import android.text.TextUtils
import android.view.View.OnFocusChangeListener
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.e.kadakk.R
import com.e.kadakk.activities.categorydetail.model.CategoryDetailModel
import com.e.kadakk.activities.dashboard.coupon.model.CouponTabModel
import com.e.kadakk.activities.dashboard.search.model.SearchModel
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.Validator
import com.e.kadakk.utils.server.*
import java.util.regex.Pattern

class SearchTabViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<SearchTabNavigator>(application) {
    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface


    var mPosition: Int = 0
    var mUserId: String = ""
    lateinit var mSessionPref: SessionPreferences

    private val PASSWORD_PATTERN: Pattern =
        Pattern.compile("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#\$%^&*]).{8,20})")

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    public fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    fun onFavoriteClick(position: Int,  data: SearchModel.Data){

        getNavigator().onFavoriteClick(position, data)
    }

    fun onItemClick(position: Int,  data: SearchModel.Data){

        getNavigator().onItemClick(position, data)
    }


    /******* API implementation ******/
    fun callSearchAPI(isShow: Boolean, text: String) {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        if (isShow){
            serverResponse.showLoaderOnRequest(true)
        }
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        var json = JsonElementUtil.getJsonObject(
            "search_term", text
        )




        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.SEARCH_OFFER_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                "",
                "",
                "",
                ""

            ),
            AppConstants.SEARCH_OFFER_API, serverResponse
        )

    }




    fun callFavoriteAPI(offerId: String) {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        //get request

        /*
        "{
        ""user_id"":""S7MytlIyMjRRsgYA"",
        ""offer_id"": ""S7MytFIyVLIGAA==""
}"
          */
        val jsonObj = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "offer_id", offerId

        );

        mRxApiCallHelper.call(mApiInterface.postRequest(AppConstants.ADD_WISH_LIST_API, jsonObj!!,  mSessionPref.getFirebaseRegIdPrefValue() , "", mSessionPref.mLoginData!!.data.login.authorization, "", "0.0", "0.0"),
            AppConstants.ADD_WISH_LIST_API, serverResponse)

    }



}