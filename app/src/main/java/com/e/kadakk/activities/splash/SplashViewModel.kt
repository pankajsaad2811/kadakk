package com.e.kadakk.activities.splash

import android.app.Application
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.server.serverResponseNavigator


class SplashViewModel (application: Application, serverResponse: serverResponseNavigator) : BaseViewModel<SplashNavigator>(application){
    lateinit var serverResponse: serverResponseNavigator;
    var mApplication: Application;
    init {
        mApplication = application;
        this.serverResponse = serverResponse;
    }
}