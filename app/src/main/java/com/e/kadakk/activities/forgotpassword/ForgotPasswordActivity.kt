package com.e.kadakk.activities.forgotpassword

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.forgotpassword.model.ForgotPasswordModel
import com.e.kadakk.activities.otp.OTPActivity
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityForgotPasswordBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson

class ForgotPasswordActivity : BaseActivity<ActivityForgotPasswordBinding, ForgotPasswordViewModel>(),
    serverResponseNavigator, ForgotPasswordNavigator, ToolBarNavigator {
    private lateinit var mViewModel: ForgotPasswordViewModel
    private lateinit var mBinding: ActivityForgotPasswordBinding
    private lateinit var mContext: Context
    private lateinit var mActivity: Activity
    private val TAG = ForgotPasswordActivity::class.java.simpleName
    private lateinit var mSessionPref: SessionPreferences
    private var mShowNetworkDialog: Dialog? = null
    private lateinit var mToolBarViewModel: ToolBarViewModel

    /*
* true means show dialog else do not show dialog  after get  the response from API
* */
    private var isOpenCountryDialog: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding();
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel

        initViews()

    }
    companion object {
        fun getIntent(context: Context): Intent {
            var intent = Intent(context, ForgotPasswordActivity::class.java)
            return intent;
        }
    }





    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_forgot_password
    }

    override fun getViewModel(): ForgotPasswordViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ForgotPasswordViewModel::class.java)
        return mViewModel
    }


    private fun initViews() {
        mContext = this@ForgotPasswordActivity
        mSessionPref = SessionPreferences(mContext)
        mViewModel.setSessionPref(mSessionPref)
        mActivity = this@ForgotPasswordActivity
        mViewModel.setNavigator(this)



        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding = getViewDataBinding()
        mBinding.layoutToolbar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)

        mBinding.layoutToolbar.ibBack.visibility = View.VISIBLE
        mBinding.layoutToolbar.tvTitle.text = ""




    }
    /*
        * show all common message
        * */
    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }
            });
    }

    override fun sendClick() {

    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)

        if (eventType == AppConstants.FORGOT_PASSWORD_API){

            var mBean = Gson().fromJson(response, ForgotPasswordModel::class.java)


           var mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
                getAppString(R.string.app_name), mBean.message, getAppString(R.string.ok),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                        var bundle : Bundle = Bundle()
                        bundle.putString("USERID", mBean.data.forgot.userId)
                        bundle.putBoolean("ISSIGNUP", false)
                        startActivity(OTPActivity.getIntent(mContext!!, bundle))
                    }

                    override fun cancelClick() {

                    }
                })

        }


    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            response,
            false
        )

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
        BaseActivity.showErrorMessage(
            mContext,
            getString(R.string.session_expire_msg),
            true
        )
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
        showMinorUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            false
        )
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
        showHardUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            true
        )
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            getString(R.string.No_internet_connection),
            false
        )
    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }


}
