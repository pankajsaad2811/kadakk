package com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing.model.OngoingModel
import com.e.kadakk.databinding.ItemOngoingOrderListBinding



class OngoingOrderAdapter (context: Context, viewModel: OngoingOrderViewModel) :
    RecyclerView.Adapter<OngoingOrderAdapter.ViewHolder>() {

    lateinit var mContext: Context
    var mList: ArrayList<OngoingModel.Data.ListOrder?> = ArrayList()
    val TAG = OngoingOrderAdapter::class.java.simpleName
    var mViewModel: OngoingOrderViewModel;

    init {
        mContext = context
        mViewModel = viewModel
    }

    fun setList(list: ArrayList<OngoingModel.Data.ListOrder?>) {
        mList = ArrayList()
        mList = list
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        val view = DataBindingUtil.inflate<ItemOngoingOrderListBinding>(
            LayoutInflater.from(mContext), R.layout.item_ongoing_order_list, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val data = mList.get(position)

        holder.binding!!.tvOderId.setText("#"+data!!.orderId)
        holder.binding!!.tvTotalItem.setText(data.totalItems+" items")
        holder.binding!!.tvTotalBill.setText(mContext.getString(R.string.rs)+data.totalBill)
        holder.binding!!.tvOderStatus.setText(data.status)

/*        if (data.status.toLowerCase() == "noshow" || data.status.toLowerCase() == "cancel" ){

            holder.binding!!.ivStatus.setImageResource(R.drawable.rounded_red)
            holder.binding!!.tvStatus.setTextColor(ContextCompat.getColor(mContext!!, R.color.red))

        }else{
            holder.binding!!.ivStatus.setImageResource(R.drawable.rounded_green)
            holder.binding!!.tvStatus.setTextColor(ContextCompat.getColor(mContext!!, R.color.dark_green1))
        }*/

        holder.setViewModel(position, mViewModel,data)

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var binding = DataBindingUtil.bind<ItemOngoingOrderListBinding>(itemView)

        fun setViewModel(position: Int, viewModel: OngoingOrderViewModel,data:OngoingModel.Data.ListOrder?) {
            binding!!.position = position
            binding!!.itemRow = data
            binding!!.viewModel = viewModel
            binding!!.setVariable(BR.viewModel, viewModel)
            binding!!.executePendingBindings()
        }
    }

}