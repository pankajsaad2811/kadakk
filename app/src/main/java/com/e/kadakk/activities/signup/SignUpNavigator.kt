package com.e.kadakk.activities.signup

import android.view.View

interface SignUpNavigator {
    fun LoginClick()
    fun onDateOfBirthClick()
    fun onAddressClick()
    fun countryCodeClick()
}