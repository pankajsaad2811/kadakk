package com.e.kadakk.activities.dashboard.cart.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.checkout.model.CheckoutModel
import com.e.kadakk.activities.dashboard.cart.CardTabViewModel
import com.e.kadakk.databinding.ItemCartBinding
import com.e.kadakk.utils.GlideApp


/**
 * Created by Satish Patel on 10,December,2020
 */
class CartTabAdapter (context: Context, viewModel: CardTabViewModel) :
    RecyclerView.Adapter<CartTabAdapter.ViewHolder>(){

    lateinit var mContext: Context
    lateinit var fromWhere:String
    var mList: ArrayList<CheckoutModel.Data.ListCard?> = ArrayList()
    val TAG = CartTabAdapter::class.java.simpleName
    var mViewModel: CardTabViewModel

    init {
        mContext = context
        mViewModel = viewModel
    }

    fun setList(list: ArrayList<CheckoutModel.Data.ListCard?>) {
        mList = ArrayList()
        mList = list
        Log.e("zzffz", "JJJJJ: "+mList.size)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = DataBindingUtil.inflate<ItemCartBinding>(
            LayoutInflater.from(mContext), R.layout.item_cart, parent, false
        )
        return ViewHolder(view.root)
    }

    override fun getItemCount(): Int {
        return mList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        val data = mList.get(position)

        holder.binding!!.tvItemName.setText(data?.title)
        holder.binding!!.tvOff.text = ""+data?.discountValue+"% off"
        holder.binding!!.tvAmount.text = data?.offerPrice
        holder.binding!!.tvQuantity.text = data?.quantity

         GlideApp.with(mContext)
             .load(data?.offerImage)
             .into(holder.binding!!.itemImage)

        holder.binding!!.btnMinus.setOnClickListener {

            val qty :String = holder.binding!!.tvQuantity.text.toString()

            if(Integer.parseInt(qty)>1){
                val newQty = Integer.parseInt(qty)-1
                holder.binding!!.tvQuantity.text = ""+newQty
                mViewModel.callUpdateCartItemAPI(data!!.offer_id,newQty)
            }

        }

        holder.binding!!.btnPlus.setOnClickListener {

            val qty :String = holder.binding!!.tvQuantity.text.toString()

            if(Integer.parseInt(qty)<5){
                val newQty = Integer.parseInt(qty)+1
                holder.binding!!.tvQuantity.text = ""+newQty
                mViewModel.callUpdateCartItemAPI(data!!.offer_id,newQty)
            }
        }

        holder.setViewModel(position, mViewModel, data)
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var binding = DataBindingUtil.bind<ItemCartBinding>(itemView)

        fun setViewModel(position: Int, viewModel: CardTabViewModel, data: CheckoutModel.Data.ListCard?) {
            binding!!.position = position
            binding!!.itemRow = data
            binding!!.viewModel = viewModel
            binding!!.setVariable(BR.viewModel, viewModel)
            binding!!.executePendingBindings()
        }
    }


}