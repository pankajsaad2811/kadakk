package com.e.kadakk.activities.orderitems

import com.e.kadakk.activities.orderdetail.OrderDetailModel


/**
 * Created by Satish Patel on 17,December,2020
 */
interface OrderItemsNavigator {
    fun showMessage(msg:  String)
    fun itemClick(pos:Int , data: OrderDetailModel.Data.ListItem?)
}