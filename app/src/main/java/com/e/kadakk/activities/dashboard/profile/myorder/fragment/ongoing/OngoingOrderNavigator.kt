package com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing

import com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing.model.OngoingModel


interface OngoingOrderNavigator {
    fun showMessage(msg:  String)
    fun gotoShoppingClick()
    fun onItemClick(pos:Int,data:OngoingModel.Data.ListOrder?)
}