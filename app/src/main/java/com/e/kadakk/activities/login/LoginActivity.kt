package com.e.kadakk.activities.login

import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.DashboardActivity
import com.e.kadakk.activities.choosetopics.ChooseTopicsActivity
import com.e.kadakk.activities.forgotpassword.ForgotPasswordActivity
import com.e.kadakk.activities.otp.OTPActivity
import com.e.kadakk.activities.signup.SignUpActivity
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityLoginBinding
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.LoginModel
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson

class LoginActivity : BaseActivity<ActivityLoginBinding, LoginViewModel>(),
    serverResponseNavigator, LoginNavigator {
    private lateinit var mViewModel: LoginViewModel
    private lateinit var mBinding: ActivityLoginBinding
    private lateinit var mContext: Context
    private lateinit var mActivity: Activity
    private val TAG = LoginActivity::class.java.simpleName
    private lateinit var mSessionPref: SessionPreferences
    private var mShowNetworkDialog: Dialog? = null
    /*
* true means show dialog else do not show dialog  after get  the response from API
* */
    private var isOpenCountryDialog: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel.setNavigator(this)
        mBinding.viewModel = mViewModel
        initViews()
    }

    companion object {
        fun getIntent(context: Context): Intent {
            val intent = Intent(context, LoginActivity::class.java)
            return intent
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_login
    }

    override fun getViewModel(): LoginViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(LoginViewModel::class.java)
        return mViewModel
    }

    private fun initViews() {
        mContext = this@LoginActivity
        mActivity = this@LoginActivity
        mSessionPref = SessionPreferences(mContext)
        mViewModel.setNavigator(this)
        mViewModel!!.setSessionPref(mSessionPref)
    }

    /*
        * show all common message
        * */
    override fun showMessage(msg: String) {
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }
            });
    }

    override fun loginClick() {

    }

    override fun forgotPasswordClick() {

        startActivity(ForgotPasswordActivity.getIntent(mContext))

    }

    override fun faceBookClick() {

    }

    override fun googleClick() {

    }

    override fun onTwitterClick() {

    }

    override fun createAccountClick() {
        startActivity(SignUpActivity.getIntent(mContext))
//        startActivity(ChooseTopicsActivity.getIntent(mContext, null))
    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {

        try {
            showLoaderOnRequest(false)

            if (eventType == AppConstants.LOGIN_API){
                var mBean = Gson().fromJson(response, LoginModel::class.java)

                if (mBean.data.login.otp!=null){


                    var mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
                        getAppString(R.string.app_name), mBean.message, getAppString(R.string.ok),
                        "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                            override fun okClick() {
                                var bundle : Bundle = Bundle()
                                bundle.putString("USERID", mBean.data.login.userId)
                                bundle.putBoolean("ISSIGNUP", true)
                                startActivity(OTPActivity.getIntent(mContext!!, bundle))
                            }

                            override fun cancelClick() {

                            }
                        })
                }else if(mBean.data.login.selectedCategory == 0){
                    var bundle : Bundle = Bundle()
                    bundle.putString("USERID", mBean.data.login.userId)
                    startActivity(ChooseTopicsActivity.getIntent(mContext!!, bundle))
                } else{

                    mSessionPref.saveSessionPref(mBean)
                    startActivity(DashboardActivity.getIntent(mContext!!, true))
                }
            }

        }catch (e: Exception){
            Log.e("Exception", "OnResponce: "+e.message)
        }

    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            response,
            false
        )

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
        BaseActivity.showErrorMessage(
            mContext,
            getString(R.string.session_expire_msg),
            true
        )
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
        showMinorUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            false
        )
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
        showHardUpdateMessage(
            mContext,
            getString(R.string.new_update_available),
            true
        )
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
        showErrorMessage(
            mContext,
            getString(R.string.No_internet_connection),
            false
        )
    }

}
