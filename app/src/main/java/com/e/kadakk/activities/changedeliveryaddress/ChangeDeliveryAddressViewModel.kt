package com.e.kadakk.activities.changedeliveryaddress

import android.app.Application
import com.e.kadakk.activities.changedeliveryaddress.model.ChangeDeliveryAddressModel
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.*



class ChangeDeliveryAddressViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<ChangeDeliveryAddressNavigator>(application) {

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface

    lateinit var mSessionPref: SessionPreferences

    fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    fun onAddNewAddressClick(){
        getNavigator().addNewAddressClick()
    }

    fun onItemClick(pos:Int , data: ChangeDeliveryAddressModel.Data){
        getNavigator().itemClick(pos,data)
    }

    fun onItemDeleteClick(pos:Int , data: ChangeDeliveryAddressModel.Data){
        getNavigator().itemDeleteClick(pos,data)
    }

    public fun callAddressListAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

      //  val jsonObj = JsonElementUtil.getJsonObject("user_id", mSessionPref.mLoginData!!.data.login.userId)

        mRxApiCallHelper.call(mApiInterface.getRequestWithUserId(
            AppConstants.GET_ADDRESS_LIST_API,
            "",
            "",
            mSessionPref.mLoginData!!.data.login.authorization,
            "",
            "",
            "",
            mSessionPref.mLoginData!!.data.login.userId
        ), AppConstants.GET_ADDRESS_LIST_API, serverResponse)

    }

    /**
     * Call SignUp API
     */
    fun callDeleteAddressAPI(address_id: String) {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val jsonObj = JsonElementUtil.getJsonObject(
            "address_id",address_id
           )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.DELETE_ADDRESS_API,
                jsonObj!!,
                "",
                "",
                "",
                "",
                "",
                ""
            ), AppConstants.DELETE_ADDRESS_API, serverResponse
        )
    }

}