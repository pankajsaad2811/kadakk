package com.e.kadakk.activities.signup

import android.app.Application
import android.text.TextUtils
import android.view.View.OnFocusChangeListener
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.e.kadakk.R
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.server.*
import java.util.regex.Pattern

class SignUpViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<SignUpNavigator>(application) {
    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface



    var birthDayStr = ""
    var addressStr = ""

    var mobilenumberStr: String = ""
    var codeStr = ""
    var mFullNameStr = ""
    var mEmailStr = ""
    var passwordStr = ""
    var mConfirmPasswordStr = ""
    private val PASSWORD_PATTERN: Pattern =
        Pattern.compile("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#\$%^&*]).{8,20})")

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    /**
     * To pass login result to activity
     */
    private val signUpResult = MutableLiveData<String>()

    /*
  * get login result live data
  * */
    fun getSignUpResult(): LiveData<String> = signUpResult

    fun onDateOfBirthClick() {
        getNavigator().onDateOfBirthClick()
    }

    fun LoginClick() {
        getNavigator().LoginClick()
    }

    fun onAddressClick() {
        getNavigator().onAddressClick()
    }

    fun onCountryCodeClicked() {
        getNavigator().countryCodeClick()
    }

    fun nextClick() {

        isAllValid()
    }


    fun isAllValid(){
        if (TextUtils.isEmpty(mFullNameStr.trim())) {
            signUpResult.value = getStringfromVM(R.string.please_enter_your_full_name)
            return;
        } else if (TextUtils.isEmpty(mEmailStr.trim())) {
            signUpResult.value = getStringfromVM(R.string.please_enter_your_Email)
            return;
        } else if (!AppUtil.isEmailValid(mEmailStr.trim())) {
            signUpResult.value = getStringfromVM(R.string.please_enter_valid_email_Id)
            return;
        } else if (TextUtils.isEmpty(passwordStr.trim())) {
            signUpResult.value = getStringfromVM(R.string.please_enter_the_password)
            return;
        } else if ((!isPasswordCorrect((passwordStr.trim()))) || ((passwordStr.trim().length < 8))) {
            signUpResult.value = getStringfromVM(R.string.please_enter_password_correctly)
            return;
        } else if (TextUtils.isEmpty(codeStr.trim())) {
            signUpResult.value = getStringfromVM(R.string.please_select_country_code)
            return;
        } else if (TextUtils.isEmpty(mobilenumberStr.trim())) {
            signUpResult.value = getStringfromVM(R.string.please_enter_phone_number)
            return;
        } else if (mobilenumberStr.trim().length < 10) {
            signUpResult.value = getStringfromVM(R.string.please_enter_valid_phone_number)
            return;
        } else if (TextUtils.isEmpty(birthDayStr.trim())) {
            signUpResult.value = getStringfromVM(R.string.please_select_date_of_birth)
            return;
        } else {
            signUpResult.value = "";
            return

        }
    }

    fun isPasswordCorrect(s: String?): Boolean {
        return PASSWORD_PATTERN.matcher(s).find()
    }

    /******** API implementation *********/

    /**** get country list ***/
    public fun countryListAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        serverResponse.showLoaderOnRequest(true)
        var jsonObj = JsonElementUtil.getJsonObject()
        mRxApiCallHelper.call(
            mApiInterface.getRequest(
                AppConstants.COUNTRY_LIST_API,
                "", "", ", ", "", "", ""

            ), "countryListResponse", serverResponse
        )
    }

    /**
     * Call SignUp API
     */
    public fun callSignUpAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
/*
      "Json Data
{
   ""fullname"": ""Aman Rathore"",
   ""email"":""prathak@mailinator.com"",
   ""password"":""123456"",
   ""country_code"": ""91"",
   ""user_phone"": ""9644065414"",
   ""dob"": ""1996-08-25""
}
"*/
        val jsonObj = JsonElementUtil.getJsonObject(
            "fullname",
            mFullNameStr.trim(),
            "email",
            mEmailStr,
            "user_phone",
            "" + mobilenumberStr,
            "password",
            passwordStr,
            "country_code",
            codeStr,
            "dob", birthDayStr
        );

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.REGISTER_API,
                jsonObj!!,
                "",
                "",
                "",
                "",
                "",
                ""


            ),
            AppConstants.REGISTER_API, serverResponse
        )
    }


}