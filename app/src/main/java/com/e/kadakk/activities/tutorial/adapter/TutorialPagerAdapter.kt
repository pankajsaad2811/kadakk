package com.e.kadakk.activities.tutorial.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.request.RequestOptions
import com.e.kadakk.R
import com.e.kadakk.activities.tutorial.bean.TutorialListModel
import com.e.kadakk.databinding.ItemTutorialBinding
import com.e.kadakk.utils.GlideApp

import java.util.*

class TutorialPagerAdapter(
    private val mContext: Context,
    tutorialList: ArrayList<TutorialListModel.TutorialListBean>,
    itemCLick: ItemCLick
) :
    PagerAdapter() {
    private val itemCLick: ItemCLick
    var tutorialList: ArrayList<TutorialListModel.TutorialListBean>
    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val tutorialBean: TutorialListModel.TutorialListBean = tutorialList[position]
        val inflater = LayoutInflater.from(mContext)
        val mBinding: ItemTutorialBinding =
            DataBindingUtil.inflate(inflater, R.layout.item_tutorial, collection, false)
        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.drawable.default_image)
        requestOptions.error(R.drawable.default_image)


        GlideApp.with(mContext)
            .setDefaultRequestOptions(requestOptions)
            .load(tutorialBean.image)
            .into(mBinding.ivTutorial)

        mBinding.tvTitle.setText(tutorialBean.title)
        mBinding.tvMessage.setText(tutorialBean.msgtext)
        collection.addView(mBinding.getRoot())
        return mBinding.getRoot()
    }

    override fun destroyItem(
        collection: ViewGroup,
        position: Int,
        view: Any
    ) {
        collection.removeView(view as View)
    }

    override fun getCount(): Int {
        return tutorialList.size
    }

    override fun isViewFromObject(
        view: View,
        `object`: Any
    ): Boolean {
        return view === `object`
    }

    interface ItemCLick {
        // fun onGetStartedClicked()
    }

    init {
        this.tutorialList = tutorialList
        this.itemCLick = itemCLick
    }
}