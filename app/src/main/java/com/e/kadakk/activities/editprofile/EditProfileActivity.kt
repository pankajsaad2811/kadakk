package com.e.kadakk.activities.editprofile

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.profile.accountInformations.model.AccountInformationModel
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.country.CountryDialogListFragment
import com.e.kadakk.country.CountryListResponse
import com.e.kadakk.databinding.ActivityEditProfileBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.*
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import com.theartofdev.edmodo.cropper.CropImage
import org.json.JSONObject
import java.io.File
import java.util.*
import kotlin.collections.ArrayList



class EditProfileActivity : BaseActivity<ActivityEditProfileBinding, EditProfileViewModel>(),
    EditProfileNavigator, ToolBarNavigator, serverResponseNavigator {

    var mShowNetworkDialog: Dialog? = null
    var TAG: String = EditProfileActivity::class.java.simpleName
    var mToolBarViewModel: ToolBarViewModel? = null;
    public lateinit var mBinding: ActivityEditProfileBinding
    private var mContext: Context? = null
    private lateinit var mSessionPref: SessionPreferences
    private var mViewModel: EditProfileViewModel? = null
    var mCountryList: ArrayList<CountryListResponse.Data> = ArrayList()
    //For Image
    private val GALLERY_REQUEST = 130
    private val REQUEST_CAMERA = 120
    private val STORAGE_PERMISSIONS = 3
    private var image_picker: Image_Picker? = null
    private lateinit var IMAGE_PATH: File
    private lateinit var file: File

    private var isOpenCountryDialog: Boolean = false

    companion object {

        fun getIntent(context: Context): Intent {
            val intent = Intent(context, EditProfileActivity::class.java);
            return intent
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_edit_profile
    }

    override fun getViewModel(): EditProfileViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(EditProfileViewModel::class.java)
        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)

        getControls()
    }

    fun getControls() {
        mContext = this@EditProfileActivity
        image_picker = Image_Picker(mContext)
        mSessionPref = SessionPreferences(mContext!!)
        mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)

        mBinding.layoutToolbar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.layoutToolbar.tvTitle.text = getAppString(R.string.edit_profile)
        mBinding.layoutToolbar.ibNotification.visibility = View.INVISIBLE

        mBinding.layoutToolbar.ibBack.visibility = View.VISIBLE

        mViewModel!!.callProfileAPI()
    }

    override fun onResume() {
        super.onResume()
        setProfileData()
    }

    fun setProfileData() {
        mSessionPref = SessionPreferences(mContext!!)
    }

    override
    fun onDestroy() {
        super.onDestroy()
    }

    override fun onPause() {
        super.onPause()
    }

    override fun updatePictureClick() {
        isStoragePermissionGranted()
    }

    override fun saveClick() {
        mViewModel?.updateProfileAPI()
    }

    override fun cancelClick() {
        finish()
    }

    override fun onDateOfBirthClick() {
        AppUtil.hideSoftKeyBoard((mContext as Activity?)!!, mBinding.etDateOfBirth)
        onPickDate()
    }

    fun onPickDate(){
        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            // Display Selected date in textbox
//            mBinding.tvDate.setText(DateUtil.getDateDDMMMYYYY(""+dayOfMonth+"-"+(monthOfYear+1)+"-"+year))
            mBinding.etDateOfBirth.setText((""+dayOfMonth+"-"+(monthOfYear+1)+"-"+year))
            mViewModel!!.birthDayStr = (""+dayOfMonth+"-"+(monthOfYear+1)+"-"+year)

        }, year, month, day)

        val cc = Calendar.getInstance()
        cc.add(Calendar.YEAR,  -18)
        dpd.datePicker.maxDate =cc.timeInMillis

        dpd.show()
    }

    override fun onAddressClick() {

    }

    override fun countryCodeClick() {
        try {
            isOpenCountryDialog = false;
            val jsonFileString = Methods.getJsonDataFromAsset(mContext!!, "CountryCode.json")
            val bean = Gson().fromJson(jsonFileString, CountryListResponse.CountryBean::class.java)
            mCountryList = bean.data as ArrayList<CountryListResponse.Data>
            showCountryListDialog(mCountryList, getAppString(R.string.country))
        }catch (e: Exception){
            Log.e("Exception", "countryCodeClick: "+e.message)
        }
    }

    /**********Show country code dialog  */
    var mSelectedCountry: CountryListResponse.Data? = null;

    private fun showCountryListDialog(
        list: java.util.ArrayList<CountryListResponse.Data>,
        placeHolder: String
    ) {
        lateinit var dialogFrag: CountryDialogListFragment
        try {
            dialogFrag = CountryDialogListFragment.newInstance(
                getAppString(R.string.app_name),
                list,
                placeHolder
            )
            AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)

            dialogFrag.show(supportFragmentManager, "countrystate-dialog")
            dialogFrag.setCancelable(false)

            // val ft: FragmentTransaction = fragmentManager.beginTransaction()

        }
        catch(e:Exception)
        {
            Log.e("dialog_exception","--->"+e.toString())
        }
        dialogFrag.setOnDialogClickedListener(object :
            CountryDialogListFragment.OnDialogClickedListener {
            override fun onDialogYes() {
                dialogFrag.dismiss()

                AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)
            }

            override fun onDialogNo() {
                dialogFrag.dismiss()

                AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)
            }

            override fun selectedDialogItem(
                dialogPos: Int,
                selectedData: CountryListResponse.Data
            ) {
                AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)

                dialogFrag.dismiss()

                if (!selectedData.phonecode.contains("+")) {
                    selectedData.phonecode = "+" + selectedData.phonecode
                } else {
                    selectedData.phonecode = "" + selectedData.phonecode
                }
                mSelectedCountry = selectedData
                mViewModel?.codeStr=mSelectedCountry!!.phonecode
                mBinding.etcontryCode.setText((mSelectedCountry!!.phonecode))
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onResponse(eventType: String, response: String) {
        try {

            showLoaderOnRequest(false)

            if (eventType == AppConstants.GET_PROFILE_API){

                val mBean = Gson().fromJson(response, AccountInformationModel::class.java)
                mBinding.etFullName.setText(mBean.data.profile.fullName)
                mBinding.etPhone.setText(mBean.data.profile.userPhone)
                mBinding.etDateOfBirth.setText(mBean.data.profile.dob)

            }else if(eventType == AppConstants.UPDATE_PROFILE_API){
                val json = JSONObject(response)
                DialogUtil.okCancelDialog(mContext!!,
                    getAppString(R.string.app_name),
                    json.optString("message"),
                    getAppString(R.string.ok),
                    getString(R.string.no),
                    true,
                    false,
                    object : DialogUtil.Companion.selectOkCancelListener {
                        override fun okClick() {

                            finish()
                        }

                        override fun cancelClick() {

                        }

                    })
            }

        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: "+e.message)
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            });
    }

    fun isStoragePermissionGranted(): Boolean {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
            ) {
                selectImage()
                return true
            } else {
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA),
                    STORAGE_PERMISSIONS
                )
                return false
            }
        } else {
            selectImage()
            return true
        }
    }

    private fun selectImage() {
        val items = arrayOf<CharSequence>("Take Photo", "Choose from Library", "Cancel")
        val builder = AlertDialog.Builder(this@EditProfileActivity)
        builder.setTitle("Add Photo!")
        builder.setItems(items, DialogInterface.OnClickListener { dialog, item ->
            if (items[item] == "Take Photo") {
                cameraIntent()
            } else if (items[item] == "Choose from Library") {
                galleryIntent()
            } else if (items[item] == "Cancel") {
                dialog.dismiss()
            }
        })
        builder.show()
    }

    private fun cameraIntent() {


        val picIntent =
            Intent(MediaStore.ACTION_IMAGE_CAPTURE).addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)


        val file_path = Environment.getExternalStorageDirectory().toString() +
                "/" + resources.getString(R.string.app_name)

        val dir = File(file_path)
        if (!dir.exists())
            dir.mkdirs()
        IMAGE_PATH =
            File(dir, resources.getString(R.string.app_name) + System.currentTimeMillis() + ".png")


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            picIntent.putExtra(
                MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(
                    this,
                    "$packageName.provider", IMAGE_PATH
                )
            )
        } else {
            picIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(IMAGE_PATH))
        }

        startActivityForResult(picIntent, REQUEST_CAMERA)
    }

    private fun galleryIntent() {
        val intent = Intent()
        intent.action = Intent.ACTION_PICK
        intent.type = "image/*"
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION or Intent.FLAG_GRANT_WRITE_URI_PERMISSION)
        startActivityForResult(intent, GALLERY_REQUEST)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == STORAGE_PERMISSIONS) {
            if (grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                selectImage()
            } else {

            }
        }
    }


    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) { //after a place is searched
        super.onActivityResult(requestCode, resultCode, data)

        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == GALLERY_REQUEST) {

                val selectedImageURI = data?.getData()

                try {

                    if (image_picker?.checkURI(selectedImageURI)!!) {
                        image_picker?.cropImage(this, selectedImageURI, 0)
                    } else {
                        image_picker?.cropImage(
                            this,
                            image_picker?.getImageUrlWithAuthority(this, selectedImageURI),
                            0)
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                }

            } else if (requestCode == REQUEST_CAMERA) {

                //  onCaptureImageResult(data);

                image_picker?.cropImage(this, Uri.fromFile(IMAGE_PATH), 0)

            } else {

                if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                    val result = CropImage.getActivityResult(data)
                    if (resultCode == Activity.RESULT_OK) {
                        val resultUri = result.getUri()
                        mViewModel!!.profileImagePath =
                            image_picker?.getRealPathFromURI(resultUri).toString()

                        file = File(mViewModel!!.profileImagePath)
                        Glide.with(this).load(mViewModel!!.profileImagePath)
                            .into(mBinding.ivProfile)


                    } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                        val error = result.getError()
                    }
                } else {
                    super.onActivityResult(requestCode, resultCode, data)
                }

            }

        }

    }




}