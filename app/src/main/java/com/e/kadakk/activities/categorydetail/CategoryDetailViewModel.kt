package com.e.kadakk.activities.categorydetail

import android.app.Application
import com.e.kadakk.activities.categorydetail.model.CategoryDetailModel
import com.e.kadakk.activities.dashboard.search.model.SearchModel
import com.e.kadakk.activities.editprofile.EditProfileNavigator
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.*


class CategoryDetailViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<CategoryDetailNavigator>(application) {

    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    var profileImagePath: String = ""
    var birthDayStr = ""
    var mobilenumberStr: String = ""
    var codeStr = ""
    var mFullNameStr = ""
    var mEmailStr = ""
    var mPosition: Int = 0
    var offerId = ""
    lateinit var mSessionPref: SessionPreferences

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    fun onGrabClick(){
        getNavigator().grabClick()
    }

    fun onFavoriteClick(position: Int,  data: CategoryDetailModel.Data.SimilarOffer){

        getNavigator().onFavoriteClick(position, data)
    }


    fun onItemClick(position: Int,  data: CategoryDetailModel.Data.SimilarOffer){

        getNavigator().onItemClick(position, data)
    }


    fun onGrabNowClick(position: Int,  data: CategoryDetailModel.Data.SimilarOffer){

        getNavigator().onGrabNowClick(position, data)
    }


    fun callOfferDetailsAPI() {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

//   ?offer_id=S7MytFIyVLIGAA=="
        var json = JsonElementUtil.getJsonObject(
            "offer_id", offerId

        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.OFFER_DETAILS_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""

            ),
            AppConstants.OFFER_DETAILS_API, serverResponse
        )

    }


    fun callFavoriteAPI(offerId: String) {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        //get request

        /*
        "{
        ""user_id"":""S7MytlIyMjRRsgYA"",
        ""offer_id"": ""S7MytFIyVLIGAA==""
}"
          */
        val jsonObj = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "offer_id", offerId

        );

        mRxApiCallHelper.call(mApiInterface.postRequest(AppConstants.ADD_WISH_LIST_API, jsonObj!!,  mSessionPref.getFirebaseRegIdPrefValue() , "", mSessionPref.mLoginData!!.data.login.authorization, "", "0.0", "0.0"),
            AppConstants.ADD_WISH_LIST_API, serverResponse)

    }

    fun addToCardAPI(qty : String,offerId: String) {

        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        //get request

        /*
        "{
        ""user_id"":""S7MytlIyMjRRsgYA"",
        ""offer_id"": ""S7MytFIyVLIGAA==""
}"
          */
        val jsonObj = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "offer_id", offerId,
            "quantity",qty
        )

        mRxApiCallHelper.call(mApiInterface.postRequest(AppConstants.ADD_TO_CARD_API, jsonObj!!,  mSessionPref.getFirebaseRegIdPrefValue() , "", mSessionPref.mLoginData!!.data.login.authorization, "", "0.0", "0.0"),
            AppConstants.ADD_TO_CARD_API, serverResponse)

    }

}