package com.e.kadakk.activities.dashboard.profile.accountInformations

import android.app.Application
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.*



class AccountInformationViewModel (application: Application, serverResponse: serverResponseNavigator) : BaseViewModel<AccountInformationNavigator>(application) {

    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface

    lateinit var mSessionPref: SessionPreferences

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

     fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }


    /******* API implementation ******/
    fun profileAPI() {

        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        //get request

        val jsonObj = JsonElementUtil.getJsonObject()

        mRxApiCallHelper.call(mApiInterface.getRequest(
            AppConstants.GET_PROFILE_API,
            "",
            "",
            mSessionPref.mLoginData!!.data.login.authorization,
            "",
            "",
            ""
        ), AppConstants.GET_PROFILE_API, serverResponse)

    }

}