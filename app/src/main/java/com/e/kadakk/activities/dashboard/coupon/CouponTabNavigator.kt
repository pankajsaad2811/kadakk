package com.e.kadakk.activities.dashboard.coupon

import com.e.kadakk.activities.dashboard.coupon.model.CouponTabModel

interface CouponTabNavigator {
    fun showMessage(msg:  String)
    fun onSearchClick()
    fun onCategoryItemClick(position: Int)
    fun onBennerItemClick(position: Int, data: CouponTabModel.Data.ByCategory.CatOffer)
    fun onOfferItemClick(position: Int, data: CouponTabModel.Data.ByCategory.CatOffer)
    fun onBennerGrabNowClick(position: Int,data: CouponTabModel.Data.ByCategory.CatOffer)
    fun onViewAllClick(position: Int)
    fun onFavoriteClick(position: Int, innerPosition:Int, data: CouponTabModel.Data.ByCategory.CatOffer)


}