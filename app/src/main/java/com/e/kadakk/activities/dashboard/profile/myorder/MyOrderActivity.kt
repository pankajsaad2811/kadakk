package com.e.kadakk.activities.dashboard.profile.myorder

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TableLayout
import androidx.lifecycle.ViewModelProvider
import androidx.viewpager.widget.ViewPager
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityMyOrderBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.android.material.tabs.TabLayout



class MyOrderActivity :  BaseActivity<ActivityMyOrderBinding, MyOrderViewModel>(), MyOrderNavigator, ToolBarNavigator, serverResponseNavigator {
    var TAG: String = MyOrderActivity::class.java.simpleName
    var mToolBarViewModel: ToolBarViewModel? = null;
    public lateinit var mBinding: ActivityMyOrderBinding
    private var mContext: Context? = null
    private var mViewModel: MyOrderViewModel? = null
    var viewPager: ViewPager? = null

    companion object {
        fun getIntent(context: Context): Intent {
            var intent = Intent(context, MyOrderActivity::class.java)
            return intent
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_my_order
    }

    override fun getViewModel(): MyOrderViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(MyOrderViewModel::class.java)

        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)
        // getIntentData()
        getControls()
    }

    fun getControls() {
        mContext = this@MyOrderActivity
        // mSessionPref = SessionPref(mContext!!)
        // mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)

        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.toolBar.tvTitle.text = getAppString(R.string.my_order)
        mBinding.toolBar.ibNotification.visibility = View.INVISIBLE

        mBinding.toolBar.ibBack.visibility = View.VISIBLE
        mBinding.toolBar.ivHeaderLogo.visibility = View.GONE

        val tabLayout = mBinding.tabs
        viewPager = mBinding.viewPager

        tabLayout.addTab(tabLayout.newTab().setText("Ongoing"))
        tabLayout.addTab(tabLayout.newTab().setText("Complete"))
        tabLayout.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = MyOrderTabAdapter(this, supportFragmentManager, tabLayout.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })

    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {

    }

    override fun onResponse(eventType: String, response: String) {

    }

    override fun onRequestFailed(eventType: String, response: String) {

    }

    override fun onRequestRetry() {

    }

    override fun onSessionExpire() {

    }

    override fun onMinorUpdate() {

    }

    override fun onAppHardUpdate() {

    }

    override fun noNetwork() {

    }
}