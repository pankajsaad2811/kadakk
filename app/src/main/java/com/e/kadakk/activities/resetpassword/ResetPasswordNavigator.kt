package com.e.kadakk.activities.resetpassword

interface ResetPasswordNavigator {

    fun showMessage(msg:  String)
    fun onUpdateClick()

}