package com.e.kadakk.activities.checkout.model

import com.e.kadakk.activities.categorydetail.model.CategoryDetailModel
import com.google.gson.annotations.SerializedName


data class CheckoutModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("errorcode")
    val errorcode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("version")
    val version: CategoryDetailModel.Version
) {

    data class Data(
        @SerializedName("default_address")
        val defaultAddress: DefaultAddress,
        @SerializedName("order_info")
        val order_info: OrderInfo,
        @SerializedName("list_card")
        val listCard: List<ListCard>
    ) {

        data class ListCard(
            @SerializedName("offer_id")
            val offer_id: String,
            @SerializedName("offer_image")
            val offerImage: String,
            @SerializedName("title")
            val title: String,
            @SerializedName("discount_value")
            val discountValue: String,
            @SerializedName("discount_type")
            val discountType: String,
            @SerializedName("offer_price")
            val offerPrice: String,
            @SerializedName("quantity")
            val quantity: String
        ){

        }

        data class OrderInfo(
            @SerializedName("order_id")
            val orderId: String,
            @SerializedName("total_items")
            val totalItems: Int,
            @SerializedName("total_price")
            val total_price: Int,
            @SerializedName("delivery_fees")
            val deliveryFees: String,
            @SerializedName("total_bill")
            val totalBill: String
        ){

        }

        data class DefaultAddress(
            @SerializedName("id")
            val id: String,
            @SerializedName("user_id")
            val userId: String,
            @SerializedName("username")
            val username: String,
            @SerializedName("address_type")
            val addressType: String,
            @SerializedName("address")
            val address: String,
            @SerializedName("latitude")
            val latitude: String,
            @SerializedName("longitude")
            val longitude: String,
            @SerializedName("country_code")
            val countryCode: String,
            @SerializedName("mobile")
            val mobile: String,
            @SerializedName("default_address")
            val defaultAddress: String,
            @SerializedName("created_at")
            val created_at: String
        ){

        }

    }

}