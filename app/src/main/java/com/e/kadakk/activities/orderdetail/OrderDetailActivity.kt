package com.e.kadakk.activities.orderdetail

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.trackmap.TrackMapActivity
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityOrderDetailBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson

class OrderDetailActivity : BaseActivity<ActivityOrderDetailBinding, OrderDetailViewModel>(),
    OrderDetailNavigator, ToolBarNavigator, serverResponseNavigator {

    var TAG: String = OrderDetailActivity::class.java.simpleName
    var mToolBarViewModel: ToolBarViewModel? = null;
    public lateinit var mBinding: ActivityOrderDetailBinding
    private var mContext: Context? = null
    private var mViewModel: OrderDetailViewModel? = null
    var mShowNetworkDialog: Dialog? = null
    lateinit var mSessionPref: SessionPreferences
    var status = ""
    var totalBill = ""
    var totalItems = ""
    var shopName = ""
    var offerImage = ""
    var email = ""
    var contact = ""
    var latitude = ""
    var longitude = ""

    lateinit var mBean :OrderDetailModel
    var descriptionData = arrayOf("Pending", "Arrange", "Complete")

    companion object {

        fun getIntent(context: Context, bundle: Bundle): Intent {
            val intent = Intent(context, OrderDetailActivity::class.java)
            intent.putExtra("bundle-data", bundle)
            return intent
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_order_detail
    }

    override fun getViewModel(): OrderDetailViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(OrderDetailViewModel::class.java)

        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)
        getIntentData()
        getControls()
        //mViewModel!!.profileAPI()
    }

    /**
     * Get intent data
     */
    private fun getIntentData() {
        if (intent.getBundleExtra("bundle-data") != null) {
            mViewModel!!.orderId = intent.getBundleExtra("bundle-data")!!.getString("orderId")!!
            status = intent.getBundleExtra("bundle-data")!!.getString("status")!!
            totalBill = intent.getBundleExtra("bundle-data")!!.getString("total_bill")!!
            totalItems = intent.getBundleExtra("bundle-data")!!.getString("total_item")!!

            offerImage = intent.getBundleExtra("bundle-data")!!.getString("offer_image")!!
            shopName = intent.getBundleExtra("bundle-data")!!.getString("shop_name")!!
            email = intent.getBundleExtra("bundle-data")!!.getString("email")!!
            contact = intent.getBundleExtra("bundle-data")!!.getString("contact")!!
            latitude = intent.getBundleExtra("bundle-data")!!.getString("latitude")!!
            longitude = intent.getBundleExtra("bundle-data")!!.getString("longitude")!!
        }
    }

    fun getControls() {
        mContext = this@OrderDetailActivity
        mSessionPref = SessionPreferences(mContext!!)
        mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)

        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.toolBar.tvTitle.text = "#"+mViewModel!!.orderId
        mBinding.toolBar.ibNotification.visibility = View.INVISIBLE
        mBinding.toolBar.ibBack.visibility = View.VISIBLE
        mBinding.toolBar.ivHeaderLogo.visibility = View.GONE

       mBinding.statusProgressbar.setStateDescriptionData(descriptionData)
       mBinding.statusProgressbar.enableAnimationToCurrentState(true)

        mViewModel!!.callOrderDetailsAPI()

    }

    override fun trackMapClick() {
        val bundle: Bundle = Bundle()
        bundle.putString("offer_image",offerImage)
        bundle.putString("shop_name", shopName)
        bundle.putString("email", email)
        bundle.putString("contact", contact)
        bundle.putString("latitude", latitude)
        bundle.putString("longitude", longitude)
        startActivity(TrackMapActivity.getIntent(mContext!!, bundle))
    }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {

        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })

    }

    override fun onResponse(eventType: String, response: String) {
        try {
            showLoaderOnRequest(false)
            if (eventType == AppConstants.ORDER_DETAIL_API){
                mBean = Gson().fromJson(response, OrderDetailModel::class.java)
                mBinding.tvTotalAmount.text = ""+mBean.data.totalAmount
                mBinding.tvDeliveryFee.text = ""+mBean.data.deliveryFees
                mBinding.tvTotalBill.text = mContext!!.getString(R.string.rs)+totalBill
                mBinding.tvTotalItem.text = totalItems+" items"
            }
        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: " + e.message)
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }


}