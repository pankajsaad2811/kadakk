package com.e.kadakk.activities.notification

import android.app.Application
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.*


/**
 * Created by Satish Patel on 18,December,2020
 */
class NotificationViewModel (application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<NotificationNavigator>(application) {

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    lateinit var mSessionPref: SessionPreferences
    var notificationStatus = ""

    fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    fun callNotificationAPI() {

        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val json = JsonElementUtil.getJsonObject(
            "user_id", mSessionPref.mLoginData!!.data.login.userId,
            "notification_status",notificationStatus
        )

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.ACCOUNT_SETTING_API, json!!,
                mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                mSessionPref.mLoginData!!.data.login.authorization,
                "",
                "",
                ""),
            AppConstants.ACCOUNT_SETTING_API, serverResponse
        )

    }

}