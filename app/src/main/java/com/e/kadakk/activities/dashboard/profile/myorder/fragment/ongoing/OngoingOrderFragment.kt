package com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.DashboardActivity
import com.e.kadakk.activities.dashboard.cart.adapter.CartTabAdapter
import com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing.model.OngoingModel
import com.e.kadakk.activities.orderdetail.OrderDetailActivity
import com.e.kadakk.activities.orderitems.OrderItemsActivity
import com.e.kadakk.activities.trackmap.TrackMapActivity
import com.e.kadakk.base.BaseFragment
import com.e.kadakk.databinding.FragmentOngoingOrderBinding
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson


class OngoingOrderFragment  : BaseFragment<FragmentOngoingOrderBinding,OngoingOrderViewModel>(),
    OngoingOrderNavigator, serverResponseNavigator {

    lateinit var mOngoingOrderAdapter: OngoingOrderAdapter
    var mList: ArrayList<OngoingModel.Data.ListOrder?> = ArrayList()
    var mViewModel: OngoingOrderViewModel? = null
    lateinit var mBinding: FragmentOngoingOrderBinding
    lateinit var mContext: Context
    var mShowNetworkDialog: Dialog? = null
    lateinit var mSessionPref: SessionPreferences

    companion object {

        @JvmStatic
        fun newInstance() = OngoingOrderFragment().apply {

        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {

        return R.layout.fragment_ongoing_order
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel!!.setNavigator(this)
    }

    override fun getViewModel(): OngoingOrderViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(activity!!.application, this))
            .get(OngoingOrderViewModel::class.java)

        return mViewModel!!
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding = getViewDataBinding()
        getControls()
        // mViewModel!!.callCouponOfferAPI()
    }

    @SuppressLint("NewApi")
    fun getControls() {
        mContext = this@OngoingOrderFragment.activity!!
        mSessionPref = SessionPreferences(mContext)
        mViewModel!!.setSessionPref(mSessionPref)

        mOngoingOrderAdapter = OngoingOrderAdapter(mContext, mViewModel!!)
        mBinding.rvList.layoutManager = LinearLayoutManager(mContext)
        mBinding.rvList.adapter = mOngoingOrderAdapter

        mViewModel!!.callOngoingListAPI()

    }


    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel!!.setNavigator(this)
        mBinding = getViewDataBinding()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun gotoShoppingClick() {
        //move to continue shopping
        startActivity(DashboardActivity.getIntent(mContext,true))
    }

    override fun onItemClick(pos: Int, data: OngoingModel.Data.ListOrder?) {

        val bundle: Bundle = Bundle()
        bundle.putString("orderId", data!!.orderId)
        bundle.putString("status", data.status)
        bundle.putString("total_bill", data.totalBill)
        bundle.putString("total_item", data.totalItems)
        startActivity(OrderItemsActivity.getIntent(mContext,bundle))
        //  startActivity(TrackMapActivity.getIntent(mContext))
    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })

    }

    override fun onResponse(eventType: String, response: String) {
        try {
            showLoaderOnRequest(false)
            if(eventType == AppConstants.ORDER_LIST_API){
                val mBean = Gson().fromJson(response, OngoingModel::class.java)
                mList = mBean.data.listOrder as  ArrayList<OngoingModel.Data.ListOrder?>
                mOngoingOrderAdapter.setList(mList)
            }

        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: "+e.message)
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }

}