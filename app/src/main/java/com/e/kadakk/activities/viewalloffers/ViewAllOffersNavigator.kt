package com.e.kadakk.activities.viewalloffers

import android.view.View

interface ViewAllOffersNavigator {
    fun showMessage(msg:  String)
    fun onFavoriteClick(position: Int)
    fun onGrabClick(position: Int)
}