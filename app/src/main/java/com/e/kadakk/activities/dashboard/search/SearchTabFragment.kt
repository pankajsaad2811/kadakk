package com.e.kadakk.activities.dashboard.search

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.GridLayoutManager
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.categorydetail.CategoryDetailActivity
import com.e.kadakk.activities.dashboard.search.adapter.SearchTabAdapter
import com.e.kadakk.activities.dashboard.search.model.SearchModel

import com.e.kadakk.base.BaseActivity
import com.e.kadakk.base.BaseFragment
import com.e.kadakk.base.GridSpacesItemDecoration
import com.e.kadakk.databinding.FragmentSearchTabBinding
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson

class SearchTabFragment : BaseFragment<FragmentSearchTabBinding, SearchTabViewModel>(),
    serverResponseNavigator,
    SearchTabNavigator {


    lateinit var mContext: Context
    //lateinit var mHomeBean: HomeTabResponse.HomeTabBean;

    lateinit var mSearchTabAdapter: SearchTabAdapter
    var mList: ArrayList<SearchModel.Data> = ArrayList()


    var mShowNetworkDialog: android.app.Dialog? = null;
    var mOkCancelDialog: Dialog? = null;
    lateinit var mSessionPref: SessionPreferences


    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_search_tab
    }

    override fun getViewModel(): SearchTabViewModel {
        mViewModel =
            ViewModelProviders.of(this, ViewModelProviderFactory(activity!!.application, this))
                .get(SearchTabViewModel::class.java)

        return mViewModel!!

    }

    var mViewModel: SearchTabViewModel? = null
    lateinit var mBinding: FragmentSearchTabBinding;

    companion object {

        val SPECIAL_ITEM_ROW = 2;

        @JvmStatic
        fun newInstance() =
            SearchTabFragment().apply {

            }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mViewModel!!.setNavigator(this)


    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mBinding = getViewDataBinding()
        getControls()
//        mViewModel!!.callCouponOfferAPI()
    }

    @SuppressLint("NewApi")
    fun getControls() {
        mContext = this@SearchTabFragment.activity!!
        mSessionPref = SessionPreferences(mContext)
        mViewModel!!.setSessionPref(mSessionPref)


        mSearchTabAdapter = SearchTabAdapter(mContext, mViewModel!!)
        mBinding.rvSearch.layoutManager = GridLayoutManager(mContext, 2)
        mBinding.rvSearch.adapter = mSearchTabAdapter
        val spanCount =
            SPECIAL_ITEM_ROW // 3 columns
        val spacing = 10 // 50px
        val includeEdge = false
        mBinding.rvSearch.addItemDecoration(
            GridSpacesItemDecoration(
                spanCount,
                spacing,
                includeEdge
            )
        )

        mBinding.etSearch.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(p0: Editable?) {

            }

            override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

            }

            override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

                mViewModel!!.callSearchAPI(false, p0.toString())

            }


        })



        mViewModel!!.callSearchAPI(true, "")




    }


    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing()) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    fun setData() {


    }


    override fun onResponse(eventType: String, response: String) {
        showLoaderOnRequest(false)

        try {

            if (eventType == AppConstants.SEARCH_OFFER_API) {

                var mBean = Gson().fromJson(response, SearchModel::class.java)
                mList = mBean.data as ArrayList<SearchModel.Data>
                mSearchTabAdapter.setList(mList)


            }else if(eventType == AppConstants.ADD_WISH_LIST_API){


                if (mList.get(mViewModel!!.mPosition).isFavourite == 0) {
                    mList.get(mViewModel!!.mPosition).isFavourite =
                        1
                }else{
                    mList.get(mViewModel!!.mPosition).isFavourite =
                        0
                }
                mSearchTabAdapter.notifyItemChanged(mViewModel!!.mPosition)

            }


        } catch (e: Exception) {
            AppUtil.showLogs("Exception", "onResponse: " + e.message)

        }

    }


    override fun onPause() {
        super.onPause()

    }


    override fun onResume() {
        super.onResume()

//        pageNo = 1
//        mViewModel!!.callChatUsersListAPI(pageNo)
    }


    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)

    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }


    override fun noNetwork() {
        showLoaderOnRequest(false)
        BaseActivity.showErrorMessage(
            mContext,
            getString(R.string.No_internet_connection),
            false
        )
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), "Session expire", getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

//                    startActivity(LoginActivity.getIntent(mContext))
//                    activity!!.finishAffinity()
//                    mSessionPref.clearSession()

                }

                override fun cancelClick() {

                }

            });
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun showMessage(msg: String) {

        mOkCancelDialog = DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name), msg, getAppString(R.string.ok),
            "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }
            })
    }

    override fun onFavoriteClick(position: Int, data: SearchModel.Data) {
        mViewModel!!.mPosition = position

        mViewModel!!.callFavoriteAPI(data.offerId)
    }

    override fun onItemClick(position: Int, data: SearchModel.Data) {
        var bundle: Bundle = Bundle()
        bundle.putString("offerId", data.offerId)
        startActivity(CategoryDetailActivity.getIntent(mContext!!, bundle))

    }


    override fun onDestroy() {
        super.onDestroy()

    }


}