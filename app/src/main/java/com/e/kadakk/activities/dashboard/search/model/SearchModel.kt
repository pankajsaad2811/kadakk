package com.e.kadakk.activities.dashboard.search.model


import com.google.gson.annotations.SerializedName

data class SearchModel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("errorcode")
    val errorcode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("version")
    val version: Version
) {
    data class Data(
        @SerializedName("calory")
        val calory: String,
        @SerializedName("discount_type")
        val discountType: String,
        @SerializedName("discount_value")
        val discountValue: String,
        @SerializedName("is_favourite")
        var isFavourite: Int,
        @SerializedName("offer_id")
        val offerId: String,
        @SerializedName("offer_image")
        val offerImage: String,
        @SerializedName("short_description")
        val shortDescription: String,
        @SerializedName("time")
        val time: String,
        @SerializedName("title")
        val title: String
    )

    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )
}