package com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing.model

import com.google.gson.annotations.SerializedName


data class OngoingModel( @SerializedName("data")
                         val `data`: Data,
                         @SerializedName("errorcode")
                         val errorcode: Int,
                         @SerializedName("message")
                         val message: String,
                         @SerializedName("status")
                         val status: Int,
                         @SerializedName("version")
                         val version: Version) {
    data class Data(
        @SerializedName("list_order")
        val listOrder: List<ListOrder>
    ){

        data class ListOrder(
            @SerializedName("order_id")
            val orderId: String,
            @SerializedName("total_items")
            val totalItems: String,
            @SerializedName("total_bill")
            val totalBill: String,
            @SerializedName("status")
            val status: String,
            @SerializedName("order_date")
            val orderDate: String
        ){

        }

    }

    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )
}