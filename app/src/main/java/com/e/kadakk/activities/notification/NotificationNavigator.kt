package com.e.kadakk.activities.notification


/**
 * Created by Satish Patel on 18,December,2020
 */
interface NotificationNavigator {
    fun showMessage(msg:  String)
}