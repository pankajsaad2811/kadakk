package com.e.kadakk.activities.dashboard.profile.myorder

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.e.kadakk.activities.dashboard.profile.ProfileTabFragment
import com.e.kadakk.activities.dashboard.profile.myorder.fragment.complete.CompleteOrderFragment
import com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing.OngoingOrderFragment



class MyOrderTabAdapter (private val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {

        if(position==0){
            return OngoingOrderFragment.newInstance()
        }else{
            return CompleteOrderFragment.newInstance()
        }
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}