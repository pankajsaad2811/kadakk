package com.e.kadakk.activities.editprofile



interface EditProfileNavigator {
    fun showMessage(msg:  String)
    fun saveClick()
    fun updatePictureClick()
    fun cancelClick()
    fun onDateOfBirthClick()
    fun onAddressClick()
    fun countryCodeClick()
}