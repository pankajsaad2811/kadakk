package com.e.kadakk.activities.choosetopics.model


import com.google.gson.annotations.SerializedName

data class ChooseCategoryModel(
    @SerializedName("data")
    val `data`: List<Data>,
    @SerializedName("errorcode")
    val errorcode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("version")
    val version: Version
) {
    data class Data(
        @SerializedName("cat_id")
        val catId: String,
        @SerializedName("cat_img")
        val catImg: String,
        @SerializedName("cat_name")
        val catName: String,
        @SerializedName("created_date")
        val createdDate: String,
        var isSelected: Boolean = false
    )

    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )
}