package com.e.kadakk.activities.dashboard.profile.myorder.fragment.complete

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing.OngoingOrderNavigator
import com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing.OngoingOrderViewModel
import com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing.model.OngoingModel
import com.e.kadakk.base.BaseFragment
import com.e.kadakk.databinding.FragmentCompleteOrderBinding
import com.e.kadakk.databinding.FragmentOngoingOrderBinding
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory



class CompleteOrderFragment : BaseFragment<FragmentCompleteOrderBinding, OngoingOrderViewModel>(),
    OngoingOrderNavigator, serverResponseNavigator {


    var mViewModel: OngoingOrderViewModel? = null
    lateinit var mBinding: FragmentCompleteOrderBinding
    lateinit var mContext: Context
    var mShowNetworkDialog: Dialog? = null

    companion object {

        @JvmStatic
        fun newInstance() = CompleteOrderFragment().apply {

        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {

        return R.layout.fragment_complete_order
    }

    override fun getViewModel(): OngoingOrderViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(activity!!.application, this))
            .get(OngoingOrderViewModel::class.java)

        return mViewModel!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mViewModel!!.setNavigator(this)
        mBinding = getViewDataBinding()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mContext = context
    }

    override fun gotoShoppingClick() {
        //move to continue shopping
    }

    override fun onItemClick(pos: Int, data: OngoingModel.Data.ListOrder?) {

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })

    }

    override fun onResponse(eventType: String, response: String) {
        TODO("Not yet implemented")
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }



}