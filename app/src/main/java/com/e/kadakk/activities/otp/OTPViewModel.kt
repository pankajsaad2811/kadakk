package com.e.kadakk.activities.otp

import android.app.Application
import android.text.TextUtils
import com.e.kadakk.R
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.Validator
import com.e.kadakk.utils.server.*


class OTPViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<OTPNavigator>(application) {

    lateinit var mApplication: Application
    lateinit var serverResponse: serverResponseNavigator
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    lateinit var mSessionPref: SessionPreferences

    var otpStr: String = ""
    var mobileStr: String = ""
    var mUserId: String = ""
    var isSignup = false

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }


    fun setSessionPref(mPref: SessionPreferences){
        mSessionPref = mPref
    }

    fun onProceedClick() {

        if (isValidAll() == "") {

            getNavigator().proceedClick()
          //  callOTPVerifyAPI()
        } else {
            getNavigator().showMessage(isValidAll())
        }

    }


    fun onResendOTPClick(){

        getNavigator().resendOTPClick()
    }

    /*
   * perform validation
   * */
    fun isValidAll(): String {
        if (TextUtils.isEmpty(otpStr)) {
            return getStringfromVM(R.string.please_enter_otp)
        } else if (!Validator.isOtpValid(otpStr)) {
            return getStringfromVM(R.string.please_enter_valid_otp)
        } else {
            return ""
        }


    }


    /**
     * Call forgot password API
     */
    public fun callOTPVerifyAPI() {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

//       "Json Method
//
//{
//    ""user_id"":""K7MyswYA"",
//    ""otp"":""123456""
//} "
        val jsonObj = JsonElementUtil.getJsonObject("user_id", mUserId, "otp", otpStr);

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.VERIFY_OTP_API,
                jsonObj!!,
                 mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                "",
                "",
                "",
                ""
            ),
            AppConstants.VERIFY_OTP_API, serverResponse
        )
    }


    /**
     * Call forgot password API
     */
    public fun resendOtpAPI() {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        val jsonObj = JsonElementUtil.getJsonObject("user_id", mUserId);

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.RESENDOTP_API,
                jsonObj!!, mSessionPref.getFirebaseRegIdPrefValue(),"",
                "",
                "",
                "",
                ""
            ),
            AppConstants.RESENDOTP_API, serverResponse
        )
    }

}