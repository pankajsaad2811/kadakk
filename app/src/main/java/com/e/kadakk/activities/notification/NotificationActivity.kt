package com.e.kadakk.activities.notification

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.databinding.ActivityNotificationSettingBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import org.json.JSONObject


/**
 * Created by Satish Patel on 18,December,2020
 */
class NotificationActivity : BaseActivity<ActivityNotificationSettingBinding, NotificationViewModel>(),
    NotificationNavigator, ToolBarNavigator, serverResponseNavigator {

    var TAG: String = NotificationActivity::class.java.simpleName
    var mToolBarViewModel: ToolBarViewModel? = null;
    public lateinit var mBinding: ActivityNotificationSettingBinding
    private var mContext: Context? = null
    private var mViewModel: NotificationViewModel? = null
    var mShowNetworkDialog: Dialog? = null
    lateinit var mSessionPref: SessionPreferences

    companion object {

        fun getIntent(context: Context): Intent {
            val intent = Intent(context, NotificationActivity::class.java)
            return intent
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_notification_setting
    }

    override fun getViewModel(): NotificationViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(NotificationViewModel::class.java)
        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)
        //getIntentData()
        getControls()
        //mViewModel!!.profileAPI()
    }

    fun getControls() {
        mContext = this@NotificationActivity
        mSessionPref = SessionPreferences(mContext!!)
        mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)

        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.toolBar.tvTitle.text = "Notification Setting"
        mBinding.toolBar.ibNotification.visibility = View.INVISIBLE
        mBinding.toolBar.ibBack.visibility = View.VISIBLE
        mBinding.toolBar.ivHeaderLogo.visibility = View.GONE


        mBinding.toggle.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                mViewModel!!.notificationStatus = "on"
                mViewModel!!.callNotificationAPI()
            }else{
                mViewModel!!.notificationStatus = "off"
                mViewModel!!.callNotificationAPI()
            }
        }

       // mViewModel!!.callNotificationAPI()

    }


    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {

        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {

                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })

    }

    override fun onResponse(eventType: String, response: String) {
        try {

            showLoaderOnRequest(false)

            if(eventType == AppConstants.ACCOUNT_SETTING_API){

                val json = JSONObject(response)

                DialogUtil.okCancelDialog(mContext!!,
                    getAppString(R.string.app_name),
                    json.optString("message"),
                    getAppString(R.string.ok),
                    getString(R.string.no),
                    true,
                    false,
                    object : DialogUtil.Companion.selectOkCancelListener {
                        override fun okClick() {
                            finish()
                        }

                        override fun cancelClick() {

                        }

                    })
            }
        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: " + e.message)
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }


}