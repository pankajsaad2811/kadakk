package com.e.kadakk.activities.editprofile

import android.app.Application
import android.text.TextUtils
import com.e.kadakk.R
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.Validator
import com.e.kadakk.utils.server.*
import okhttp3.MediaType
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.File



class EditProfileViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<EditProfileNavigator>(application) {

    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface
    var profileImagePath: String = ""
    var birthDayStr = ""
    var mobilenumberStr: String = ""
    var codeStr = ""
    var mFullNameStr = ""
    var mEmailStr = ""

    lateinit var mSessionPref: SessionPreferences

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }

    fun onDateOfBirthClick() {
        getNavigator().onDateOfBirthClick()
    }

    fun onAddressClick() {
        getNavigator().onAddressClick()
    }

    fun onCountryCodeClicked() {
        getNavigator().countryCodeClick()
    }

    fun saveClick() {

        if (isValidAll().equals("")){
            getNavigator().saveClick()
        }else{
            getNavigator().showMessage(isValidAll())
        }
    }

    fun cancelClick() {
        getNavigator().cancelClick()
    }

    fun onUpdatePictureClick() {
        getNavigator().updatePictureClick()
    }

    /*
* perform validation
* */
    fun isValidAll() :String{

        if (TextUtils.isEmpty(mFullNameStr)) {
            return getStringfromVM(R.string.please_enter_your_full_name)
        }else if (TextUtils.isEmpty(codeStr.trim())) {
            return getStringfromVM(R.string.please_select_country_code)
        } else if (TextUtils.isEmpty(mobilenumberStr.trim())) {
            return getStringfromVM(R.string.please_enter_phone_number)
        } else if (mobilenumberStr.trim().length < 10) {
            return getStringfromVM(R.string.please_enter_valid_phone_number)
        } else if (TextUtils.isEmpty(birthDayStr.trim())) {
            return getStringfromVM(R.string.please_select_date_of_birth)
        }else {
            return   "";
        }
    }

    /******* API implementation ******/
    fun callProfileAPI() {

        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        //get request

        val jsonObj = JsonElementUtil.getJsonObject()

        mRxApiCallHelper.call(mApiInterface.getRequest(
            AppConstants.GET_PROFILE_API,
            "",
            "",
            mSessionPref.mLoginData!!.data.login.authorization,
            "",
            "",
            ""
        ), AppConstants.GET_PROFILE_API, serverResponse)

    }


    /******* API implementation ******/
    fun updateProfileAPI() {

        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!


        val map = HashMap<String, RequestBody>()


        map.put("fullname", createRequestBody(mFullNameStr.trim()))
        map.put("country_code", createRequestBody(codeStr.trim()))
        map.put("user_phone", createRequestBody(mobilenumberStr.trim()))
        map.put("dob", createRequestBody(birthDayStr))


        if (profileImagePath == ""){

            mRxApiCallHelper.call(
                mApiInterface.updateProfileWithoutImageAPI(
                    map,
                    mSessionPref.mLoginData!!.data.login.authorization,
                    "123",
                    "India",
                    "",
                    "",
                    ""
                ),
                AppConstants.UPDATE_PROFILE_API,
                serverResponse
            )

        }else{
            val file = File(profileImagePath)
            val requestFile = RequestBody.create("multipart/form-data".toMediaTypeOrNull(), file)
            val body = MultipartBody.Part.createFormData("user_pic", file.getName(), requestFile)
           // val body = MultipartBody.Part.createFormData("profile_image", file.getName(), requestFile)
            mRxApiCallHelper.call(
                mApiInterface.updateProfileAPI(
                    map,
                    body,
                    mSessionPref.mLoginData!!.data.login.authorization,
                    "123",
                    "India",
                    "",
                    "",
                    ""
                ),
                AppConstants.UPDATE_PROFILE_API,
                serverResponse
            )

        }

    }

    fun createRequestBody(s: String): RequestBody {
        return RequestBody.create(
            "multipart/form-data".toMediaTypeOrNull(), s
        )
    }

}