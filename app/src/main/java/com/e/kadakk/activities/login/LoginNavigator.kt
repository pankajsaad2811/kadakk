package com.e.kadakk.activities.login

import android.view.View

interface LoginNavigator {
    fun showMessage(msg:  String)
    fun loginClick()
    fun forgotPasswordClick()
    fun faceBookClick()
    fun googleClick()
    fun onTwitterClick()
    fun createAccountClick()


}