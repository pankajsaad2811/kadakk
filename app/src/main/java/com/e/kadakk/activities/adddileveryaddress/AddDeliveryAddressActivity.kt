package com.e.kadakk.activities.adddileveryaddress

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.CompoundButton
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.BR
import com.e.kadakk.R
import com.e.kadakk.activities.changedeliveryaddress.model.ChangeDeliveryAddressModel
import com.e.kadakk.base.BaseActivity
import com.e.kadakk.country.CountryDialogListFragment
import com.e.kadakk.country.CountryListResponse
import com.e.kadakk.databinding.ActivityAddDeliveryAddressBinding
import com.e.kadakk.databinding.ActivityChangeDeliveryAddressBinding
import com.e.kadakk.toolbar.ToolBarNavigator
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.*
import com.e.kadakk.utils.server.serverResponseNavigator
import com.e.kadakk.utils.viewmodelfactory.ViewModelProviderFactory
import com.google.gson.Gson
import org.json.JSONObject


/**
 * Created by Satish Patel on 05,November,2020
 */
class AddDeliveryAddressActivity : BaseActivity<ActivityAddDeliveryAddressBinding, AddDeliveryAddressViewModel>(),
    serverResponseNavigator, AddDeliveryAddressNavigator, ToolBarNavigator, AdapterView.OnItemSelectedListener {

    var mViewModel: AddDeliveryAddressViewModel? = null
    lateinit var mBinding: ActivityAddDeliveryAddressBinding
    lateinit var mContext: Context
    var mShowNetworkDialog: Dialog? = null
    var mToolBarViewModel: ToolBarViewModel? = null
    var changeDeliveryAddressModel: ChangeDeliveryAddressModel? = null
    private lateinit var mSessionPref: SessionPreferences
    private var isOpenCountryDialog: Boolean = false
    private lateinit var mCountryList: ArrayList<CountryListResponse.Data>
    private var isDefaultAddress: String = "no"
    private var addressType: String = ""

    companion object {
        fun getIntent(context: Context): Intent {
            val intent = Intent(context, AddDeliveryAddressActivity::class.java);
            return intent;
        }
    }

    override fun getBindingVariable(): Int {
        return BR.viewModel
    }

    override fun getLayoutId(): Int {
        return R.layout.activity_add_delivery_address
    }

    override fun getViewModel(): AddDeliveryAddressViewModel {
        mViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(AddDeliveryAddressViewModel::class.java)
        return mViewModel!!
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mBinding = getViewDataBinding()
        mViewModel!!.setNavigator(this)
        getControls()
    }

    fun getControls() {
        mContext = this@AddDeliveryAddressActivity
        mSessionPref = SessionPreferences(mContext!!)
        mViewModel!!.setSessionPref(mSessionPref)

        mToolBarViewModel = ViewModelProvider(this, ViewModelProviderFactory(application, this))
            .get(ToolBarViewModel::class.java)
        mBinding.toolBar.toolviewModel = mToolBarViewModel
        mToolBarViewModel!!.setToolBarNavigator(this)
        mBinding.toolBar.tvTitle.text = getAppString(R.string.add_delivery_address)
        mBinding.toolBar.ibNotification.visibility = View.INVISIBLE
        mBinding.toolBar.ibBack.visibility = View.VISIBLE
        mBinding.toolBar.ivHeaderLogo.visibility = View.GONE

        mBinding.rbDefaultAddress.setOnCheckedChangeListener { buttonView, isChecked ->
            if (isChecked) {
                   isDefaultAddress = "yes"
                }else{
                   isDefaultAddress = "no"
                }
            }

        val adapter = ArrayAdapter.createFromResource(
            this,
            R.array.address_type,
            android.R.layout.simple_spinner_item
        )

        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        mBinding.spAddressType.adapter = adapter
        mBinding.spAddressType.onItemSelectedListener = this

      }

    override fun backIconClick() {
        onBackPressed()
    }

    override fun notificationIconClick() {

    }

    override fun onNothingSelected(parent: AdapterView<*>?) {

    }
    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        addressType = parent?.getItemAtPosition(position).toString()
    }

    override fun onResponse(eventType: String, response: String) {

        try {

            showLoaderOnRequest(false)

             if(eventType == AppConstants.ADD_ADDRESS_API){
                val json = JSONObject(response)

                DialogUtil.okCancelDialog(mContext!!,
                    getAppString(R.string.app_name),
                    json.optString("message"),
                    getAppString(R.string.ok),
                    getString(R.string.no),
                    true,
                    false,
                    object : DialogUtil.Companion.selectOkCancelListener {
                        override fun okClick() {
                            finish()
                        }

                        override fun cancelClick() {

                        }

                    })
            }

        } catch (e: Exception) {
            Log.e("Exception", "OnResponce: "+e.message)
        }
    }

    override fun showLoaderOnRequest(isShowLoader: Boolean) {
        if (isShowLoader && mShowNetworkDialog == null) {
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else if (isShowLoader && !mShowNetworkDialog!!.isShowing) {
            mShowNetworkDialog = null
            mShowNetworkDialog = DialogUtil.showLoader(mContext!!)
        } else {
            if (mShowNetworkDialog != null && isShowLoader == false) {
                DialogUtil.hideLoaderDialog(mShowNetworkDialog!!)
                mShowNetworkDialog = null
            }
        }
    }

    override fun onRequestFailed(eventType: String, response: String) {
        showLoaderOnRequest(false)
        showMessage(response)
    }

    override fun onRequestRetry() {
        showLoaderOnRequest(false)
    }

    override fun noNetwork() {
        showLoaderOnRequest(false)
    }

    override fun onSessionExpire() {
        showLoaderOnRequest(false)
    }

    override fun onMinorUpdate() {
        showLoaderOnRequest(false)
    }

    override fun onAppHardUpdate() {
        showLoaderOnRequest(false)
    }

    override fun showMessage(msg: String) {

        DialogUtil.okCancelDialog(mContext!!,
            getAppString(R.string.app_name),
            msg,
            getAppString(R.string.ok),
            getString(R.string.no),
            true,
            false,
            object : DialogUtil.Companion.selectOkCancelListener {
                override fun okClick() {

                }

                override fun cancelClick() {

                }

            })
    }

    override fun saveClick() {
        mViewModel?.callAddDeliveryAddressAPI(isDefaultAddress,addressType)
    }

    override fun countryCodeClick() {

        try {
            isOpenCountryDialog = false;
            val jsonFileString = Methods.getJsonDataFromAsset(mContext!!, "CountryCode.json")
            val bean = Gson().fromJson(jsonFileString, CountryListResponse.CountryBean::class.java)
            mCountryList = bean.data as ArrayList<CountryListResponse.Data>;
            showCountryListDialog(mCountryList, getAppString(R.string.country))
        }catch (e: Exception){
            Log.e("Exception", "countryCodeClick: "+e.message)
        }
    }

    /**********Show country code dialog  */
    var mSelectedCountry: CountryListResponse.Data? = null;

    private fun showCountryListDialog(
        list: java.util.ArrayList<CountryListResponse.Data>,
        placeHolder: String
    ) {
        lateinit var dialogFrag: CountryDialogListFragment
        try {
            dialogFrag = CountryDialogListFragment.newInstance(
                getAppString(R.string.app_name),
                list,
                placeHolder
            )
            AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)



            dialogFrag.show(supportFragmentManager, "countrystate-dialog")
            dialogFrag.setCancelable(false)

            // val ft: FragmentTransaction = fragmentManager.beginTransaction()

        }
        catch(e:Exception)
        {
            Log.e("dialog_exception","--->"+e.toString())
        }
        dialogFrag.setOnDialogClickedListener(object :
            CountryDialogListFragment.OnDialogClickedListener {
            override fun onDialogYes() {
                dialogFrag.dismiss()

                AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)
            }

            override fun onDialogNo() {
                dialogFrag.dismiss()

                AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)
            }

            override fun selectedDialogItem(
                dialogPos: Int,
                selectedData: CountryListResponse.Data
            ) {
                AppUtil.hideSoftKeyBoard(mContext!!, mBinding.etcontryCode)

                dialogFrag.dismiss()

                if (!selectedData.phonecode.contains("+")) {
                    selectedData.phonecode = "+" + selectedData.phonecode
                } else {
                    selectedData.phonecode = "" + selectedData.phonecode
                }
                mSelectedCountry = selectedData
                mViewModel?.codeStr=mSelectedCountry!!.phonecode
                mBinding.etcontryCode.setText((mSelectedCountry!!.phonecode))
            }
        })
    }

}