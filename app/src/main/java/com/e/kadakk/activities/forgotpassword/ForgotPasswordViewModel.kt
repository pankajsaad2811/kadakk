package com.e.kadakk.activities.forgotpassword

import android.app.Application
import android.text.TextUtils
import android.view.View.OnFocusChangeListener
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.e.kadakk.R
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.Validator
import com.e.kadakk.utils.server.*
import java.util.regex.Pattern

class ForgotPasswordViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<ForgotPasswordNavigator>(application) {
    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface

    var emailStr: String = ""
    lateinit var mSessionPref: SessionPreferences

    private val PASSWORD_PATTERN: Pattern =
        Pattern.compile("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#\$%^&*]).{8,20})")

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    public fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }




    public fun onSendClick() {

        if (isValidAll().equals("")){
            forgotPasswordAPI()
        }else{
            getNavigator().showMessage(isValidAll())
        }

    }



    /*
    * perform validation
    * */
    fun isValidAll() :String{
        if (TextUtils.isEmpty(emailStr)) {
            return getStringfromVM(R.string.Please_enter_email_id_or_mobile)
        }else if(!Validator.isValidMobile(emailStr) && !Validator.isEmailValid(emailStr)){
            return getStringfromVM(R.string.Please_enter_valid_email_Id)
        }else if(Validator.isValidMobile(emailStr) &&  !Validator.isMobileNumberValid(emailStr)){
            return getStringfromVM(R.string.Please_enter_valid_phone_number)
        }else {
            return   "";
        }


    }


    /******* API implementation ******/
    fun forgotPasswordAPI() {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        //get request

        /*
        "Json  data:

{
      ""email"":""retailer123@mailinator.com""
}"
          */
        val jsonObj = JsonElementUtil.getJsonObject("email", emailStr);

        mRxApiCallHelper.call(mApiInterface.postRequest(
            AppConstants.FORGOT_PASSWORD_API, jsonObj!!, mSessionPref.getFirebaseRegIdPrefValue() , "",
            "",
            "",
            "",
            ""

            ),
            AppConstants.FORGOT_PASSWORD_API, serverResponse)

    }


}