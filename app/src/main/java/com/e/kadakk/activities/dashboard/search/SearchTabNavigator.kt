package com.e.kadakk.activities.dashboard.search

import android.view.View
import com.e.kadakk.activities.dashboard.search.model.SearchModel

interface SearchTabNavigator {
    fun showMessage(msg:  String)
    fun onFavoriteClick(position: Int, data: SearchModel.Data)
    fun onItemClick(position: Int,  data: SearchModel.Data)


}