package com.e.kadakk.activities.login

import android.app.Application
import android.text.TextUtils
import android.view.View.OnFocusChangeListener
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.e.kadakk.R
import com.e.kadakk.base.BaseViewModel
import com.e.kadakk.utils.AppConstants
import com.e.kadakk.utils.AppUtil
import com.e.kadakk.utils.SessionPreferences
import com.e.kadakk.utils.Validator
import com.e.kadakk.utils.server.*
import java.util.regex.Pattern

class LoginViewModel(application: Application, serverResponse: serverResponseNavigator) :
    BaseViewModel<LoginNavigator>(application) {
    lateinit var serverResponse: serverResponseNavigator
    var mApplication: Application
    lateinit var mRxApiCallHelper: RxAPICallHelper
    lateinit var mApiInterface: ApiInterface

    var emailStr: String = ""
    var passwordStr: String = ""
    lateinit var mSessionPref: SessionPreferences

    private val PASSWORD_PATTERN: Pattern =
        Pattern.compile("((?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[~!@#\$%^&*]).{8,20})")

    init {
        mApplication = application
        this.serverResponse = serverResponse
    }

    public fun setSessionPref(mPref: SessionPreferences) {
        mSessionPref = mPref
    }


    fun onForgotPasswordClick(){
        getNavigator().forgotPasswordClick()
    }

    fun onFaceBookClick(){
        getNavigator().faceBookClick()
    }

    fun onGoogleClick(){
        getNavigator().googleClick()
    }

    fun onTwitterClick(){
        getNavigator().onTwitterClick()
    }

    fun createAccountClick(){
        getNavigator().createAccountClick()
    }

    public fun onLoginClick() {

        if (isValidAll().equals("")){
            loginAPI()
        }else{
            getNavigator().showMessage(isValidAll())
        }

    }



    /*
    * perform validation
    * */
    fun isValidAll() :String{
        if (TextUtils.isEmpty(emailStr)) {
            return getStringfromVM(R.string.Please_enter_email_id_or_mobile)
        }else if(!Validator.isValidMobile(emailStr) && !Validator.isEmailValid(emailStr)){
            return getStringfromVM(R.string.Please_enter_valid_email_Id)
        }else if(Validator.isValidMobile(emailStr) &&  !Validator.isMobileNumberValid(emailStr)){
            return getStringfromVM(R.string.Please_enter_valid_phone_number)
        }else if (TextUtils.isEmpty(passwordStr)) {
            return  getStringfromVM(R.string.Please_enter_password)
        } else if (passwordStr.length < 8) {
            return   getStringfromVM(R.string.Password_should_be_min_8_character)
        } /*else if (!Validator.isValidPassword(passwordStr)) {
            return   getStringfromVM(R.string.Please_enter_valid_password)
        } */else {
            return   "";
        }


    }


    /******* API implementation ******/
    fun loginAPI() {


        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!
        //get request

        /*
        "Json Method

{
    ""email"": ""prathak@mailinator.com"",
    ""password"":""12345678""
}"
          */
        val jsonObj = JsonElementUtil.getJsonObject("email", emailStr, "password", passwordStr);

        mRxApiCallHelper.call(mApiInterface.postRequest(
            AppConstants.LOGIN_API, jsonObj!!,
            mSessionPref.getFirebaseRegIdPrefValue() ,
            "",
            "",
            "",
            "",
            ""

            ),
            AppConstants.LOGIN_API, serverResponse)

    }

    /*
 * Place order
 * */
    fun socialLoginAPI(social_type: String, social_id:String, first_name:String, last_name:String, email:String, phone:String) {
        mRxApiCallHelper = RxAPICallHelper()
        mRxApiCallHelper.setDisposable(mCompositeDisposable)
        serverResponse.showLoaderOnRequest(true)
        mApiInterface = ApiBuilderSingleton.getInstance()!!

        /*  "{
          ""social_id"":""socialid"",
          ""login_type"":""Facebook"",
          ""name"":""name"",
          ""email"":""email"",
          ""mobile"":""mobile"",
          ""usertype"":""1"",
      }
      Note: login_type:Facebook,Google,Apple
      "*/


        val jsonObj = JsonElementUtil.getJsonObject("login_type", social_type,
            "social_id", social_id, "name", first_name +" "+ last_name,
            "email", email, "mobile", phone, "usertype", "3")

        mRxApiCallHelper.call(
            mApiInterface.postRequest(
                AppConstants.LOGIN_API,
                jsonObj!!,
                 mSessionPref.getFirebaseRegIdPrefValue(),
                "",
                "",
                "",
                "",
                ""
            ),
            AppConstants.LOGIN_API, serverResponse
        )
    }


}