package com.e.kadakk.activities.orderdetail

import com.google.gson.annotations.SerializedName


data class OrderDetailModel ( @SerializedName("data")
                              val `data`: Data,
                              @SerializedName("errorcode")
                              val errorcode: Int,
                              @SerializedName("message")
                              val message: String,
                              @SerializedName("status")
                              val status: Int,
                              @SerializedName("version")
                              val version: Version){

    data class Data(
        @SerializedName("list_item")
        val listItem: List<ListItem>,
        @SerializedName("total_amount")
        val totalAmount:Int,
        @SerializedName("delivery_fees")
        val deliveryFees:Double

    ){

        data class ListItem( @SerializedName("offer_id")
                             val offerId: String,
                             @SerializedName("offer_image")
                             val offerImage: String,
                             @SerializedName("title")
                             val title: String,
                             @SerializedName("offer_price")
                             val offerPrice: String,
                             @SerializedName("quantity")
                             val quantity: String,
                             @SerializedName("shop_id")
                             val shopId: String,
                             @SerializedName("shop_name")
                             val shopName: String,
                             @SerializedName("email")
                             val email: String,
                             @SerializedName("contact")
                             val contact: String,
                             @SerializedName("address")
                             val address: String,
                             @SerializedName("latitude")
                             val latitude: String,
                             @SerializedName("longitude")
                             val longitude: String
        ){

        }

    }


    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )

}