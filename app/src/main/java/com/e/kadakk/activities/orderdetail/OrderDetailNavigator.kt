package com.e.kadakk.activities.orderdetail



interface OrderDetailNavigator {
    fun showMessage(msg:  String)
    fun trackMapClick()
}