package com.e.kadakk.country


import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.e.kadakk.R
import com.e.kadakk.databinding.DialogCountryRowBinding
import java.util.*



class CountryDialogListAdapter(
    private val mContext: Context,
    internal var dialogListener: dialogItemSelectedInterface
) : RecyclerView.Adapter<CountryDialogListAdapter.ViewHolder>() {

    private var TAG = CountryDialogListAdapter::class.java.simpleName
    private lateinit var mOptArr: ArrayList<CountryListResponse.Data>


    init {

    }


    fun setList(optArr: ArrayList<CountryListResponse.Data>) {
        mOptArr = ArrayList<CountryListResponse.Data>()
        mOptArr = optArr
        notifyDataSetChanged()
    }




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        //View view = LayoutInflater.from(mContext).inflate(R.layout.dialog_country_row, parent, false);
        val binding = DataBindingUtil.inflate<DialogCountryRowBinding>(
            LayoutInflater.from(mContext),
            R.layout.dialog_country_row, parent, false
        )
        return ViewHolder(binding.getRoot())
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val model = mOptArr!!.get(position)
        holder.binding!!.tvItem.setText(model.name)
        Log.e(TAG,"model.phonecode::: "+model.phonecode)
        if(model.phonecode.isNotEmpty() && !model.phonecode.contains("+")) {
            holder.binding!!.tvCode.setText("+" + model.phonecode)
        }else{
            holder.binding!!.tvCode.setText("" + model.phonecode)
        }

//        if (isMultipleState) {
//            holder.binding!!.cbState.visibility = View.VISIBLE
//        } else {
//            holder.binding!!.cbState.visibility = View.GONE
//        }

//        if (model.isSelect) {
//            holder.binding!!.cbState.isChecked = true
//        } else {
//            holder.binding!!.cbState.isChecked = false
//        }


//        holder.binding!!.cbState.setOnClickListener(View.OnClickListener {
//            dialogListener.dialogItem(
//                position
//            )
//        })


        holder.binding!!.rowRl.setOnClickListener(View.OnClickListener {
            dialogListener.dialogItem(
                position
            )
        })
    }

    override fun getItemCount(): Int {
        return mOptArr!!.size
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val binding: DialogCountryRowBinding?

        init {
            binding = DataBindingUtil.bind<DialogCountryRowBinding>(itemView) as DialogCountryRowBinding?
        }
    }

}
