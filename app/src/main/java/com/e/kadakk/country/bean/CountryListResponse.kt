package com.e.kadakk.country

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

 public class CountryListResponse  {
    data class CountryBean(
        val message: String,
        val errorcode: Int,
        val data: List<Data>,
        val status: Int
    )
    @Parcelize
    data class Data(
        var id: String,
        var name: String,
        var phonecode: String,

        var isSelect : Boolean
    ) : Parcelable


}