@file:Suppress("DEPRECATION")

package com.e.kadakk.base

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.Settings.Secure
import android.util.DisplayMetrics
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.annotation.LayoutRes
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.lifecycle.AndroidViewModel
import com.e.kadakk.R
import com.e.kadakk.utils.AppApplication
import com.e.kadakk.utils.DialogUtil
import com.e.kadakk.utils.SessionPreferences
import java.io.File


import java.util.*

public abstract class BaseActivity<T : ViewDataBinding, V : AndroidViewModel>
    : AppCompatActivity() {

    private var mViewDataBinding: T? = null
    private var mViewModel: V? = null
    private var TAG = BaseActivity::class.java.simpleName

    public var isFullView = false;

    private var mToolBarBinding: T? = null
    /**
     * Override for set binding variable
     *
     * @return variable id
     */
    abstract fun getBindingVariable(): Int

    /**
     * @return layout resource id
     */
    @LayoutRes
    abstract fun getLayoutId(): Int


    /**
     * Override for set view model
     *
     * @return view model instance
     */
    abstract fun getViewModel(): V

    override fun onCreate(savedInstanceState: Bundle?) {
//        performDependencyInjection()
        super.onCreate(savedInstanceState)
        performDataBinding()

    }

    fun getViewDataBinding(): T {
        return mViewDataBinding!!
    }

    private fun performDataBinding() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT && isFullView) {

//            window.setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
//                WindowManager.LayoutParams.FLAG_FULLSCREEN);

            window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
            Log.e(TAG , "setFUkllView : "+isFullView)
        }

        mViewDataBinding = DataBindingUtil.setContentView<T>(this, getLayoutId())
        this.mViewModel = if (mViewModel == null) getViewModel() else mViewModel
        mViewDataBinding!!.setVariable(getBindingVariable(), mViewModel)
        mViewDataBinding!!.executePendingBindings()
    }


    public fun toolBarBinding()
    {
//        mToolBarViewModel = ViewModelProviders.of(this, ViewModelProviderFactory(application , this))
//            .get(ToolBarViewModel::class.java)
//        mBinding.toolBar.viewModel = mToolBarViewModel
    }
    /*
    * get string from edit text
    * */
    fun getEtString(et: EditText): String {
        val str = et.text.toString().trim();
        return str;
    }

    /*
    *
    * */
    fun getAppString(id: Int): String {
        return getString(id)

    }


    /*
    * define static methods , call any where as per need
    * */
    companion object {


        fun getUniqueId():String
        {
         var android_id = Secure.getString(
             AppApplication.getInstanceValue()!!.getContentResolver(),
            Secure.ANDROID_ID);

                Log.d("Android","Android ID : "+android_id);
            return android_id;

        }

        /*
         * get color from colors.xml
         * */
        fun getColor(context: Context, id: Int): Int {
            val version = Build.VERSION.SDK_INT
            return if (version >= 23) {
                ContextCompat.getColor(context, id)
            } else {
                context.resources.getColor(id)
            }
        }

        /*
* show common messages
* */
        fun showErrorMessage(
            context: Context?,
            message: String,
            isExit: Boolean
        ) {

            if (context != null) {
                DialogUtil.okCancelDialog(context,
                    context.resources.getString(R.string.app_name), message,
                    context.resources.getString(R.string.ok),
                    "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                        override fun okClick() {
                            if (isExit) {
                                SessionPreferences(context).clearSession()

//                                var intent = LoginActivity.getIntent(context);
//                                intent.flags =
//                                    Intent.FLAG_ACTIVITY_CLEAR_TOP or
//                                            Intent.FLAG_ACTIVITY_NEW_TASK or
//                                            Intent.FLAG_ACTIVITY_CLEAR_TASK
//
//                                (context as Activity). startActivity(intent)
                            }

                        }

                        override fun cancelClick() {
                        }


                    })
            }

        }



        /*
     * get color code
     * */
        fun getParseColor(colorcode: String): Int {
            return Color.parseColor(colorcode)
        }



        /*****************
         * Enable/disable views
         */
        //Set enable / disable view  - true means enable else false means disable
        fun setSelection(view: View, visibility: Boolean) {
            view.isEnabled = visibility
            for (i in 0 until (view as ViewGroup).childCount) {
                val child = view.getChildAt(i)
                if (child is ViewGroup) {
                    setSelection(child, visibility)
                } else {
                    child.isEnabled = visibility
                }
            }
        }

        /*
        * local cart count broad cast
        * */




    }


    fun gettingCardInfo(): ArrayList<String> {
        val listOfPattern = ArrayList<String>()

        val ptAmeExp = "^3[47][0-9]{5,}$"
        listOfPattern.add(ptAmeExp)


        val ptElectron = "^(4026|417500|4405|4508|4844|4913|4917)\\\\d+$"
        listOfPattern.add(ptElectron)

        val ptVisa = "^4[0-9]{6,}$"
        listOfPattern.add(ptVisa)

        val ptMasterCard = "^5[1-5][0-9]{5,}$"
        listOfPattern.add(ptMasterCard)

        //ptMaestroPayment "^(5[06-9]|6[37])[0-9]{10,17}$";
        val ptMaestroPayment =
            "^(5018|5020|5038|5612|5893|6304|6759|6761|6762|6763|0604|6390)\\\\d+$"
        listOfPattern.add(ptMaestroPayment)


        //        String ptDinClb = "^3(?:0[0-5]|[68][0-9])[0-9]{4,}$";
        val ptDinClb = "^3(?:0[0-5]|[68][0-9])[0-9]{11}$"
        listOfPattern.add(ptDinClb)

        //        String ptJcb = "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$";
        val ptJcb = "^(?:2131|1800|35[0-9]{3})[0-9]{3,}$"
        listOfPattern.add(ptJcb)

        val ptDiscover = "^6(?:011|5[0-9]{2})[0-9]{3,}$"
        listOfPattern.add(ptDiscover)


        val ptUnionPayment = "^(62|88)\\\\d+$"//"^(62[0-9]{14,17})$";
        listOfPattern.add(ptUnionPayment)

        val ptDankortPayment = "^(5019)\\\\d+$"
        listOfPattern.add(ptDankortPayment)

        val ptRupayPayment =
            "^(6061|6062|6063|6064|6065|6066|6067|6068|6069|607|608)\\\\d+$"//"^6[0-9]{15}$";
        listOfPattern.add(ptRupayPayment)


        return listOfPattern
    }

    open fun getDeviceHeight(context: Context): Int {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        return height
    }

    open fun getDeviceWidth(context: Context): Int {
        val displayMetrics = DisplayMetrics()
        (context as Activity).windowManager.defaultDisplay.getMetrics(displayMetrics)
        val height = displayMetrics.heightPixels
        val width = displayMetrics.widthPixels
        return width
    }

    open fun pxFromDp(context: Context, dp: Float): Float {
        return dp * context.resources.displayMetrics.density
    }



    fun getStatusBarHeight(): Int {
        var result = 0;
        var resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            result = getResources().getDimensionPixelSize(resourceId);
        }
        return result;
    }

    fun showNavigationBar(resources: Resources): Boolean {
        val id: Int = resources.getIdentifier("config_showNavigationBar", "bool", "android")
        return id > 0 && resources.getBoolean(id)
    }


    /* show hard update messages
     * */
    fun showHardUpdateMessage(
        context: Context?,
        message: String,
        isExit: Boolean
    ) {

        if (context != null) {
            DialogUtil.okCancelDialog(context,
                context.resources.getString(R.string.app_name), message,
                context.resources.getString(R.string.update),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                        val appPackageName: String =
                            context.getPackageName() // getPackageName() from Context or Activity object

                        try {
                            context.startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("market://details?id=$appPackageName")
                                )
                            )
                        } catch (anfe: ActivityNotFoundException) {
                            context.startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                                )
                            )
                        }
                    }

                    override fun cancelClick() {
                    }


                })
        }

    }



    /*
* show common messages
* */
    fun showErrorMessage(
        context: Context?,
        message: String,
        isExit: Boolean
    ) {

        if (context != null) {
            DialogUtil.okCancelDialog(context,
                context.resources.getString(R.string.app_name), message,
                context.resources.getString(R.string.ok),
                "", true, false, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                        if (isExit) {
                            SessionPreferences(context).clearSession()

//                            var intent = LoginActivity.getIntent(context);
//                            intent.flags =
//                                Intent.FLAG_ACTIVITY_CLEAR_TOP or
//                                        Intent.FLAG_ACTIVITY_NEW_TASK or
//                                        Intent.FLAG_ACTIVITY_CLEAR_TASK
//
//                            (context as Activity). startActivity(intent)
                        }

                    }

                    override fun cancelClick() {
                    }


                })
        }

    }







    /* show minor update messages
           * */

    fun showMinorUpdateMessage(
        context: Context?,
        message: String,
        isExit: Boolean
    ) {

        if (context != null) {
            DialogUtil.okCancelDialog(context,
                context.resources.getString(R.string.app_name), message,
                context.resources.getString(R.string.update),
                context.resources.getString(R.string.cancel), true, true, object : DialogUtil.Companion.selectOkCancelListener {
                    override fun okClick() {
                        val appPackageName: String =
                            context.getPackageName() // getPackageName() from Context or Activity object

                        try {
                            context.startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("market://details?id=$appPackageName")
                                )
                            )
                        } catch (anfe: ActivityNotFoundException) {
                            context.startActivity(
                                Intent(
                                    Intent.ACTION_VIEW,
                                    Uri.parse("https://play.google.com/store/apps/details?id=$appPackageName")
                                )
                            )
                        }
                    }

                    override fun cancelClick() {
                    }


                })
        }

    }


    fun getFileSizeKiloBytes(file: File): Double {
        var t =   file.length().toDouble()/1024
        return t;// ( "$t  kb")
    }

}