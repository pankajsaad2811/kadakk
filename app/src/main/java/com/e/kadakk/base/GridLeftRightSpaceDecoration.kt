package com.e.kadakk.base

import android.R.attr.right
import android.R.attr.left
import android.graphics.Rect
import android.view.View
import androidx.recyclerview.widget.RecyclerView


class GridLeftRightSpaceDecoration(
    private val spanCount: Int,
    private val leftspacing: Int,
    private val rightspacing: Int,
    private val bottomspacing: Int,
    private val topspacing: Int,
    private val includeEdge: Boolean
) : RecyclerView.ItemDecoration() {

    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        val position = parent.getChildAdapterPosition(view) // item position
        val column = position % spanCount // item column

        if (includeEdge) {
            outRect.left =
                leftspacing - column * leftspacing / spanCount // spacing - column * ((1f / spanCount) * spacing)
            outRect.right =
                (column + 1) * rightspacing / spanCount // (column + 1) * ((1f / spanCount) * spacing)

            if (position < spanCount) { // top edge
                outRect.top = topspacing
            }
            outRect.bottom = bottomspacing // item bottom
        } else {
            outRect.left = column * leftspacing / spanCount // column * ((1f / spanCount) * spacing)
            outRect.right =
                rightspacing - (column + 1) * rightspacing / spanCount // spacing - (column + 1) * ((1f /    spanCount) * spacing)
            if (position >= spanCount) {
                outRect.top = topspacing // item top
            }
        }
    }
}