package com.e.kadakk.utils.server

import com.google.gson.JsonElement
import com.google.gson.JsonObject
import io.reactivex.Observable
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.http.*

public interface ApiInterface {


//
//      "Authorization:eyJ1c2VyaWQiOiIzMCIsInRva2VuIjoiNWJiYWZiNmYzOGNhOCIsInVzZXJfc3RhdHVzIjoiMSIsInJlZ2lzdHJhdGlvbl90eXBlIjoiMSJ9
//device_type:ios
//device_id:111
//current_time_zone:
//language:en
//Content-Type:application/json
//version:1.0.0
//current_country:India
//lat:22.12541
//lng:22.12541"



    @GET
    public fun getRequest(
        @Url url: String, @Header("device_id") deviceId: String,
        @Header("current_time_zone") currentTimeZone: String,
        @Header("Authorization") authorization: String,
        @Header("current_country") currentCountry: String,
        @Header("lat") lat: String,
        @Header("lng") lng: String

    ): Observable<JsonObject>


    @GET
    public fun getRequestWithUserId(
        @Url url: String, @Header("device_id") deviceId: String,
        @Header("current_time_zone") currentTimeZone: String,
        @Header("Authorization") authorization: String,
        @Header("current_country") currentCountry: String,
        @Header("lat") lat: String,
        @Header("lng") lng: String,
        @Query("user_id") user_id: String

    ): Observable<JsonObject>


    @POST
    public fun postRequest(
        @Url url: String, @Body body: JsonObject,
        @Header("device_id") deviceId: String,
        @Header("current_time_zone") currentTimeZone: String,
        @Header("Authorization") authorization: String,
        @Header("current_country") currentCountry: String,
        @Header("lat") lat: String,
        @Header("lng") lng: String
    ): Observable<JsonObject>


    @Multipart
    @POST("users/profile_update")
    abstract fun updateProfileAPI(
        @PartMap partMap: HashMap<String, RequestBody>, @Part file: MultipartBody.Part,
        @Header("Authorization") authorization: String,
        @Header("device_id") deviceId: String
        , @Header("current_country") currentCountry: String,
        @Header("current_time_zone") currentTimeZone: String,
        @Header("lat") lat: String,
        @Header("lng") lng: String

    ): Observable<JsonElement>


    @Multipart
    @POST("users/profile_update")
    abstract fun updateProfileWithoutImageAPI(
        @PartMap partMap: HashMap<String, RequestBody>,
        @Header("Authorization") authorization: String,
        @Header("device_id") deviceId: String
        , @Header("current_country") currentCountry: String,
        @Header("current_time_zone") currentTimeZone: String,
        @Header("lat") lat: String,
        @Header("lng") lng: String

    ): Observable<JsonElement>

}