package com.e.kadakk.utils.viewmodelfactory

import android.app.Application
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.e.kadakk.activities.adddileveryaddress.AddDeliveryAddressViewModel
import com.e.kadakk.activities.categorydetail.CategoryDetailViewModel
import com.e.kadakk.activities.changedeliveryaddress.ChangeDeliveryAddressViewModel
import com.e.kadakk.activities.checkout.CheckOutViewModel
import com.e.kadakk.activities.choosetopics.ChooseTopicsViewModel
import com.e.kadakk.activities.dashboard.DashboardViewModel
import com.e.kadakk.activities.dashboard.cart.CardTabViewModel
import com.e.kadakk.activities.dashboard.coupon.CouponTabViewModel
import com.e.kadakk.activities.dashboard.favorite.FavoriteTabViewModel
import com.e.kadakk.activities.dashboard.profile.ProfileTabViewModel
import com.e.kadakk.activities.dashboard.profile.accountInformations.AccountInformationViewModel
import com.e.kadakk.activities.dashboard.profile.myorder.MyOrderViewModel
import com.e.kadakk.activities.dashboard.profile.myorder.fragment.ongoing.OngoingOrderViewModel
import com.e.kadakk.activities.editprofile.EditProfileViewModel
import com.e.kadakk.activities.dashboard.search.SearchTabViewModel
import com.e.kadakk.activities.forgotpassword.ForgotPasswordViewModel
import com.e.kadakk.activities.login.LoginViewModel
import com.e.kadakk.activities.notification.NotificationViewModel
import com.e.kadakk.activities.orderdetail.OrderDetailViewModel
import com.e.kadakk.activities.orderitems.OrderItemsViewModel
import com.e.kadakk.activities.otp.OTPViewModel
import com.e.kadakk.activities.resetpassword.ResetPasswordViewModel
import com.e.kadakk.activities.signup.SignUpViewModel
import com.e.kadakk.activities.splash.SplashViewModel
import com.e.kadakk.activities.trackmap.TrackMapViewModel
import com.e.kadakk.activities.tutorial.TutorialViewModel
import com.e.kadakk.activities.viewallbycategory.ViewAllOffersByCategoryViewModel
import com.e.kadakk.activities.viewalloffers.ViewAllOffersViewModel
import com.e.kadakk.toolbar.ToolBarViewModel
import com.e.kadakk.utils.server.serverResponseNavigator

class ViewModelProviderFactory(
    private val application: Application,
    serverResponse: serverResponseNavigator
)

    : ViewModelProvider.AndroidViewModelFactory(application) {


    lateinit var serverResponse: serverResponseNavigator;

    init {
        this.serverResponse = serverResponse;
    }

    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(ToolBarViewModel::class.java)) {

            return ToolBarViewModel(application) as T

        }else if (modelClass.isAssignableFrom(SplashViewModel::class.java)) {

            return SplashViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(TutorialViewModel::class.java)) {

            return TutorialViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(DashboardViewModel::class.java)) {

            return DashboardViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(ViewAllOffersByCategoryViewModel::class.java)) {

            return ViewAllOffersByCategoryViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(ViewAllOffersViewModel::class.java)) {

            return ViewAllOffersViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(SearchTabViewModel::class.java)) {

            return SearchTabViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(ProfileTabViewModel::class.java)) {

            return ProfileTabViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(ChooseTopicsViewModel::class.java)) {

            return ChooseTopicsViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(AccountInformationViewModel::class.java)) {

            return AccountInformationViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(ChangeDeliveryAddressViewModel::class.java)) {

            return ChangeDeliveryAddressViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(MyOrderViewModel::class.java)) {

            return MyOrderViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(OngoingOrderViewModel::class.java)) {

            return OngoingOrderViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(FavoriteTabViewModel::class.java)) {

            return FavoriteTabViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(LoginViewModel::class.java)) {

            return LoginViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(ForgotPasswordViewModel::class.java)) {

            return ForgotPasswordViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(OTPViewModel::class.java)) {

            return OTPViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(ResetPasswordViewModel::class.java)) {

            return ResetPasswordViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(SignUpViewModel::class.java)) {

            return SignUpViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(CouponTabViewModel::class.java)) {

            return CouponTabViewModel(
                application,
                serverResponse
            ) as T

        }else if (modelClass.isAssignableFrom(EditProfileViewModel::class.java)) {

            return EditProfileViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(CategoryDetailViewModel::class.java)) {

            return CategoryDetailViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(ChangeDeliveryAddressViewModel::class.java)) {

            return ChangeDeliveryAddressViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(AddDeliveryAddressViewModel::class.java)) {

            return AddDeliveryAddressViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(CheckOutViewModel::class.java)) {

            return CheckOutViewModel(application, serverResponse) as T

        }else if (modelClass.isAssignableFrom(CardTabViewModel::class.java)) {

            return CardTabViewModel(application, serverResponse) as T
        }else if (modelClass.isAssignableFrom(OrderDetailViewModel::class.java)) {

            return OrderDetailViewModel(application, serverResponse) as T
        }else if (modelClass.isAssignableFrom(TrackMapViewModel::class.java)) {

            return TrackMapViewModel(application, serverResponse) as T
        }else if (modelClass.isAssignableFrom(OrderItemsViewModel::class.java)) {

            return OrderItemsViewModel(application, serverResponse) as T
        }else if (modelClass.isAssignableFrom(NotificationViewModel::class.java)) {

            return NotificationViewModel(application, serverResponse) as T
        }

        throw IllegalArgumentException("Unknown ViewModel class: " + modelClass.name)
    }

}
