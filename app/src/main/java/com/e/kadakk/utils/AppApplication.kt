package com.e.kadakk.utils

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.multidex.MultiDex
import com.google.firebase.FirebaseApp

class AppApplication : Application() {


    var mContext: Context? = null;

    override fun onCreate() {
        super.onCreate()
        mContext = this;
        instance = this
        FirebaseApp.initializeApp(this);


    }
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this);
    }

    companion object {

        var instance: AppApplication? = null
        @Synchronized
        fun getInstanceValue(): AppApplication? {
            if (instance == null) {
                instance =
                    AppApplication();
            }
            Log.e("" ,"(instance == null) :::::  "+((instance == null)));
            return instance;
        }
    }
}
