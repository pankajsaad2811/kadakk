package com.e.kadakk.utils


import com.google.gson.annotations.SerializedName

data class LoginModel(
    @SerializedName("data")
    val `data`: Data,
    @SerializedName("errorcode")
    val errorcode: Int,
    @SerializedName("message")
    val message: String,
    @SerializedName("status")
    val status: Int,
    @SerializedName("version")
    val version: Version
) {
    data class Data(
        @SerializedName("login")
        val login: Login
    ) {
        data class Login(
            @SerializedName("Authorization")
            val authorization: String,
            @SerializedName("firstname")
            val firstname: String,
            @SerializedName("lastname")
            val lastname: String,
            @SerializedName("user_email")
            val userEmail: String,
            @SerializedName("user_id")
            val userId: String,
            @SerializedName("user_pic")
            val userPic: String,
            @SerializedName("user_role")
            val userRole: String,
            @SerializedName("user_status")
            val userStatus: Int,
            @SerializedName("otp")
            val otp: String,
            @SerializedName("selected_category")
            val selectedCategory: Int
        )
    }

    data class Version(
        @SerializedName("current_version")
        val currentVersion: String,
        @SerializedName("status")
        val status: Int,
        @SerializedName("versioncode")
        val versioncode: Int,
        @SerializedName("versionmessage")
        val versionmessage: String
    )
}