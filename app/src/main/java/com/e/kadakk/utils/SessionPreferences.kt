package com.e.kadakk.utils

import android.content.Context
import android.content.SharedPreferences
import android.text.TextUtils
import com.google.gson.Gson

class SessionPreferences(context: Context) {
    val TAG = SessionPreferences::class.java.simpleName
    val loginPrefKey: String = "loginPrefKey";
    val loginPrefValue: String = "loginPrefValue";
    val locationPrefValue: String = "locationPrefValue";


    val cartCountPrefKey: String = "cartCountPrefKey";
    val videoCountPrefKey: String = "videoCountPrefKey";
    val cartCountPrefValue: String = "cartCountPrefValue";
    val firebaseRegId: String = "firebaseRegId";
    val tutorial: String = "tutorial";
    val unReadCount: String = "unReadCount";


    var mLoginData: LoginModel? = null;
    var mContext: Context = context


    init {
        getSessionPref()
    }

    fun getUserSessionKey(): SharedPreferences {
        var loginSharedPref = mContext!!.getSharedPreferences(loginPrefKey, Context.MODE_PRIVATE);
        return loginSharedPref;
    }




    fun saveSessionPref(userData: LoginModel) {
        var loginEditor = getUserSessionKey().edit();
        var gson = Gson();
        var str = gson.toJson(userData)
        loginEditor.putString(loginPrefValue, str)
        loginEditor.commit()
    }


    fun getSessionPref(): LoginModel? {
        var gson = Gson();
        var json = getUserSessionKey().getString(loginPrefValue, "")
//        Log.e(TAG , "jsonjson::::  "+json);
        if (json != null && !TextUtils.isEmpty(json)) {
            mLoginData = gson.fromJson(json, LoginModel::class.java)
            return mLoginData as LoginModel?
        } else {
            return null;
        }

    }

    /*
    * clear all local value on logout
    * */
    fun clearSession() {
        var loginEditor = getUserSessionKey().edit();
        loginEditor.clear()
        loginEditor.commit()
    }



    /****** firebase RegId******/

    fun getFirebaseRegIdPref(): SharedPreferences {
        var cartSharedPref =
            mContext!!.getSharedPreferences(firebaseRegId, Context.MODE_PRIVATE);
        return cartSharedPref;
    }


    fun saveFirebaseRegIdPrefValue(count: String) {
        var editor = getFirebaseRegIdPref().edit();
        editor.putString(firebaseRegId, count)
        editor.commit()
    }


    fun getFirebaseRegIdPrefValue(): String {
        var count = getFirebaseRegIdPref().getString(firebaseRegId, "")
        return count!!;
    }

    /*************/


  /****** Tutorial ******/

    fun getTutorialPref(): SharedPreferences {
        var cartSharedPref =
            mContext!!.getSharedPreferences(tutorial, Context.MODE_PRIVATE);
        return cartSharedPref;
    }


    fun saveTutorialPrefValue(count: Boolean) {
        var editor = getTutorialPref().edit();
        editor.putBoolean(tutorial, count)
        editor.commit()
    }


    fun getTutorialValue(): Boolean {
        var count = getTutorialPref().getBoolean(tutorial, false)
        return count!!;
    }

    /*************/






}