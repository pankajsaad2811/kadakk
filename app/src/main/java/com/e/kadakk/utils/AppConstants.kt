package com.e.kadakk.utils

import androidx.databinding.library.BuildConfig

public class AppConstants {
    companion object {
        val KEY_AUTHORIZATION = "key_Authorization"
        val KEY_IS_WALKTHROUGH_COMPLETED = "key_get_started_completed"
        val KEY_IS_LOGIN = "key_IS_LOGIN"
        var DEVICE_TYPE = "android";
        var DEVICE_NOTIFICATION_ID = "12345";
        var DEVICE_TOKEN = "";
        var PARENT_ID = ""
        var APP_VERSION = BuildConfig.VERSION_NAME.toString()

        //these is for amazon aws uplaoding
        //var BUCKET_URl = "https://cf-templates-1omg15nh115tm-us-east-1.s3.amazonaws.com/wlwell/";
        val MY_BUCKET = "lmsparentsimages/"
        val STUDENT_PROFILE_PICTURES = "parent_profile_pictures"
        val BUCKET_PROFILE_PICTURES = MY_BUCKET + STUDENT_PROFILE_PICTURES
        const val BUCKET_URl_WHEN_SERVERSENT =
            "https://lmsparentsimages.s3.us-east-2.amazonaws.com/"
        val STUDENT_FEEDBACK_ATTACHMENT = "parent_feedback_attachment"

        val BUCKET_STUDENT_FEEDBACK = MY_BUCKET + STUDENT_FEEDBACK_ATTACHMENT


        //************************
        // var BASE_URL = "https://lms.consagous.co.in/api/"
        // var BASE_URL = "http://knowledgerevise.com/Offer/webservices/"
        var BASE_URL = "http://kadakkoffers.com/webservices/"
        var COUNTRY_LIST_API = "student/get_countries_list"
        var LOGIN_API = BASE_URL + "users/login"
        var REGISTER_API = BASE_URL + "users/register"
        var GET_PROFILE_API = BASE_URL + "users/get_profile"
        var UPDATE_PROFILE_API = "users/profile_update"
        var FORGOT_PASSWORD_API = BASE_URL + "users/forgotpassword"
        var VERIFY_OTP_API = BASE_URL + "users/signup_otp"
        var RESENDOTP_API = BASE_URL + "users/resend_otp";
        var RESET_PASSWORD_API = "parents/password_reset"
        var UPDATE_PASSWORD_API = "parents/update_password"
        var GET_ADDRESS_LIST_API = "Offer/list_address"
        var ADD_ADDRESS_API = "Offer/add_address"
        var DELETE_ADDRESS_API = "Offer/delete_address"
        var UPDATE_PROFILE_IMAGE_API = "parents/profile_image"
        var UPDATE_REQUEST_API = "parents/update_request"
        var UPDATE_NOTIFICATION_SETTING_API = "parents/notification_setting"
        var STATIC_PAGES_API = "parents/static_page"
        var CHANGE_NUMBER_API = "parents/change_number"
        var GET_STUDENT_LIST_API = "parents/student_list"

        // var UPDATE_PROFILE_API = "parents/update_profile"
        var SUBMIT_FEEDBACK_API = "parents/update_feedback"
        var FEEDBACK_REASON_LIST_API = "parents/feedback_list"
        var GET_NAME_API = "parents/get_name"
        var REMOVE_STUDENT_API = "parents/remove_student"
        var STUDENT_FEEDBACK_API = "parents/student_feedback"
        var RESETPASSWORD_API = BASE_URL + "users/reset_password";
        var OFFER_CHOOSE_CATEGORY_API = BASE_URL + "Offer/choose_category";
        var OFFER_UPDATE_CHOOSE_CATEGORY_API = BASE_URL + "users/update_users_category";
        var OFFER_HOME_OFFERS_API = BASE_URL + "Offer/home_offers";
        var SEARCH_OFFER_API = BASE_URL + "Offer/search_offer";
        var VIEW_ALL_OFFER_API = BASE_URL + "Offer/view_all_offer";
        var ADD_WISH_LIST_API = BASE_URL + "Offer/add_wishlist";
        var OFFER_BY_CATEGORY_API = BASE_URL + "Offer/offer_by_category";
        var MY_WISH_LIST_API = BASE_URL + "Offer/mywishlist";
        var OFFER_DETAILS_API = BASE_URL + "Offer/offer_details";
        var CARD_ITEM_LIST_API = BASE_URL + "offer/list_card";
        var ADD_TO_CARD_API = BASE_URL + "Offer/add_to_card"
        var DELETE_TO_CARD_API = BASE_URL + "Offer/delete_card"
        var UPDATE_TO_CARD_API = BASE_URL + "offer/update_card"
        var CONFIRM_ORDER_API = BASE_URL + "offer/confirm_order"
        var ORDER_LIST_API = BASE_URL + "offer/my_order"
        var ORDER_DETAIL_API = BASE_URL + "offer/order_details"
        var ACCOUNT_SETTING_API = BASE_URL + "offer/account_setting"

        //************************
    }
}