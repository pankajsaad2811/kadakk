package com.e.kadakk.utils

import android.util.Log
import android.util.Patterns
import java.util.regex.Pattern

public class Validator
{
    companion object {
        public fun isEmailValid(email: String): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

       public fun isUrlValid(email: String): Boolean {
            return Patterns.WEB_URL.matcher(email).matches();
        }

        public fun isMobileNumberValid(mobile: String): Boolean {

            if(mobile.length>9){
                return true
            }else {
                return false
            }

        }


        public fun isValidMobile(phone: String): Boolean {

            Log.e("dsfdsf","TTTTT: "+(Pattern.matches("[0-9]+", phone)))
            return (Pattern.matches("[0-9]+", phone))
        }

        public fun isOtpValid(mobile: String): Boolean {

            if(mobile.length>3){
                return true
            }else {
                return false
            }

        }

        fun isValidPassword(password: String): Boolean {
            return password.matches("(?=.*[A-Z])(?=.*[@#$%])(?=.*[0-9])(?=.*[a-z]).{8,20}".toRegex())

        }
    }

}