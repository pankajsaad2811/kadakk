package com.e.kadakk.utils;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;


import com.amazonaws.util.IOUtils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

//import com.google.android.gms.common.util.IOUtils;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class FileUtils {

    public static String TAG = FileUtils.class.getSimpleName();
    public static int MAX_VIDEO_SIZE = 50;

    public static String FOLDER_PATH = Environment.getExternalStorageDirectory().toString();
    public static String FOLDER_NAME = "WLWell";
    public static String IMG_FOLDER_NAME = "image";
    public static String DOWNLOAD_FOLDER_NAME = "download";
    public static String SUB_FOLDER_NAME = "temp";

    public static File createImageFile() {
        String imageFileName = "IMG_" + System.currentTimeMillis();
        File dir = new File(
                FOLDER_PATH + File.separator + FOLDER_NAME
                        + File.separator + IMG_FOLDER_NAME
        );
        Log.e(TAG, "dir.exists():::::::  " + dir.exists());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File image = null;
        try {
            image = File.createTempFile(imageFileName, ".jpeg", dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    public static File createVideoFile() {
        String imageFileName = "Vid_" + System.currentTimeMillis();
        File dir = new File(Environment.getExternalStorageDirectory()
                + File.separator + APP_DIR

//                + File.separator + SUB_DIR
        );
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File image = null;
        try {
            image = File.createTempFile(imageFileName, ".mp4", dir);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return image;
    }

    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {


            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                Log.e(TAG, "uri.toString :::::  " + uri.toString() + "  id: " + id);

                if (id.startsWith("raw:")) {
                    return id.replaceFirst("raw:", "");
                }

                //added some code
                String[] contentUriPrefixesToTry = new String[]{
                        "content://downloads/public_downloads",
                        "content://downloads/my_downloads"
                };

                for (String contentUriPrefix : contentUriPrefixesToTry) {
                    Uri contentUri = ContentUris.withAppendedId(Uri.parse(contentUriPrefix), Long.valueOf(id));
                    try {
                        String path = getDataColumn(context, contentUri, null, null);
                        if (path != null) {
                            return path;
                        }
                    } catch (Exception e) {
                    }
                }
                //added some code till that

                // path could not be retrieved using ContentResolver, therefore copy file to accessible cache using streams
                String fileName = getFileName(context, uri);
                File cacheDir = getDocumentCacheDir(context);
                File file = generateFileName(fileName, cacheDir);
                String destinationPath = null;
                if (file != null) {
                    destinationPath = file.getAbsolutePath();
                    saveFileFromUri(context, uri, destinationPath);
                }

                return destinationPath;

                //                final Uri contentUri = ContentUris.withAppendedId(
                //                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                //                return getDataColumn(context, contentUri, null, null);


            }


            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            } else if ("content".equalsIgnoreCase(uri.getScheme())) {
                Log.e(TAG, "conteeetnttt2222: " + uri.getScheme());
                Log.e(TAG, "isGooglePhotosUri(uri): " + isGooglePhotosUri(uri));


                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();

                //                return getDataColumn(context, uri, null, null);

                Cursor cursor = null;
                final String column = "_data";
                final String[] projection = {
                        column
                };

                try {
                    cursor = context.getContentResolver().query(uri, projection, null, null,
                            null);

                    Log.e(TAG, "check_dataIndex11::::::   " + (cursor.getColumnIndex(column) != -1) +
                            "  uri:  " + uri.toString());

                    //                    if (cursor != null && cursor.moveToFirst() &&  !uri.toString().startsWith("content")) {
                    //                        if(cursor.getColumnIndex(column)!=-1){
                    //                            final int index = cursor.getColumnIndexOrThrow(column);
                    //                            return cursor.getString(index);
                    //                        }else{
                    return getFilePathFromURI(context, uri);
                    //                        }
                    //                    }
                } finally {
                    if (cursor != null)
                        cursor.close();
                }
            }

        }//end of kitkat check

/////

        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            Log.e(TAG, "conteeetnttt11111: " + uri.getScheme());
            //   Log.e(TAG, "isGooglePhotosUri(uri): " + isGooglePhotosUri(uri));
            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        } else if (isNewGooglePhotosUri(uri)) {
            String pathUri = uri.getPath();
            String newUri = pathUri.substring(pathUri.indexOf("content"), pathUri.lastIndexOf("/ACTUAL"));
            return getDataColumnNew(context, Uri.parse(newUri), null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            Log.e(TAG, "fileeeee: ");

            return uri.getPath();
        }

        return null;
    }

      public static boolean isNewGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.contentprovider".equals(uri.getAuthority());
    }
    public static boolean isNewGoogleDocUri(Uri uri) {
        return "com.google.android.apps.docs.contentprovider".equals(uri.getAuthority());
    }

    public static String getDataColumnNew(Context context, Uri uri, String selection, String[] selectionArgs) {
        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs, null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    private static String getDataColumn(Context context, Uri uri, String selection,
                                        String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);

            Log.e(TAG, "check_dataIndex::::::   " + (cursor.getColumnIndex(column) != -1) +
                    "  uri:  " + uri.toString());

            if (cursor != null && cursor.moveToFirst()) {
                if (cursor.getColumnIndex(column) != -1) {
                    final int index = cursor.getColumnIndexOrThrow(column);
                    return cursor.getString(index);
                } else {
                    return getFilePathFromURI(context, uri);
                }
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority())
                //  ||"com.google.android.apps.photos.contentprovider".equals(uri.getAuthority())
                ;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Drive.
     */
    public static boolean isGoogleDriveUri(Uri uri) {
        return "com.google.android.apps.docs.storage".equals(uri.getAuthority()) ||
                "com.google.android.apps.docs.storage.legacy".equals(uri.getAuthority());
    }

    static String sourceFilePath = (FOLDER_PATH + File.separator +
            FOLDER_NAME + File.separator + IMG_FOLDER_NAME);

    public static String getFilePathFromURI(Context context, Uri contentUri) {
        //copy file and send new file path
        String fileName = getFileName(contentUri);
        if (!TextUtils.isEmpty(fileName)) {
//            File copyFile = new File(FOLDER_PATH + File.separator +
//                    FOLDER_NAME + File.separator +
//                    fileName);


            File copyFile = new File(FOLDER_PATH + File.separator +
                    FOLDER_NAME + File.separator + IMG_FOLDER_NAME + File.separator +
                    fileName);

            if (copyFile.exists()) {
                return copyFile.getAbsolutePath();
            } else {

               /* if (!new File(sourceFilePath).exists()) {
                    new File(FOLDER_PATH + File.separator +
                            FOLDER_NAME).mkdirs();
                }*/

                if (!new File(sourceFilePath).exists()) {
                    new File(sourceFilePath).mkdirs();
                }

                copy(context, contentUri, copyFile);
                Log.e(TAG, "copyFile.getAbsolutePath(): " + copyFile.getAbsolutePath());
                return copyFile.getAbsolutePath();
            }
        }
        return null;
    }

    public static String getFileName(Uri uri) {
        if (uri == null) return null;
        String fileName = null;
        String path = uri.getPath();
        int cut = path.lastIndexOf('/');
        if (cut != -1) {
            fileName = path.substring(cut + 1);
        }
        return fileName;
    }

    public static void copy(Context context, Uri srcUri, File dstFile) {
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(srcUri);
            if (inputStream == null) return;
            OutputStream outputStream = new FileOutputStream(dstFile);
//            IOUtils.copyStream(inputStream, outputStream);
            IOUtils.copy(inputStream, outputStream);
            inputStream.close();
            outputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    private static FileUtils instance = null;

    private static Context mContext;

    public static final String APP_DIR = FOLDER_NAME
            + "/" + SUB_FOLDER_NAME;

//    private static final String SUB_DIR =   ProfileBioSetupActivity.SUB_FOLDER_NAME
    ;
    private static final String TEMP_DIR = FOLDER_NAME + "/" + SUB_FOLDER_NAME
            + "/.TEMP";

    public static FileUtils getInstance(Context context) {
        if (instance == null) {
            synchronized (FileUtils.class) {
                if (instance == null) {
                    mContext = context.getApplicationContext();
                    instance = new FileUtils();
                }
            }
        }
        return instance;
    }

    /**
     * @param bm Bitmap to save
     * @return path of the file
     */
    public static String saveBitmapToLocal(Context context, Bitmap bm, boolean temp) {
        String path;
        try {
            File file;
            if (temp) {
                file = FileUtils.getInstance(context).createTempFile("IMG_", ".jpeg");
            } else {
                file = FileUtils.getInstance(context).createFile("IMG_", ".jpeg");
            }
            FileOutputStream fos = new FileOutputStream(file);
            bm.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
            path = file.getAbsolutePath();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        return path;
    }

    /**
     * @param prefix    prefix for file
     * @param extension file extension
     * @return newly created file
     * @throws IOException when not able to create file
     */
    public File createTempFile(String prefix, String extension) throws IOException {
        File file = new File(getAppDirPath() + ".TEMP/" + prefix + System.currentTimeMillis() + extension);
        return file.createNewFile() ? file : null;
    }


    /**
     * @param prefix    prefix for file
     * @param extension file extension
     * @return newly created file
     * @throws IOException when not able to create file
     */
    public File createFile(String prefix, String extension) throws IOException {
        File file = new File(getSDDirPath(), prefix + System.currentTimeMillis() + extension);
        return file.createNewFile() ? file : null;
    }

    /**
     * Get the current application content directory, and return null if the external storage is not available
     *
     * @return app directory path
     */
    public String getAppDirPath() {
        String path = null;
        if (getLocalPath() != null) {
            path = getLocalPath() + APP_DIR + "/";
        }
        return path;
    }

    public String getSDDirPath() {
        String path = null;
        if (getLocalPath() != null) {
            path = Environment.getExternalStorageDirectory() + "/" + APP_DIR + "/";
        }
        return path;
    }

    /**
     * Get the current app directory
     *
     * @return local path
     */
    private static String getLocalPath() {
        String sdPath;
        sdPath = mContext.getExternalFilesDir(null).getAbsolutePath() + "/";
        //sdPath = mContext.getFilesDir().getAbsolutePath() + "/";

        return sdPath;
    }

    /**
     * Check that the sd card is ready and readable
     *
     * @return if sd media is writable
     */
    public boolean isSDCanWrite() {
        String status = Environment.getExternalStorageState();
        return status.equals(Environment.MEDIA_MOUNTED)
                && Environment.getExternalStorageDirectory().canWrite()
                && Environment.getExternalStorageDirectory().canRead();
    }

    private FileUtils() {
        if (isSDCanWrite()) {
            createLocalDir(APP_DIR);
            createLocalDir(TEMP_DIR);
            createSDDir(APP_DIR);
        }
    }

    /**
     * Create a directory on the SD card root directory
     *
     * @param dirName to create
     */
    public File createLocalDir(String dirName) {
        File dir = new File(getLocalPath() + dirName);
        dir.mkdirs();
        return dir;
    }

    /**
     * Create a directory on the SD card root directory
     *
     * @param dirName to create
     */
    public File createSDDir(String dirName) {
        File dir = new File(Environment.getExternalStorageDirectory() + "/" + dirName);
        dir.mkdirs();
        return dir;
    }

    public static String copyToPublicDir(String srcFile) {
        File publicFile = createImageFile();
        copy(new File(srcFile), publicFile);
        return publicFile.getAbsolutePath();
    }

    private static void copy(File src, File dst) {
        InputStream in = null;
        OutputStream out = null;
        try {
            in = new FileInputStream(src);
            out = new FileOutputStream(dst);
            // Transfer bytes from in to out
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (out != null) try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (in != null) try {
                in.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


    /*
     * get uri from document file
     * */
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static String getDocumentUri(Context context, Intent data) {

        String wholeID = "";
        Uri selectedImage = data.getData();
        if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            wholeID = getPath(context, selectedImage);
        } else {
            wholeID = DocumentsContract.getDocumentId(selectedImage);

        }

        // Split at colon, use second item in the array
        Log.i("debug", "uri google drive " + wholeID);
        String id = wholeID.split(":")[1];

        String[] column = {MediaStore.Images.Media.DATA};

        // where id is equal to
        String sel = MediaStore.Images.Media._ID + "=?";

        Cursor cursor = context.getContentResolver().
                query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                        column, sel, new String[]{id}, null);


        int columnIndex = cursor.getColumnIndex(column[0]);
        String filePath = "";
        if (cursor.moveToFirst()) {
            filePath = cursor.getString(columnIndex);
        }
        cursor.close();
        return filePath;
    }


    /*
     * get file from google storage
     * */
    public static String getDriveFilePath(Uri uri, Context context) {
        Uri returnUri = uri;
        Cursor returnCursor = context.getContentResolver().query
                (returnUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));


//overwrite on file on existing created file therefore get path


        File file = new File(
                FOLDER_PATH + File.separator + FOLDER_NAME
                , name);


        //if folder not exists then create folder
        if (!new File(
                FOLDER_PATH + File.separator + FOLDER_NAME).exists()) {
            new File(
                    FOLDER_PATH + File.separator + FOLDER_NAME).mkdirs();
        }

//                ImagePicker.mCurrentVideoPath);
        //context.getCacheDir()
//                Environment.getExternalStorageDirectory()
//                , name);
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1 * 1024 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }

                Log.e("File Size", "Size " + file.length());

            inputStream.close();
            outputStream.close();
                Log.e("File Path", "Path " + file.getPath());
                Log.e("File Size", "Size " + file.length());

        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }


    /*
     * get file from google storage
     * */
    public static String getDriveImgFilePath(Uri uri, Context context) {
        Uri returnUri = uri;
        Cursor returnCursor = context.getContentResolver().query
                (returnUri, null, null, null, null);
        /*
         * Get the column indexes of the data in the Cursor,
         *     * move to the first row in the Cursor, get the data,
         *     * and display it.
         * */
        int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
        int sizeIndex = returnCursor.getColumnIndex(OpenableColumns.SIZE);
        returnCursor.moveToFirst();
        String name = (returnCursor.getString(nameIndex));
        String size = (Long.toString(returnCursor.getLong(sizeIndex)));


//overwrite on file on existing created file therefore get path
//        File file = new File(
//                FOLDER_PATH + File.separator + FOLDER_NAME
//                , name);

        File file = new File(
                sourceFilePath
                , name);
        try {
            InputStream inputStream = context.getContentResolver().openInputStream(uri);
            FileOutputStream outputStream = new FileOutputStream(file);
            int read = 0;
            int maxBufferSize = 1 * 1024 * 1024;
            int bytesAvailable = inputStream.available();

            //int bufferSize = 1024;
            int bufferSize = Math.min(bytesAvailable, maxBufferSize);

            final byte[] buffers = new byte[bufferSize];
            while ((read = inputStream.read(buffers)) != -1) {
                outputStream.write(buffers, 0, read);
            }

                Log.e("File Size", "Size " + file.length());

            inputStream.close();
            outputStream.close();

                Log.e("File Path", "Path " + file.getPath());
                Log.e("File Size", "Size " + file.length());

        } catch (Exception e) {
            Log.e("Exception", e.getMessage());
        }
        return file.getPath();
    }


    public static String getName(String filename) {
        if (filename == null) {
            return null;
        }
        int index = filename.lastIndexOf('/');
        return filename.substring(index + 1);
    }

    public static final String DOCUMENTS_DIR = "documents";

    public static String getFileName(@NonNull Context context, Uri uri) {
        String mimeType = context.getContentResolver().getType(uri);
        String filename = null;

        if (mimeType == null && context != null) {
            String path = getPath(context, uri);
            if (path == null) {
                filename = getName(uri.toString());
            } else {
                File file = new File(path);
                filename = file.getName();
            }
        } else {
            Cursor returnCursor = context.getContentResolver().query(uri, null, null, null, null);
            if (returnCursor != null) {
                int nameIndex = returnCursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                returnCursor.moveToFirst();
                filename = returnCursor.getString(nameIndex);
                returnCursor.close();
            }
        }

        return filename;
    }


    public static File getDocumentCacheDir(@NonNull Context context) {
        File dir = new File(context.getCacheDir(), DOCUMENTS_DIR);
        if (!dir.exists()) {
            dir.mkdirs();
        }
//        logDir(context.getCacheDir());
//        logDir(dir);

        return dir;
    }


    @Nullable
    public static File generateFileName(@Nullable String name, File directory) {
        if (name == null) {
            return null;
        }

        File file = new File(directory, name);

        if (file.exists()) {
            String fileName = name;
            String extension = "";
            int dotIndex = name.lastIndexOf('.');
            if (dotIndex > 0) {
                fileName = name.substring(0, dotIndex);
                extension = name.substring(dotIndex);
            }

            int index = 0;

            while (file.exists()) {
                index++;
                name = fileName + '(' + index + ')' + extension;
                file = new File(directory, name);
            }
        }

        try {
            if (!file.createNewFile()) {
                return null;
            }
        } catch (IOException e) {
            //Log.w(TAG, e);
            return null;
        }

        //logDir(directory);

        return file;
    }

    //these method is used to trim data properly
    public static CharSequence trimTrailingWhitespace(CharSequence source) {

        if (source == null)
            return "";

        int i = source.length();

        // loop back to the first non-whitespace character
        while (--i >= 0 && Character.isWhitespace(source.charAt(i))) {
        }

        return source.subSequence(0, i + 1);
    }


    private static void saveFileFromUri(Context context, Uri uri, String destinationPath) {
        InputStream is = null;
        BufferedOutputStream bos = null;
        try {
            is = context.getContentResolver().openInputStream(uri);
            bos = new BufferedOutputStream(new FileOutputStream(destinationPath, false));
            byte[] buf = new byte[1024];
            is.read(buf);
            do {
                bos.write(buf);
            } while (is.read(buf) != -1);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (is != null) is.close();
                if (bos != null) bos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURI_API19(final Context context, final Uri uri) {
        try {
            final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

            // DocumentProvider
            if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
                // ExternalStorageProvider
                if (isExternalStorageDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    // This is for checking Main Memory
                    if ("primary".equalsIgnoreCase(type)) {
                        if (split.length > 1) {
                            return Environment.getExternalStorageDirectory() + "/" + split[1];
                        } else {
                            return Environment.getExternalStorageDirectory() + "/";
                        }
                        // This is for checking SD Card
                    } else {
                        return "storage" + "/" + docId.replace(":", "/");
                    }

                }
                // DownloadsProvider
                else if (isDownloadsDocument(uri)) {
                    String fileName = getFilePathMain(context, uri);
                    if (fileName != null) {
                        return Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName;
                    }

                    final String id = DocumentsContract.getDocumentId(uri);
                    final Uri contentUri = ContentUris.withAppendedId(
                            Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
                    return getDataColumn(context, contentUri, null, null);
                }
                // MediaProvider
                else if (isMediaDocument(uri)) {
                    final String docId = DocumentsContract.getDocumentId(uri);
                    final String[] split = docId.split(":");
                    final String type = split[0];

                    Uri contentUri = null;
                    if ("image".equals(type)) {
                        contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                    } else if ("video".equals(type)) {
                        contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                    } else if ("audio".equals(type)) {
                        contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                    }

                    final String selection = "_id=?";
                    final String[] selectionArgs = new String[]{
                            split[1]
                    };

                    return getDataColumn(context, contentUri, selection, selectionArgs);
                } else if (isGoogleDriveUri(uri)) {
                    return uri.getLastPathSegment();

                }
            }
            // MediaStore (and general)
            else if ("content".equalsIgnoreCase(uri.getScheme())) {

                // Return the remote address
                if (isGooglePhotosUri(uri))
                    return uri.getLastPathSegment();

                return getDataColumn(context, uri, null, null);
            }
            // File
            else if ("file".equalsIgnoreCase(uri.getScheme())) {
                return uri.getPath();
            }

            return "";
        } catch (Exception e){
            Cursor cursor = null;
            final String[] projection = {
                    MediaStore.MediaColumns.DISPLAY_NAME
            };

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null,
                        null);
                if (cursor != null && cursor.moveToFirst()) {
                    final int index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
                    return cursor.getString(index);
                }
            } finally {
                if (cursor != null)
                    cursor.close();
            }
            return "";
        }
    }

    @SuppressLint("NewApi")
    public static String getRealPathFromURIDoc_API19(final Context context, final Uri uri)
    {
        return uri.getLastPathSegment();
    }

    public static String getFilePathMain(Context context, Uri uri) {

        Cursor cursor = null;
        final String[] projection = {
                MediaStore.MediaColumns.DISPLAY_NAME
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, null, null,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DISPLAY_NAME);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


}
