package com.e.kadakk.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.SystemClock
import android.text.InputFilter
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.ViewGroup
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.annotation.ColorRes
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility
import com.amazonaws.regions.Region
import com.amazonaws.regions.Regions
import com.amazonaws.services.s3.AmazonS3Client
import com.e.kadakk.R

class AppUtil {

    companion object {
        private var toast: Toast? = null

        //these method is to start new intent
        fun startIntent(
            bundle: Bundle?,
            mSourceActivity: Activity,
            destinationClass: Class<*>?
        ) {
            val mainIntent = Intent(mSourceActivity, destinationClass)
            if (bundle != null) mainIntent.putExtras(bundle)
            mSourceActivity.startActivity(mainIntent)
            mSourceActivity.overridePendingTransition(R.anim.slide_in_right_anim, R.anim.slide_out_left_anim)
        }

        //these method is to hide keyboard
        fun hideSoftKeyBoard(context: Context, view: View) {
            try {
                val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm?.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
            } catch (e: Exception) {
                // TODO: handle exception
                e.printStackTrace()
            }
        }

        fun hideKeyboard(activity: Activity) {
            try {
                val view = activity.currentFocus
                if (view != null) {
                    val imm =
                        activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(view.windowToken, 0)
                }
            } catch (ex: java.lang.Exception) {
                ex.printStackTrace()
            }
        }


        //these method is to get Color
        fun getColor(@ColorRes colorId: Int): Int {
            return AppApplication.getInstanceValue()!!.resources.getColor(colorId)
        }
 //these method is to get Color
        fun showLogs(tag: String, message: String) {
           Log.e(tag, message)
        }


        //these method is to show toast
        fun showToast(
            context: Context?,
            message: String?
        ) {
            if (toast != null && toast!!.getView()!!.isShown()) {
                toast!!.cancel()
            }
            toast = Toast.makeText(context, message, Toast.LENGTH_SHORT)
            toast!!.setGravity(Gravity.CENTER, 0, 0)
            toast!!.show()
        }

        //these method is to check is email valid or not
        public fun isEmailValid(email: String): Boolean {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches()
        }

        //these method is to set status bar color
        fun setStatusBarColor(activity: Activity, Color: Int) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                val window = activity.window
                window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
                window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
                window.statusBarColor = Color

            } else {
            }
        }


        private var sTransferUtility: TransferUtility? = null
        private var awsCredentials: AWSCredentials? = null
        var mLastClickTime=0L

        //these is amazon file and code handling
//        fun getS3Client(context: Context): AmazonS3Client? {
//            var sS3Client: AmazonS3Client? = null
//            if (sS3Client == null) {
//                awsCredentials = object : AWSCredentials {
//                    override fun getAWSAccessKeyId(): String {
//                        return context.resources.getString(R.string.AWSAccessKeyId)
//                    }
//
//                    override fun getAWSSecretKey(): String {
//                        return context.resources.getString(R.string.AWSSecretKey)
//                    }
//                }
//                sS3Client =
//                    AmazonS3Client(awsCredentials)
//
//                sS3Client.setRegion(
//                    Region.getRegion(
//                        Regions.US_EAST_2
//                    )
//                )
//            }
//            return sS3Client
//        }


//        fun getTransferUtility(context: Context): TransferUtility? {
//            if (sTransferUtility == null) {
//                sTransferUtility = TransferUtility(
//                    getS3Client(context.applicationContext),
//                    context.applicationContext
//                )
//            }
//            return sTransferUtility
//        }

        /*
            * for managing Double clicks*/
        fun isOpenRecently():Boolean{
            if (SystemClock.elapsedRealtime() - mLastClickTime < 1000){
                return true
            }
            mLastClickTime = SystemClock.elapsedRealtime()
            return false
        }



        fun disableEmojiInView(editText: EditText) {
            val emojiFilter = InputFilter { source, start, end, dest, dstart, dend ->
                for (index in start until end) {
                    val type = Character.getType(source[index])

                    if (type == Character.SURROGATE.toInt() || type == Character.NON_SPACING_MARK.toInt()
                        || type==Character.OTHER_SYMBOL.toInt()) {
                        return@InputFilter ""
                    }
                }
                return@InputFilter null
            }
            editText.filters = arrayOf(emojiFilter)
        }


        fun setupUI(view: View, activity: Activity?) {
            if (view !is EditText) {
                view.setOnTouchListener { v, event ->
                    if (activity != null && v != null) {
                        AppUtil.hideKeyboard(activity)
                    }
                    false
                }
            }

            // If a layout container, iterate over children and seed recursion.
            if (view is ViewGroup) {
                for (i in 0 until view.childCount) {
                    val innerView = view.getChildAt(i)
                    activity?.let { setupUI(innerView, it) }
                }
            }
        }


    }

}