package com.e.kadakk.toolbar

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel


class ToolBarViewModel(application: Application): AndroidViewModel(application) {

    var mToolBarNavigator: ToolBarNavigator ? = null;

    fun setToolBarNavigator(toolbarNavigate : ToolBarNavigator)
    {
        mToolBarNavigator = toolbarNavigate

    }


    fun backIconClick() {
        Log.e("ToolBar", "ToolBar backIconClick")
        mToolBarNavigator!!.backIconClick()

    }

    fun notificationIconClick() {
        Log.e("ToolBar", "ToolBar notificationIconClick")
        mToolBarNavigator!!.notificationIconClick()

    }





}