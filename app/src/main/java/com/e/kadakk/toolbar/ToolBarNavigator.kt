package com.e.kadakk.toolbar

interface ToolBarNavigator {

    fun backIconClick()
    fun notificationIconClick()

}